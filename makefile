.PHONY: test

test: 
	docker-compose -f docker-compose.test.yml -p ci up --abort-on-container-exit  --build
run: 
	sudo chmod -R a+rwx ./data/
	docker-compose up --build
clear-docker:
	docker system prune -a -f
full-clear-docker:
	docker system prune -a -f --volumes
