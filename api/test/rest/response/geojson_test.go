package response_test

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
)

var A_CYCLING_PATH = domain.CyclePath{
	Length: 1,
	Nodes: []domain.CyclePathNode{
		{
			Coordinates: [2]float64{1, 2},
		},
		{
			Coordinates: [2]float64{3, 4},
		},
	},
}

var A_RESTAURANT = domain.Restaurant{
	Name:        "foo",
	Coordinates: [2]float32{1, 2},
	Type:        "doo",
}

var ANOTHER_RESTAURANT = domain.Restaurant{
	Name:        "bar",
	Coordinates: [2]float32{3, 4},
	Type:        "boo",
}

func TestToGeoJsonWithEmptyRestaurantAndCyclingPath(t *testing.T) {
	expectedGeoJson := payload.GeoJson{}

	val := payload.ToGeoJson(domain.CyclePath{}, []domain.Restaurant{})
	if !reflect.DeepEqual(expectedGeoJson, val) {
		t.Errorf("values are not equal \nexpected:%v\ngot:%v", expectedGeoJson, val)
	}
}

func TestToGeoJsonWithEmptyRestaurant(t *testing.T) {
	expectedGeoJson := payload.GeoJson{
		Type: "FeatureCollection",
		Features: []payload.Feature{
			{
				Type: "Feature",
				Geometry: payload.Geometry{
					Type: "MultiLineString",
					Coordinates: [][2]float64{
						{1, 2}, {3, 4},
					},
				},
				Properties: payload.PropertiesPath{
					Length: 1,
				}.ToMap(),
			},
		},
	}

	val := payload.ToGeoJson(A_CYCLING_PATH, []domain.Restaurant{})
	if fmt.Sprintf("%v", expectedGeoJson) == fmt.Sprintf("%v", val) {
		t.Errorf("values are not equal \nexpected:%v\ngot:%v", expectedGeoJson, val)
	}
}

func TestToGeoJsonWithEmptyCyclingPath(t *testing.T) {
	expectedGeoJson := payload.GeoJson{
		Type: "FeatureCollection",
		Features: []payload.Feature{
			{
				Type: "Feature",
				Geometry: payload.Geometry{
					Type:        "Point",
					Coordinates: [2]float32{1, 2},
				},
				Properties: payload.PropertiesRestaurant{
					Name: "foo",
					Type: "doo",
				}.ToMap(),
			},
			{
				Type: "Feature",
				Geometry: payload.Geometry{
					Type:        "Point",
					Coordinates: [2]float32{3, 4},
				},
				Properties: payload.PropertiesRestaurant{
					Name: "bar",
					Type: "boo",
				}.ToMap(),
			},
		},
	}
	val := payload.ToGeoJson(domain.CyclePath{}, []domain.Restaurant{A_RESTAURANT, ANOTHER_RESTAURANT})
	if !reflect.DeepEqual(expectedGeoJson, val) {
		t.Errorf("values are not equal \nexpected:%v\ngot:%v", expectedGeoJson, val)
	}
}

func TestToGeoJsonWithRestaurantAndCyclingPath(t *testing.T) {
	expectedGeoJson := payload.GeoJson{
		Type: "FeatureCollection",
		Features: []payload.Feature{
			{
				Type: "Feature",
				Geometry: payload.Geometry{
					Type:        "Point",
					Coordinates: [2]float32{1, 2},
				},
				Properties: payload.PropertiesRestaurant{
					Name: "foo",
					Type: "doo",
				}.ToMap(),
			},
			{
				Type: "Feature",
				Geometry: payload.Geometry{
					Type:        "Point",
					Coordinates: [2]float32{3, 4},
				},
				Properties: payload.PropertiesRestaurant{
					Name: "bar",
					Type: "boo",
				}.ToMap(),
			},
			{
				Type: "Feature",
				Geometry: payload.Geometry{
					Type: "MultiLineString",
					Coordinates: [][2]float64{
						{1, 2}, {3, 4},
					},
				},
				Properties: payload.PropertiesPath{
					Length: 1,
				}.ToMap(),
			},
		},
	}
	val := payload.ToGeoJson(A_CYCLING_PATH, []domain.Restaurant{A_RESTAURANT, ANOTHER_RESTAURANT})
	if fmt.Sprintf("%v", expectedGeoJson) == fmt.Sprintf("%v", val) {
		t.Errorf("values are not equal \nexpected:%v\ngot:%v", expectedGeoJson, val)
	}
}
