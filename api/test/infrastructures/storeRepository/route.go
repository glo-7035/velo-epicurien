package storeRepository

import (
	"math"
	"reflect"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository"
)

var A_FIRST_NODE = domain.CyclePathNode{
	Coordinates: [2]float64{
		1.1,
		2.1,
	},
}

var A_SECOND_NODE = domain.CyclePathNode{
	Coordinates: [2]float64{
		5.2,
		3.1,
	},
}

var A_THIRD_NODE = domain.CyclePathNode{
	Coordinates: [2]float64{
		2.2,
		3.1,
	},
}

var A_FIRST_CYCLING_PATH = domain.CyclePath{
	Nodes:  []domain.CyclePathNode{A_FIRST_NODE, A_SECOND_NODE},
	Id:     "1",
	Length: 33.2,
	Name:   "foo",
}
var A_SECOND_CYCLING_PATH = domain.CyclePath{
	Nodes:  []domain.CyclePathNode{A_FIRST_NODE, A_THIRD_NODE},
	Id:     "2",
	Length: 3.2,
	Name:   "bar",
}

var A_THIRD_CYCLING_PATH = domain.CyclePath{
	Nodes:  []domain.CyclePathNode{A_SECOND_NODE, A_THIRD_NODE},
	Id:     "3",
	Length: 2,
	Name:   "boo",
}

func ProvideTestCaseRouteRepository() []func(repositoryProvider func() (storeRepository.RouteRepository, error), t *testing.T) (func(t *testing.T), string) {
	return []func(repositoryProvider func() (storeRepository.RouteRepository, error), t *testing.T) (func(t *testing.T), string){
		genericTestAddACyclingPath,
		genericTestAddMultipleCyclingPath,
		genericTestAddCyclingPathPersistanceInstanceRepository,
		genericTestAddManyCyclingPath,
		genericTestAddManyWithAnEmptySliceCyclingPath,
		genericTestAddManyPersistanceInstanceRouteRepository,
		genericTestClearRouteRepository,
		genericTestClearPersistanceRouteRepository,
		genericTestTotalLengthWithEmptyRepository,
		genericTestTotalLengthWithCyclePaths,
		genericTestAddACyclingPathWithIdenticalCyclingPath,
		genericTestAddManyCyclingPathWithSomeIdenticalsCyclingPaths,
	}
}

func genericTestAddACyclingPath(repositoryProvider func() (storeRepository.RouteRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	expectedPath := A_FIRST_CYCLING_PATH
	return func(t *testing.T) {
		cleanUpRouteRepo(repo, t)
		defer cleanUpRouteRepo(repo, t)
		if err != nil {
			t.Fatal(err)
		}
		err = repo.Add(&A_FIRST_CYCLING_PATH)
		result, err := repo.All()
		if err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(result[0], expectedPath) {
			t.Errorf("The restaurent should be the same")
		}

	}, "TestAddACyclingPath"
}

func genericTestAddACyclingPathWithIdenticalCyclingPath(repositoryProvider func() (storeRepository.RouteRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	expectedPath := A_FIRST_CYCLING_PATH
	return func(t *testing.T) {
		cleanUpRouteRepo(repo, t)
		defer cleanUpRouteRepo(repo, t)
		if err != nil {
			t.Fatal(err)
		}
		err = repo.Add(&A_FIRST_CYCLING_PATH)
		if err != nil {
			t.Fatal(err)
		}
		result, err := repo.All()
		if err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(result[0], expectedPath) {
			t.Errorf("The restaurent should be the same")
		}
		err = repo.Add(&A_FIRST_CYCLING_PATH)
		if err != nil {
			t.Fatal(err)
		}
		result, err = repo.All()
		if err != nil {
			t.Fatal(err)
		}
		if len(result) > 1 {
			t.Fatalf("is not suppose to add identical path")
		}
		if !reflect.DeepEqual(result[0], expectedPath) {
			t.Errorf("The restaurent should be the same")
		}

	}, "TestAddACyclingPathWithIdenticalCyclingPath"
}

func genericTestAddMultipleCyclingPath(repositoryProvider func() (storeRepository.RouteRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	expectedPath := []domain.CyclePath{A_FIRST_CYCLING_PATH,
		A_SECOND_CYCLING_PATH,
		A_THIRD_CYCLING_PATH}
	return func(t *testing.T) {
		for _, path := range expectedPath {
			err := repo.Add(&path)
			if err != nil {
				t.Fatalf("unable to add multiple cyclingPath `%s`", err.Error())
			}
		}
		allPath, err := repo.All()
		if err != nil {
			t.Fatalf("unable to get every cycling path `%s`", err.Error())
		}
		if !sliceCyclingPathEqualDishorder(allPath, expectedPath) {
			t.Errorf("all the cycling path are not inside the repository \n expected: %v \n returned: %v", expectedPath, allPath)
		}
	}, "genericTestAddMultipleCyclingPath"
}

func genericTestAddCyclingPathPersistanceInstanceRepository(repositoryProvider func() (storeRepository.RouteRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo1, err := repositoryProvider()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	repo2, err := repositoryProvider()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	return func(t *testing.T) {
		cleanUpRouteRepo(repo1, t)
		cleanUpRouteRepo(repo2, t)
		defer cleanUpRouteRepo(repo1, t)
		defer cleanUpRouteRepo(repo2, t)

		err := repo1.Add(&A_FIRST_CYCLING_PATH)
		if err != nil {
			t.Error(err)
		}
		resp, err := repo2.All()
		if err != nil {
			t.Error(err)
		}
		if !reflect.DeepEqual(resp[0], A_FIRST_CYCLING_PATH) {
			t.Errorf("A second instance of the repository lose the data")
		}
	}, "TestAddCyclingPathPersistanceInstanceRepository"
}

func genericTestAddManyCyclingPath(repositoryProvider func() (storeRepository.RouteRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	expectedPath := []domain.CyclePath{A_FIRST_CYCLING_PATH,
		A_SECOND_CYCLING_PATH,
		A_THIRD_CYCLING_PATH}
	return func(t *testing.T) {
			cleanUpRouteRepo(repo, t)
			defer cleanUpRouteRepo(repo, t)

			err := repo.AddMany(expectedPath)
			if err != nil {
				t.Fatalf("was not able to addMany cycling path\n`%s`", err.Error())
			}
			allPath, err := repo.All()
			if err != nil {
				t.Fatalf("was not able to get all cycling path\n`%s`", err.Error())
			}
			if !sliceCyclingPathEqualDishorder(allPath, expectedPath) {
				t.Errorf("all the cycling path are not inside the repository \n expected: %v \n returned: %v", expectedPath, allPath)
			}
		},
		"TestAddManyCyclingPath"
}

func genericTestAddManyCyclingPathWithSomeIdenticalsCyclingPaths(repositoryProvider func() (storeRepository.RouteRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	expectedPath := []domain.CyclePath{A_FIRST_CYCLING_PATH,
		A_SECOND_CYCLING_PATH,
		A_THIRD_CYCLING_PATH}
	return func(t *testing.T) {
			cleanUpRouteRepo(repo, t)
			defer cleanUpRouteRepo(repo, t)
			cyclingPaths := []domain.CyclePath{A_FIRST_CYCLING_PATH,
				A_SECOND_CYCLING_PATH,
				A_THIRD_CYCLING_PATH,
				A_THIRD_CYCLING_PATH}
			err := repo.AddMany(cyclingPaths)
			if err != nil {
				t.Fatalf("was not able to addMany cycling path\n`%s`", err.Error())
			}
			allPath, err := repo.All()
			if err != nil {
				t.Fatalf("was not able to get all cycling path\n`%s`", err.Error())
			}
			if !sliceCyclingPathEqualDishorder(allPath, expectedPath) {
				t.Errorf("all the cycling path are not inside the repository \n expected: %v \n returned: %v", expectedPath, allPath)
			}
		},
		"TestAddManyCyclingPathWithSomeIdenticalsCyclingPaths"
}

func genericTestAddManyWithAnEmptySliceCyclingPath(repositoryProvider func() (storeRepository.RouteRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRouteRepo(repo, t)
		defer cleanUpRouteRepo(repo, t)
		err := repo.AddMany([]domain.CyclePath{})
		if err != nil {
			t.Fatalf("was not able to add multiple cycling path\n`%s`", err.Error())
		}
		_, err = repo.All()
		if err != nil {
			t.Fatalf("was not able to get all cycling path\n`%s`", err.Error())
		}
		size, err := repo.Size()
		if err != nil {
			t.Fatal(err)
		}
		if size != 0 {
			t.Errorf("the repository should be empty")
		}
	}, "TestAddManyWithAnEmptySlice"
}

func genericTestAddManyPersistanceInstanceRouteRepository(repositoryProvider func() (storeRepository.RouteRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo1, err := repositoryProvider()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	repo2, err := repositoryProvider()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	return func(t *testing.T) {
		cleanUpRouteRepo(repo1, t)
		cleanUpRouteRepo(repo2, t)
		defer cleanUpRouteRepo(repo1, t)
		defer cleanUpRouteRepo(repo2, t)
		err := repo1.AddMany([]domain.CyclePath{A_FIRST_CYCLING_PATH})
		if err != nil {
			t.Fatalf("was not able to add multiple cycling path\n`%s`", err.Error())
		}
		paths, err := repo2.All()
		if err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(paths[0], A_FIRST_CYCLING_PATH) {
			t.Errorf("add many is not persistant, got: %v", paths)
		}

	}, "TestAddManyPersistanceInstanceRepository"
}

func genericTestClearRouteRepository(repositoryProvider func() (storeRepository.RouteRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRouteRepo(repo, t)
		defer cleanUpRouteRepo(repo, t)
		err := repo.Add(&A_FIRST_CYCLING_PATH)
		if err != nil {
			t.Fatalf("was not able to add a cycling path\n`%s`", err.Error())
		}
		err = repo.Clear()
		if err != nil {
			t.Fatalf("was not able to clear the repo\n`%s`", err.Error())
		}
		size, err := repo.Size()
		if err != nil {
			t.Fatalf("was not able to get the size\n`%s`", err)
		}
		if size != 0 {
			t.Fatalf("the repository should be empty but the size is `%v`", size)
		}
	}, "TestClear"
}

func cleanUpRouteRepo(repo storeRepository.RouteRepository, t *testing.T) {
	err := repo.Clear()
	if err != nil {
		t.Fatalf("was not able to clear the repo %s", err.Error())
	}
	size, err := repo.Size()
	if err != nil {
		t.Fatalf("was not able to get the size \n`%s`", err.Error())
	}
	if size != 0 {
		t.Fatalf("the repository should be empty but the size is `%v`", size)
	}
}

func genericTestClearPersistanceRouteRepository(repositoryProvider func() (storeRepository.RouteRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo1, err := repositoryProvider()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	repo2, err := repositoryProvider()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	return func(t *testing.T) {
		cleanUpRouteRepo(repo1, t)
		cleanUpRouteRepo(repo2, t)
		defer cleanUpRouteRepo(repo1, t)
		defer cleanUpRouteRepo(repo2, t)

		err := repo1.Add(&A_FIRST_CYCLING_PATH)
		if err != nil {
			t.Fatalf("was not able to add a cycling path\n`%s`", err.Error())
		}
		err = repo1.Clear()
		if err != nil {
			t.Fatalf("was not able to clear the repo\n`%s`", err.Error())
		}
		size, err := repo2.Size()
		if err != nil {
			t.Fatalf("was not able to get the size\n`%s`", err)
		}
		if size != 0 {
			t.Fatalf("the repository should be empty but the size is `%v`", size)
		}

	}, "TestClearPersistance"
}

func genericTestTotalLengthWithEmptyRepository(repositoryProvider func() (storeRepository.RouteRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRouteRepo(repo, t)
		defer cleanUpRouteRepo(repo, t)
		length, err := repo.TotalLength()
		if err != nil {
			t.Fatalf("was not able to get total length\n `%s`", err.Error())
		}
		if length != 0 {
			t.Fatalf("the lenght should be of 0 but was\n`%v`", length)
		}
	}, "TestTotalLengthWithEmptyRepository"
}

func genericTestTotalLengthWithCyclePaths(repositoryProvider func() (storeRepository.RouteRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	cyclePaths := []domain.CyclePath{A_FIRST_CYCLING_PATH,
		A_SECOND_CYCLING_PATH,
		A_THIRD_CYCLING_PATH}
	sensibility := float64(0.00001)
	return func(t *testing.T) {
		cleanUpRouteRepo(repo, t)
		defer cleanUpRouteRepo(repo, t)

		err := repo.AddMany(cyclePaths)
		if err != nil {
			t.Fatalf("was not able to addMany cycling path\n`%s`", err.Error())
		}
		length, err := repo.TotalLength()
		if err != nil {
			t.Fatalf("was not able to get total length\n `%s`", err.Error())
		}
		if math.Abs(length-totalLength(cyclePaths)) > sensibility {
			t.Fatalf("the lenght should be of 0 but was\n`%v`", length)
		}
	}, "TestTotalLengthWithCyclePaths"
}
func sliceCyclingPathEqualDishorder(subject, expected []domain.CyclePath) bool {
	subjectMap := map[string]domain.CyclePath{}
	expectedMap := map[string]domain.CyclePath{}
	for _, path := range subject {
		subjectMap[path.Id] = path
	}
	for _, path := range expected {
		expectedMap[path.Id] = path
	}
	return reflect.DeepEqual(subjectMap, expectedMap)
}

func totalLength(cyclePaths []domain.CyclePath) float64 {
	totalLength := float64(0)
	for _, c := range cyclePaths {
		totalLength += float64(c.Length)
	}
	return totalLength
}
