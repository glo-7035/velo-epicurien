package storeRepository

import (
	"reflect"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
)

func ProvideTestCaseCacheRepository() []func(repositoryProvider func() (storeRepository.CacheRepository, error), t *testing.T) (func(t *testing.T), string) {
	return []func(repositoryProvider func() (storeRepository.CacheRepository, error), t *testing.T) (func(t *testing.T), string){
		testAddAParcoursResponse,
		testAddTwoParcoursResponse,
		testAddTwoIdenticalsParcoursResponse,
		testAddACalorieResponse,
		testAddTwoCalorieResponse,
		testAddTwoIdenticalsCalorieResponse,
		testGetAllResponse,
		testGetAllResponseEmptyRepository,
		testClearWithEmptyRepository,
		testClear,
	}
}

func testAddAParcoursResponse(repositoryProvider func() (storeRepository.CacheRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		_ = repo.Clear()
		defer repo.Clear()
		err := repo.AddParcoursResponseIfNotExist(A_COURSE_REQUEST, A_GEO_JSON)
		if err != nil {
			t.Fatalf("was not able to add a parcours response\n`%v`", err)
		}
		resp, err := repo.GetParcoursResponse(A_COURSE_REQUEST)
		if err != nil {
			t.Fatalf("was not able to get a parcours response\n`%v`", err)
		}
		if !reflect.DeepEqual(resp, A_GEO_JSON) {
			t.Fatalf("the response are not equals\ngot:`%v`\nsuppose to get:`%v`", resp, A_GEO_JSON)
		}
	}, "TestAddAParcoursResponse"
}

func testAddTwoParcoursResponse(repositoryProvider func() (storeRepository.CacheRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		_ = repo.Clear()
		defer repo.Clear()

		err := repo.AddParcoursResponseIfNotExist(A_COURSE_REQUEST, A_GEO_JSON)
		if err != nil {
			t.Fatalf("was not able to add a parcours response\n`%v`", err)
		}
		resp1, err := repo.GetParcoursResponse(A_COURSE_REQUEST)
		if err != nil {
			t.Fatalf("was not able to get a parcours response\n`%v`", err)
		}
		if !reflect.DeepEqual(resp1, A_GEO_JSON) {
			t.Fatalf("the response are not equals\ngot:`%v`\nsuppose to get:`%v`", resp1, A_GEO_JSON)
		}

		err = repo.AddParcoursResponseIfNotExist(A_SECOND_COURSE_REQUEST, A_SECOND_GEO_JSON)
		if err != nil {
			t.Fatalf("was not able to add a parcours response\n`%v`", err)
		}
		resp2, err := repo.GetParcoursResponse(A_SECOND_COURSE_REQUEST)
		if err != nil {
			t.Fatalf("was not able to get a parcours response\n`%v`", err)
		}
		if !reflect.DeepEqual(resp2, A_SECOND_GEO_JSON) {
			t.Fatalf("the response are not equals\ngot:`%v`\nsuppose to get:`%v`", resp2, A_SECOND_GEO_JSON)
		}
	}, "TestAddTwoParcoursResponse"
}

func testAddTwoIdenticalsParcoursResponse(repositoryProvider func() (storeRepository.CacheRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		_ = repo.Clear()

		defer repo.Clear()
		err := repo.AddParcoursResponseIfNotExist(A_COURSE_REQUEST, A_GEO_JSON)
		if err != nil {
			t.Fatalf("was not able to add a parcours response\n`%v`", err)
		}
		err = repo.AddParcoursResponseIfNotExist(A_COURSE_REQUEST, A_SECOND_GEO_JSON)
		if err == nil {
			t.Fatal("suppose to return an error")
		}
		resp, err := repo.GetParcoursResponse(A_COURSE_REQUEST)
		if err != nil {
			t.Fatalf("was not able to get a parcours response\n`%v`", err)
		}
		if !reflect.DeepEqual(resp, A_GEO_JSON) {
			t.Fatalf("the response are not equals\ngot:`%v`\nsuppose to get:`%v`", resp, A_GEO_JSON)
		}
	}, "TestAddTwoIdenticalsParcoursResponse"
}

func testAddACalorieResponse(repositoryProvider func() (storeRepository.CacheRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		_ = repo.Clear()

		defer repo.Clear()
		err := repo.AddCalorieResponseIfNotExist(A_COURSE_REQUEST, A_CALORIE_RESPONSE)
		if err != nil {
			t.Fatalf("was not able to add a parcours response\n`%v`", err)
		}
		resp, err := repo.GetCalorieResponse(A_COURSE_REQUEST)
		if err != nil {
			t.Fatalf("was not able to get a parcours response\n`%v`", err)
		}
		if !reflect.DeepEqual(resp, A_CALORIE_RESPONSE) {
			t.Fatalf("the response are not equals\ngot:`%v`\nsuppose to get:`%v`", resp, A_CALORIE_RESPONSE)
		}
	}, "TestAddACalorieResponse"
}

func testAddTwoCalorieResponse(repositoryProvider func() (storeRepository.CacheRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		_ = repo.Clear()

		defer repo.Clear()
		err := repo.AddCalorieResponseIfNotExist(A_COURSE_REQUEST, A_CALORIE_RESPONSE)
		if err != nil {
			t.Fatalf("was not able to add a parcours response\n`%v`", err)
		}
		resp1, err := repo.GetCalorieResponse(A_COURSE_REQUEST)
		if err != nil {
			t.Fatalf("was not able to get a parcours response\n`%v`", err)
		}
		if !reflect.DeepEqual(resp1, A_CALORIE_RESPONSE) {
			t.Fatalf("the response are not equals\ngot:`%v`\nsuppose to get:`%v`", resp1, A_CALORIE_RESPONSE)
		}

		err = repo.AddCalorieResponseIfNotExist(A_SECOND_COURSE_REQUEST, A_SECOND_CALORIE_RESPONSE)
		if err != nil {
			t.Fatalf("was not able to add a parcours response\n`%v`", err)
		}
		resp2, err := repo.GetCalorieResponse(A_SECOND_COURSE_REQUEST)
		if err != nil {
			t.Fatalf("was not able to get a parcours response\n`%v`", err)
		}
		if !reflect.DeepEqual(resp2, A_SECOND_CALORIE_RESPONSE) {
			t.Fatalf("the response are not equals\ngot:`%v`\nsuppose to get:`%v`", resp2, A_SECOND_CALORIE_RESPONSE)
		}
	}, "TestAddTwoCalorieResponse"
}

func testAddTwoIdenticalsCalorieResponse(repositoryProvider func() (storeRepository.CacheRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		_ = repo.Clear()
		defer repo.Clear()

		err := repo.AddCalorieResponseIfNotExist(A_COURSE_REQUEST, A_CALORIE_RESPONSE)
		if err != nil {
			t.Fatalf("was not able to add a parcours response\n`%v`", err)
		}
		err = repo.AddCalorieResponseIfNotExist(A_COURSE_REQUEST, A_SECOND_CALORIE_RESPONSE)
		if err == nil {
			t.Fatalf("suppose to return an error")
		}
		resp, err := repo.GetCalorieResponse(A_COURSE_REQUEST)
		if err != nil {
			t.Fatalf("was not able to get a parcours response\n`%v`", err)
		}
		if !reflect.DeepEqual(resp, A_CALORIE_RESPONSE) {
			t.Fatalf("the response are not equals\ngot:`%v`\nsuppose to get:`%v`", resp, A_CALORIE_RESPONSE)
		}
	}, "TestAddTwoIdenticalsCalorieResponse"
}

func testClear(repositoryProvider func() (storeRepository.CacheRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		_ = repo.Clear()
		defer repo.Clear()

		err := repo.AddCalorieResponseIfNotExist(A_COURSE_REQUEST, A_CALORIE_RESPONSE)
		if err != nil {
			t.Fatalf("was not able to add a parcours response\n`%v`", err)
		}
		err = repo.AddParcoursResponseIfNotExist(A_COURSE_REQUEST, A_GEO_JSON)
		if err != nil {
			t.Fatalf("was not able to add a parcours response\n`%v`", err)
		}

		repo.Clear()
		parcours, calorie, err := repo.GetAllResponse()
		if err != nil {
			t.Fatalf("was not able to get all responses\n`%v`", err)
		}

		if len(parcours)+len(calorie) != 0 {
			t.Fatalf("the repo is not clear")
		}
	}, "TestClear"
}

func testClearWithEmptyRepository(repositoryProvider func() (storeRepository.CacheRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		_ = repo.Clear()
		defer repo.Clear()

		repo.Clear()
		parcours, calorie, err := repo.GetAllResponse()
		if err != nil {
			t.Fatalf("was not able to get all responses\n`%v`", err)
		}

		if len(parcours)+len(calorie) != 0 {
			t.Fatalf("the repo is not clear")
		}
	}, "TestClearWithEmptyRepository"
}

func testGetAllResponseEmptyRepository(repositoryProvider func() (storeRepository.CacheRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		_ = repo.Clear()
		defer repo.Clear()

		parcours, calorie, err := repo.GetAllResponse()
		if err != nil {
			t.Fatalf("was not able to get all responses\n`%v`", err)
		}

		if len(parcours)+len(calorie) != 0 {
			t.Fatalf("the repo is not clear got\n`%v`\n`%v`", parcours, calorie)
		}
	}, "TestGetAllResponseEmptyRepository"
}

func testGetAllResponse(repositoryProvider func() (storeRepository.CacheRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		_ = repo.Clear()
		defer repo.Clear()

		err := repo.AddCalorieResponseIfNotExist(A_COURSE_REQUEST, A_CALORIE_RESPONSE)
		if err != nil {
			t.Fatalf("was not able to add a parcours response\n`%v`", err)
		}
		err = repo.AddParcoursResponseIfNotExist(A_COURSE_REQUEST, A_GEO_JSON)
		if err != nil {
			t.Fatalf("was not able to add a parcours response\n`%v`", err)
		}

		err = repo.AddParcoursResponseIfNotExist(A_SECOND_COURSE_REQUEST, A_SECOND_GEO_JSON)
		if err != nil {
			t.Fatalf("was not able to add a parcours response\n`%v`", err)
		}
		expectedParcours := []payload.GeoJson{A_SECOND_GEO_JSON, A_GEO_JSON}
		expectedCalorie := []payload.CalorieResponseBody{A_CALORIE_RESPONSE}
		parcours, calorie, err := repo.GetAllResponse()
		if err != nil {
			t.Fatalf("was not able to get all responses\n`%v`", err)
		}

		if len(parcours) != 2 {
			t.Fatalf("all parcours are not equal\n got:`%v`\nexpected:`%v`", parcours, expectedParcours)
		}

		if len(calorie) != 1 {
			t.Fatalf("all calorie are not equal\n got:`%v`\nexpected:`%v`", calorie, expectedCalorie)
		}
	}, "TestGetAllResponse"
}

var A_COURSE_REQUEST = payload.CourseRequest{
	StartingPoint: payload.Geometry{
		Type:        "foo",
		Coordinates: "c1",
	},
	MaximumLength: 1,
	NumberOfStops: 1,
	Type:          []string{"a", "b"},
}

var A_SECOND_COURSE_REQUEST = payload.CourseRequest{
	StartingPoint: payload.Geometry{
		Type:        "bar",
		Coordinates: "c2",
	},
	MaximumLength: 2,
	NumberOfStops: 2,
	Type:          []string{"c", "d"},
}

var A_GEO_JSON = payload.GeoJson{
	Type: "FeatureCollection",
	Features: []payload.Feature{
		{
			Type: "Feature",
			Geometry: payload.Geometry{
				Type:        "MultiLineString",
				Coordinates: "",
			},
			Properties: map[string]interface{}{"b": "b"},
		},
	},
}

var A_SECOND_GEO_JSON = payload.GeoJson{
	Type: "FeatureCollection",
	Features: []payload.Feature{
		{
			Type: "Feature",
			Geometry: payload.Geometry{
				Type:        "MultiLineString",
				Coordinates: "foo",
			},
			Properties: map[string]interface{}{"a": "a"},
		},
	},
}

var A_CALORIE_RESPONSE = payload.CalorieResponseBody{
	CalorieIntake: 1.1,
	CalorieOutput: 2.1,
	CalorieNet:    3.1,
	Course:        A_GEO_JSON,
}
var A_SECOND_CALORIE_RESPONSE = payload.CalorieResponseBody{
	CalorieIntake: 3.1,
	CalorieOutput: 4.1,
	CalorieNet:    0.1,
	Course:        A_SECOND_GEO_JSON,
}
