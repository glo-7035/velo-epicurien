package mongo

import (
	"fmt"
	"os"
	"testing"

	"github.com/spf13/viper"
	configRoot "gitlab.com/constraintAutomaton/velo-epicurien/api/config"
	storeRepository "gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository/mongo"
)

func TestMain(m *testing.M) {
	setEnv()
	setUp()
	code := m.Run()
	tearDown()
	os.Exit(code)
}
func setEnv() {
	config := configRoot.MongoDatabaseConfigurations{
		DBAddr: "mongodb://mongo:27017",
	}
	viper.Set("Mongodatabase", config)
}
func setUp() {
	repoRestaurant, err := storeRepository.ProvideRestaurantRepository()

	if err != nil {
		panic(fmt.Sprintf("was not able to connect to mongoTest\n%s", err.Error()))
	}
	err = repoRestaurant.Clear()

	if err != nil {
		panic(fmt.Sprintf("was not able to connect to clear mongoTest database\n%s", err.Error()))
	}

	repoRoute, err := storeRepository.ProvideRouteRepository()

	if err != nil {
		panic(fmt.Sprintf("was not able to connect to mongoTest\n%s", err.Error()))
	}
	err = repoRoute.Clear()

	if err != nil {
		panic(fmt.Sprintf("was not able to connect to clear mongoTest database\n%s", err.Error()))
	}
}

func tearDown() {
	repoRestaurant, err := storeRepository.ProvideRestaurantRepository()

	if err != nil {
		panic(fmt.Sprintf("was not able to connect to mongoTest\n%s", err.Error()))
	}
	err = repoRestaurant.Clear()

	if err != nil {
		panic(fmt.Sprintf("was not able to connect to clear mongoTest database\n%s", err.Error()))
	}

	repoRoute, err := storeRepository.ProvideRouteRepository()

	if err != nil {
		panic(fmt.Sprintf("was not able to connect to mongoTest\n%s", err.Error()))
	}
	err = repoRoute.Clear()

	if err != nil {
		panic(fmt.Sprintf("was not able to connect to clear mongoTest database\n%s", err.Error()))
	}
	repoRestaurant.Close()
	repoRoute.Close()
}
