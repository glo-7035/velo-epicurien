package mongo

import (
	"testing"

	storeRepository "gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository/mongo"
	testCases "gitlab.com/constraintAutomaton/velo-epicurien/api/test/infrastructures/storeRepository"
)

func TestCasesMongoRestaurantRepository(t *testing.T) {

	for _, testCase := range testCases.ProvideTestCaseRestaurantRepository() {
		test, name := testCase(storeRepository.ProvideRestaurantRepository, t)
		t.Run(name, test)
	}
}
