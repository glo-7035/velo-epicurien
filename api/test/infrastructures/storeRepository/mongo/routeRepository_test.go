package mongo

import (
	"testing"

	storeRepository "gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository/mongo"
	testCases "gitlab.com/constraintAutomaton/velo-epicurien/api/test/infrastructures/storeRepository"
)

func TestCasesMongoRouteRepository(t *testing.T) {

	for _, testCase := range testCases.ProvideTestCaseRouteRepository() {
		test, name := testCase(storeRepository.ProvideRouteRepository, t)
		t.Run(name, test)
	}
}
