package mongo

import (
	"testing"

	storeRepository "gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository/mongo"
	testCases "gitlab.com/constraintAutomaton/velo-epicurien/api/test/infrastructures/storeRepository"
)

func TestCasesMongoCacheRepository(t *testing.T) {

	for _, testCase := range testCases.ProvideTestCaseCacheRepository() {
		test, name := testCase(storeRepository.ProvideCacheRepository, t)
		t.Run(name, test)
	}
}
