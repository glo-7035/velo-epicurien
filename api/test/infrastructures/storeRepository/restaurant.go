package storeRepository

import (
	"reflect"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository"
)

var A_FIRST_RESTAURANT = domain.Restaurant{Name: "foo1", Type: "bar1", Coordinates: [2]float32{1, 2}, CalorieLevel: 500}
var A_SECOND_RESTAURANT = domain.Restaurant{Name: "foo2", Type: "bar1", Coordinates: [2]float32{3, 4}, CalorieLevel: 600}
var A_THIRD_RESTAURANT = domain.Restaurant{Name: "foo3", Type: "bar2", Coordinates: [2]float32{4, 5}, CalorieLevel: 700}

func ProvideTestCaseRestaurantRepository() []func(repositoryProvider func() (storeRepository.RestaurantRepository, error), t *testing.T) (func(t *testing.T), string) {
	return []func(repositoryProvider func() (storeRepository.RestaurantRepository, error), t *testing.T) (func(t *testing.T), string){
		genericTestAddARestaurant,
		genericTestAddMultipleRestaurant,
		genericTestAddRestaurantPersistanceInstanceRepository,
		genericTestAddManyRestaurant,
		genericTestAddManyWithAnEmptySliceRestaurant,
		genericTestAddManyPersistanceInstanceRestaurantRepository,
		genericTestRestaurantsType,
		genericTestRestaurantsTypeEmptyRepo,
		genericTestClearRestaurantRepository,
		genericTestClearPersistanceRestaurantRepository,
		genericTestAddMultipleTimeARestaurant,
		genericTestAddManyRestaurantWithIdenticalRestaurant,
	}
}

func genericTestAddARestaurant(repositoryProvider func() (storeRepository.RestaurantRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	expectedRestaurant := A_FIRST_RESTAURANT
	return func(t *testing.T) {
		cleanUpRestaurantRepo(repo, t)
		defer cleanUpRestaurantRepo(repo, t)
		if err != nil {
			t.Fatal(err)
		}
		err = repo.Add(&A_FIRST_RESTAURANT)
		result, err := repo.All()
		if err != nil {
			t.Fatal(err)
		}
		if result[0] != expectedRestaurant {
			t.Errorf("The restaurent should be the same")
		}
	}, "testAddRestaurant"
}

func genericTestAddMultipleTimeARestaurant(repositoryProvider func() (storeRepository.RestaurantRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	expectedRestaurant := A_FIRST_RESTAURANT
	return func(t *testing.T) {
		cleanUpRestaurantRepo(repo, t)
		defer cleanUpRestaurantRepo(repo, t)
		if err != nil {
			t.Fatal(err)
		}
		err = repo.Add(&A_FIRST_RESTAURANT)
		if err != nil {
			t.Fatal(err)
		}
		result, err := repo.All()
		if err != nil {
			t.Fatal(err)
		}
		if result[0] != expectedRestaurant {
			t.Errorf("The restaurent should be the same")
		}
		err = repo.Add(&A_FIRST_RESTAURANT)
		if err != nil {
			t.Fatal(err)
		}
		result, err = repo.All()
		if err != nil {
			t.Fatal(err)
		}
		if len(result) > 1 {
			t.Fatalf("is not suppose to add duplicate restaurant")
		}
		if result[0] != expectedRestaurant {
			t.Errorf("The restaurent should be the same")
		}
	}, "TestAddMultipleTimeARestaurant"
}

func genericTestAddMultipleRestaurant(repositoryProvider func() (storeRepository.RestaurantRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	expectedRestaurant := []domain.Restaurant{A_FIRST_RESTAURANT, A_SECOND_RESTAURANT, A_THIRD_RESTAURANT}
	return func(t *testing.T) {
		cleanUpRestaurantRepo(repo, t)
		defer cleanUpRestaurantRepo(repo, t)
		for _, restaurant := range expectedRestaurant {
			err := repo.Add(&restaurant)
			if err != nil {
				t.Fatalf("unable to add multiple restaurant `%s`", err.Error())
			}
		}
		allRestaurant, err := repo.All()
		if err != nil {
			t.Fatalf("unable to get every restaurant `%s`", err.Error())
		}
		if !sliceRestaurantEqualDishorder(allRestaurant, expectedRestaurant) {
			t.Errorf("all the restaurant are not inside the repository \n expected: %v \n returned: %v", expectedRestaurant, allRestaurant)
		}
	}, "TestAddMutipleRestaurant"
}

func genericTestAddRestaurantPersistanceInstanceRepository(repositoryProvider func() (storeRepository.RestaurantRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo1, err := repositoryProvider()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	repo2, err := repositoryProvider()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	return func(t *testing.T) {
		cleanUpRestaurantRepo(repo1, t)
		cleanUpRestaurantRepo(repo2, t)
		defer cleanUpRestaurantRepo(repo1, t)
		defer cleanUpRestaurantRepo(repo2, t)

		err := repo1.Add(&A_FIRST_RESTAURANT)
		if err != nil {
			t.Error(err)
		}
		resp, err := repo2.All()
		if err != nil {
			t.Error(err)
		}
		if resp[0] != A_FIRST_RESTAURANT {
			t.Errorf("A second instance of the repository lose the data")
		}

	}, "TestAddRestaurantPersistanceInstanceRepository"
}
func genericTestAddManyRestaurant(repositoryProvider func() (storeRepository.RestaurantRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRestaurantRepo(repo, t)
		defer cleanUpRestaurantRepo(repo, t)

		expectedRestaurant := []domain.Restaurant{A_FIRST_RESTAURANT, A_SECOND_RESTAURANT, A_THIRD_RESTAURANT}
		err := repo.AddMany(expectedRestaurant)
		if err != nil {
			t.Fatalf("was not able to add multiple restaurent\n`%s`", err.Error())
		}
		allRestaurant, err := repo.All()
		if err != nil {
			t.Fatalf("was not able to get all restaurant\n`%s`", err.Error())
		}
		if !sliceRestaurantEqualDishorder(allRestaurant, expectedRestaurant) {
			t.Errorf("all the restaurant are not inside the repository \n expected: %v \n returned: %v", expectedRestaurant, allRestaurant)
		}
	}, "TestAddManyRestaurant"
}

func genericTestAddManyRestaurantWithIdenticalRestaurant(repositoryProvider func() (storeRepository.RestaurantRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	expectedRestaurant := []domain.Restaurant{A_FIRST_RESTAURANT, A_SECOND_RESTAURANT, A_THIRD_RESTAURANT}

	return func(t *testing.T) {
		cleanUpRestaurantRepo(repo, t)
		defer cleanUpRestaurantRepo(repo, t)
		restaurantes := []domain.Restaurant{A_FIRST_RESTAURANT, A_SECOND_RESTAURANT, A_THIRD_RESTAURANT, A_THIRD_RESTAURANT}
		err := repo.AddMany(restaurantes)
		if err != nil {
			t.Fatalf("was not able to add multiple restaurent\n`%s`", err.Error())
		}
		allRestaurant, err := repo.All()
		if err != nil {
			t.Fatalf("was not able to get all restaurant\n`%s`", err.Error())
		}
		if !sliceRestaurantEqualDishorder(allRestaurant, expectedRestaurant) {
			t.Errorf("all the restaurant are not inside the repository \n expected: %v \n returned: %v", expectedRestaurant, allRestaurant)
		}
	}, "TestAddManyRestaurantWithIdenticalRestaurant"
}

func genericTestAddManyWithAnEmptySliceRestaurant(repositoryProvider func() (storeRepository.RestaurantRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRestaurantRepo(repo, t)
		defer cleanUpRestaurantRepo(repo, t)
		err := repo.AddMany([]domain.Restaurant{})
		if err != nil {
			t.Fatalf("was not able to add multiple restaurent\n`%s`", err.Error())
		}
		_, err = repo.All()
		if err != nil {
			t.Fatalf("was not able to get all restaurant\n`%s`", err.Error())
		}
		size, err := repo.Size()
		if err != nil {
			t.Fatal(err)
		}
		if size != 0 {
			t.Errorf("the repository should be empty")
		}
	}, "TestAddManyWithAnEmptySlice"
}
func genericTestAddManyPersistanceInstanceRestaurantRepository(repositoryProvider func() (storeRepository.RestaurantRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo1, err := repositoryProvider()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	repo2, err := repositoryProvider()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	return func(t *testing.T) {
		cleanUpRestaurantRepo(repo1, t)
		cleanUpRestaurantRepo(repo2, t)
		defer cleanUpRestaurantRepo(repo1, t)
		defer cleanUpRestaurantRepo(repo2, t)
		err := repo1.AddMany([]domain.Restaurant{A_FIRST_RESTAURANT})
		if err != nil {
			t.Fatalf("was not able to add multiple restaurent\n`%s`", err.Error())
		}
		restaurant, err := repo2.All()
		if err != nil {
			t.Fatal(err)
		}
		if restaurant[0] != A_FIRST_RESTAURANT {
			t.Errorf("add many is not persistant")
		}

	}, "TestAddManyPersistanceInstanceRepository"
}
func genericTestRestaurantsType(repositoryProvider func() (storeRepository.RestaurantRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRestaurantRepo(repo, t)
		defer cleanUpRestaurantRepo(repo, t)
		expectedRestaurantType := map[string]uint{
			"bar1": 2,
			"bar2": 1,
		}
		restaurants := []domain.Restaurant{A_FIRST_RESTAURANT, A_SECOND_RESTAURANT, A_THIRD_RESTAURANT}
		err := repo.AddMany(restaurants)
		if err != nil {
			t.Fatalf("was not able to add multiple restaurent\n`%s`", err.Error())
		}

		restaurantType, err := repo.RestaurantsTypes()
		if err != nil {
			t.Fatalf("was not able to get restaurants type\n `%s`", err.Error())
		}
		if !reflect.DeepEqual(restaurantType, expectedRestaurantType) {
			t.Fatalf("the restaurent type is not valid \nexpected: %v\nreturned: %v",
				expectedRestaurantType, restaurantType)
		}
	}, "TestRestaurantsType"
}

func genericTestRestaurantsTypeEmptyRepo(repositoryProvider func() (storeRepository.RestaurantRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRestaurantRepo(repo, t)
		defer cleanUpRestaurantRepo(repo, t)
		restaurantType, err := repo.RestaurantsTypes()
		if err != nil {
			t.Fatalf("was not able to get restaurants type\n `%s`", err.Error())
		}

		if len(restaurantType) != 0 {
			t.Fatalf("restaurant type should be empty")
		}
	}, "TestRestaurantsTypeEmptyRepo"
}

func genericTestClearRestaurantRepository(repositoryProvider func() (storeRepository.RestaurantRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRestaurantRepo(repo, t)
		defer cleanUpRestaurantRepo(repo, t)
		err := repo.Add(&A_FIRST_RESTAURANT)
		if err != nil {
			t.Fatalf("was not able to add a restaurant\n`%s`", err.Error())
		}
		err = repo.Clear()
		if err != nil {
			t.Fatalf("was not able to clear the repo\n`%s`", err.Error())
		}
		size, err := repo.Size()
		if err != nil {
			t.Fatalf("was not able to get the size\n`%s`", err)
		}
		if size != 0 {
			t.Fatalf("the repository should be empty but the size is `%v`", size)
		}
	}, "TestClear"
}

func genericTestClearPersistanceRestaurantRepository(repositoryProvider func() (storeRepository.RestaurantRepository, error), t *testing.T) (func(t *testing.T), string) {
	repo1, err := repositoryProvider()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	repo2, err := repositoryProvider()
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	return func(t *testing.T) {
		cleanUpRestaurantRepo(repo1, t)
		cleanUpRestaurantRepo(repo2, t)
		defer cleanUpRestaurantRepo(repo1, t)
		defer cleanUpRestaurantRepo(repo2, t)

		err := repo1.Add(&A_FIRST_RESTAURANT)
		if err != nil {
			t.Fatalf("was not able to add a restaurant\n`%s`", err.Error())
		}
		err = repo1.Clear()
		if err != nil {
			t.Fatalf("was not able to clear the repo\n`%s`", err.Error())
		}
		size, err := repo2.Size()
		if err != nil {
			t.Fatalf("was not able to get the size\n`%s`", err)
		}
		if size != 0 {
			t.Fatalf("the repository should be empty but the size is `%v`", size)
		}

	}, "TestClearPersistance"
}
func cleanUpRestaurantRepo(repo storeRepository.RestaurantRepository, t *testing.T) {
	err := repo.Clear()
	if err != nil {
		t.Fatalf("was not able to clear the repo %s", err.Error())
	}
	size, err := repo.Size()
	if err != nil {
		t.Fatalf("was not able to get the size \n`%s`", err.Error())
	}
	if size != 0 {
		t.Fatalf("the repository should be empty but the size is `%v`", size)
	}
}
func sliceRestaurantEqualDishorder(subject, expected []domain.Restaurant) bool {
	if len(expected) != len(subject) {
		return false
	}
	subjectMap := map[domain.Restaurant]domain.Restaurant{}
	expectedMap := map[domain.Restaurant]domain.Restaurant{}
	for _, restaurant := range subject {
		subjectMap[restaurant] = restaurant
	}
	for _, restaurant := range expected {
		expectedMap[restaurant] = restaurant
	}
	return reflect.DeepEqual(subjectMap, expectedMap)
}
