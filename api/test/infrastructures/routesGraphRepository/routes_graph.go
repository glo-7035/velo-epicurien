package routesGraphRepository

import (
	"fmt"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	infrastructures "gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/routeGraphRepository"
)

var AFirstRestaurant = domain.Restaurant{Id: "4567", Name: "foo1", Type: "bar1", Coordinates: [2]float32{1, 2}, CalorieLevel: 856}
var ASecondRestaurant = domain.Restaurant{Id: "4568", Name: "foo2", Type: "bar1", Coordinates: [2]float32{3, 4}, CalorieLevel: 750}
var AThirdRestaurant = domain.Restaurant{Id: "4569", Name: "foo3", Type: "bar2", Coordinates: [2]float32{4, 5}, CalorieLevel: 1350}

var AFirstCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.784262, -71.286716}}
var ASecondCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.784365, -71.285707}}
var AThirdCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.783902, -71.286045}}
var ACyclePath = domain.CyclePath{Id: "7777", Name: "Sentier", Nodes: []domain.CyclePathNode{AFirstCyclePathNode, ASecondCyclePathNode, AThirdCyclePathNode}}

var AFourthCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.784262, -71.286716}}
var AFifthCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.784365, -71.285707}}
var ASecondCyclePath = domain.CyclePath{Id: "8888", Name: "Adstock", Nodes: []domain.CyclePathNode{AFourthCyclePathNode, AFifthCyclePathNode}}

func ProvideTestCaseRouteGraphRepository() []func(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string) {
	return []func(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string){
		genericTestAddARestaurant,
		genericTestClearRestaurants,
		genericCycleRouteCountTestNoData,
		genericCycleRouteCountTestWithData,
		genericTestAddOneCyclePath,
		genericTestAddMultipleCyclePaths,
		generateCycleCourseTestFor3Stops,
		generateCycleCourseTestFor1StopOnlyMultipleTypes,
		generateCycleCourseTestForMultipleStopsSingleType,
		generateCycleCourseTestForNoStops,
		generateCycleCourseTestWithOnlyStartAndEndNode,
	}
}

func genericTestAddOneCyclePath(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	expectedCyclePath := ACyclePath
	return func(t *testing.T) {
		cleanUpRepo(repo, t)
		defer cleanUpRepo(repo, t)
		if err != nil {
			t.Fatal(err)
		}
		err = repo.AddCyclePath(&ACyclePath)
		if err != nil {
			t.Fatal(err)
		}

		result, err := repo.AllCyclePaths()
		if err != nil {
			t.Fatal(err)
		}

		if result == nil {
			t.Fatal("The result was nil")
		}

		if result[len(result)-1].Id != expectedCyclePath.Id {
			t.Errorf("The cycle path should be the same")
		}

		if len(result[len(result)-1].Nodes) != len(expectedCyclePath.Nodes) {
			t.Errorf("The cycle path returned does not have proper node count. Expected: %d, Received: %d", len(expectedCyclePath.Nodes), len(result[len(result)-1].Nodes))
		}

	}, "testAddOneCyclePathToNeo"
}

func genericTestAddMultipleCyclePaths(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	expectedCyclePath1 := ACyclePath
	expectedCyclePath2 := ASecondCyclePath
	return func(t *testing.T) {
		cleanUpRepo(repo, t)
		defer cleanUpRepo(repo, t)
		if err != nil {
			t.Fatal(err)
		}

		err = repo.AddCyclePath(&ACyclePath)
		err = repo.AddCyclePath(&ASecondCyclePath)

		if err != nil {
			t.Fatal(err)
		}

		result, err := repo.AllCyclePaths()
		if err != nil {
			t.Fatal(err)
		}

		if result == nil {
			t.Fatal("The result was nil")
		}

		if result[0].Id != expectedCyclePath1.Id {
			t.Errorf("The first cycle path should be the same")
		}

		if len(result[0].Nodes) != len(expectedCyclePath1.Nodes) {
			t.Errorf("The first cycle path returned does not have proper node count. Expected: %d, Received: %d", len(expectedCyclePath1.Nodes), len(result[len(result)-1].Nodes))
		}

		if result[len(result)-1].Id != expectedCyclePath2.Id {
			t.Errorf("The second cycle path should be the same")
		}

		if len(result[len(result)-1].Nodes) != len(expectedCyclePath2.Nodes) {
			t.Errorf("The second cycle path returned does not have proper node count. Expected: %d, Received: %d", len(expectedCyclePath2.Nodes), len(result[len(result)-1].Nodes))
		}

	}, "testAddMultipleCyclePathsToNeo"
}

func genericTestAddARestaurant(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	expectedRestaurant := AFirstRestaurant
	return func(t *testing.T) {
		cleanUpRepo(repo, t)
		defer cleanUpRepo(repo, t)
		if err != nil {
			t.Fatal(err)
		}
		err = repo.AddAndConnectRestaurant(&AFirstRestaurant)
		if err != nil {
			t.Fatal(err)
		}

		result, err := repo.AllRestaurants()
		if err != nil {
			t.Fatal(err)
		}

		if result == nil {
			t.Fatal("The result was nil")
		}

		restaurantReceived := result[len(result)-1]
		if restaurantReceived.Id != expectedRestaurant.Id {
			t.Errorf("The restaurent should have the same Id")
		}

		if restaurantReceived.Type != expectedRestaurant.Type {
			t.Errorf("The restaurent should have the same Type")
		}

		if restaurantReceived.Name != expectedRestaurant.Name {
			t.Errorf("The restaurent should have the same Name")
		}

		if restaurantReceived.CalorieLevel != expectedRestaurant.CalorieLevel {
			t.Errorf("The restaurent should have the same CalorieLevel")
		}

		if restaurantReceived.Latitude() != expectedRestaurant.Latitude() {
			t.Errorf("The restaurent should have the same Latitude")
		}
		if restaurantReceived.Longitude() != expectedRestaurant.Longitude() {
			t.Errorf("The restaurent should have the same Latitude")
		}

	}, "testAddRestaurantToNeo"
}

func genericTestClearRestaurants(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRepo(repo, t)
		defer cleanUpRepo(repo, t)
		err = repo.AddAndConnectRestaurant(&AFirstRestaurant)
		if err != nil {
			t.Fatal(err)
		}
		err = repo.AddAndConnectRestaurant(&ASecondRestaurant)
		if err != nil {
			t.Fatal(err)
		}
		err = repo.AddAndConnectRestaurant(&AThirdRestaurant)
		if err != nil {
			t.Fatal(err)
		}

		cleanUpRepo(repo, t)

		result, err := repo.AllRestaurants()
		if err != nil {
			t.Fatal(err)
		}
		if result != nil {
			t.Errorf("there should be no restaurant in database")
		}

	}, "testClearRestaurantInNeo"
}

func genericCycleRouteCountTestNoData(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRepo(repo, t)
		defer cleanUpRepo(repo, t)

		result, err := repo.AllCyclePaths()
		if err != nil {
			t.Fatal(err)
		}

		if result != nil && len(result) != 0 {
			t.Errorf("Cycle count should be 0, received %d", len(result))
		}

	}, "testNbCycleRouteWithNoCycleRoute"
}

func genericCycleRouteCountTestWithData(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRepo(repo, t)
		defer cleanUpRepo(repo, t)

		err := repo.AddCyclePath(&ACyclePath)
		if err != nil {
			t.Fatal(err)
		}

		result, err := repo.AllCyclePaths()
		if err != nil {
			t.Fatal(err)
		}

		if len(result) != 1 {
			t.Errorf("Cycle count should be 1, received %d", len(result))
		}

	}, "testNbCycleRouteWithCycleRoute"
}

var restaurant1ForCourse = domain.Restaurant{Id: "83", Name: "foo1", Type: "bar1", Coordinates: [2]float32{46.101794, -71.310988}, CalorieLevel: 5656}
var restaurant2ForCourse = domain.Restaurant{Id: "84", Name: "foo2", Type: "bar2", Coordinates: [2]float32{46.104014, -71.307428}, CalorieLevel: 123}
var restaurant3ForCourse = domain.Restaurant{Id: "85", Name: "foo3", Type: "bar3", Coordinates: [2]float32{46.107813, -71.301570}, CalorieLevel: 4000}

var node1ForCourse = domain.CyclePathNode{Coordinates: [2]float64{46.101657, -71.310892}}
var node2ForCourse = domain.CyclePathNode{Coordinates: [2]float64{46.103935, -71.307332}}
var node3ForCourse = domain.CyclePathNode{Coordinates: [2]float64{46.107694, -71.301449}}
var node4ForCourse = domain.CyclePathNode{Coordinates: [2]float64{46.100595, -71.312230}}
var node5ForCourse = domain.CyclePathNode{Coordinates: [2]float64{46.109522, -71.298515}}
var fullCyclePathForCourse = domain.CyclePath{Id: "123456", Name: "Sentier",
	Nodes:  []domain.CyclePathNode{node4ForCourse, node1ForCourse, node2ForCourse, node3ForCourse, node5ForCourse},
	Length: 1100,
}

func generateCycleCourseTestFor3Stops(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRepo(repo, t)
		defer cleanUpRepo(repo, t)

		err := repo.AddCyclePath(&fullCyclePathForCourse)
		if err != nil {
			t.Fatal(err)
		}

		allRestaurantsToAdd := []domain.Restaurant{restaurant1ForCourse, restaurant2ForCourse, restaurant3ForCourse}
		err = repo.AddAndConnectManyRestaurant(allRestaurantsToAdd)
		if err != nil {
			t.Fatal(err)
		}

		maxPathLength := 1100
		course, restaurants, err := repo.GenerateCyclingCourse(&node1ForCourse, maxPathLength, 10, []string{"bar1", "bar2", "bar3"})
		if err != nil {
			t.Fatal(err)
		}
		if len(restaurants) != 3 {
			t.Errorf("resulting course should have 3 restaurants. Received %d", len(restaurants))
		}

		if len(course.Nodes) != 3 {
			t.Errorf("CycleCourse should have at least 3 nodes. received : %d", len(course.Nodes))
		}

		if !isDistanceInAcceptableRange(float64(maxPathLength), &repo, &course, &node1ForCourse) {
			t.Errorf("CycleCourse does not have acceptable length")
		}

	}, "testGenerateCycleCourseWithValidData3Stops"

}

func generateCycleCourseTestFor1StopOnlyMultipleTypes(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRepo(repo, t)
		defer cleanUpRepo(repo, t)

		err := repo.AddCyclePath(&fullCyclePathForCourse)
		if err != nil {
			t.Fatal(err)
		}

		allRestaurantsToAdd := []domain.Restaurant{restaurant1ForCourse, restaurant2ForCourse, restaurant3ForCourse}
		err = repo.AddAndConnectManyRestaurant(allRestaurantsToAdd)
		if err != nil {
			t.Fatal(err)
		}

		maxPathLength := 1100
		course, restaurants, err := repo.GenerateCyclingCourse(&node1ForCourse, maxPathLength, 1, []string{"bar1", "bar2", "bar3"})
		if err != nil {
			t.Fatal(err)
		}
		if len(restaurants) != 1 {
			t.Errorf("resulting course should have 1 restaurants. Received %d", len(restaurants))
		}

		if len(course.Nodes) != 3 {
			t.Errorf("CycleCourse should have  3 nodes. received : %d", len(course.Nodes))
		}

		if !isDistanceInAcceptableRange(float64(maxPathLength), &repo, &course, &node1ForCourse) {
			t.Errorf("CycleCourse does not have acceptable length")
		}

	}, "testGenerateCycleCourseWithValidDataSingleStopMultipleTypes"

}

func generateCycleCourseTestForMultipleStopsSingleType(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRepo(repo, t)
		defer cleanUpRepo(repo, t)

		err := repo.AddCyclePath(&fullCyclePathForCourse)
		if err != nil {
			t.Fatal(err)
		}

		allRestaurantsToAdd := []domain.Restaurant{restaurant1ForCourse, restaurant2ForCourse, restaurant3ForCourse}
		err = repo.AddAndConnectManyRestaurant(allRestaurantsToAdd)
		if err != nil {
			t.Fatal(err)
		}

		maxPathLength := 1100
		course, restaurants, err := repo.GenerateCyclingCourse(&node1ForCourse, maxPathLength, 3, []string{"bar1"})
		if err != nil {
			t.Fatal(err)
		}
		if len(restaurants) != 1 {
			t.Errorf("resulting course should have 1 restaurants. Received %d", len(restaurants))
		}

		if restaurants[0].Id != restaurant1ForCourse.Id ||
			restaurants[0].Name != restaurant1ForCourse.Name ||
			restaurants[0].Type != restaurant1ForCourse.Type {
			t.Errorf("Received and expected restaurants are not the same. Expected: \n")
			restaurant1ForCourse.Print()
			fmt.Println("Received:")
			restaurants[0].Print()
		}

		if len(course.Nodes) != 3 {
			t.Errorf("CycleCourse should have  3 nodes. received : %d", len(course.Nodes))
		}

		if !isDistanceInAcceptableRange(float64(maxPathLength), &repo, &course, &node1ForCourse) {
			t.Errorf("CycleCourse does not have acceptable length")
		}

	}, "testGenerateCycleCourseWithValidDataMultipleStopsButSingleType"

}

func generateCycleCourseTestForNoStops(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRepo(repo, t)
		defer cleanUpRepo(repo, t)

		err := repo.AddCyclePath(&fullCyclePathForCourse)
		if err != nil {
			t.Fatal(err)
		}

		allRestaurantsToAdd := []domain.Restaurant{restaurant1ForCourse, restaurant2ForCourse, restaurant3ForCourse}
		err = repo.AddAndConnectManyRestaurant(allRestaurantsToAdd)
		if err != nil {
			t.Fatal(err)
		}

		maxPathLength := 1100
		course, restaurants, err := repo.GenerateCyclingCourse(&node1ForCourse, maxPathLength, 0, []string{"bar1", "bar2", "bar3"})
		if err != nil {
			t.Fatal(err)
		}
		if restaurants != nil {
			t.Errorf("resulting course should have 0 restaurants. Received %d", len(restaurants))
		}

		if len(course.Nodes) != 3 {
			t.Errorf("CycleCourse should have  3 nodes. received : %d", len(course.Nodes))
		}

		if !isDistanceInAcceptableRange(float64(maxPathLength), &repo, &course, &node1ForCourse) {
			t.Errorf("CycleCourse does not have acceptable length")
		}

	}, "testGenerateCycleCourseWithValidDataNoStopsAtAll"

}

func generateCycleCourseTestWithOnlyStartAndEndNode(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRepo(repo, t)
		defer cleanUpRepo(repo, t)

		err := repo.AddCyclePath(&fullCyclePathForCourse)
		if err != nil {
			t.Fatal(err)
		}

		allRestaurantsToAdd := []domain.Restaurant{restaurant1ForCourse, restaurant2ForCourse, restaurant3ForCourse}
		err = repo.AddAndConnectManyRestaurant(allRestaurantsToAdd)
		if err != nil {
			t.Fatal(err)
		}

		maxPathLength := 400
		course, restaurants, err := repo.GenerateCyclingCourse(&node1ForCourse, maxPathLength, 10, []string{"bar1", "bar2", "bar3"})
		if err != nil {
			t.Fatal(err)
		}
		if len(restaurants) != 2 {
			t.Errorf("resulting course should have 1 restaurants. Received %d", len(restaurants))
		}

		if len(course.Nodes) != 2 {
			t.Errorf("CycleCourse should have  2 nodes. received : %d", len(course.Nodes))
		}

		for _, n := range course.Nodes {
			n.Print()
		}

		if !isDistanceInAcceptableRange(float64(maxPathLength), &repo, &course, &node1ForCourse) {
			t.Errorf("CycleCourse does not have acceptable length")
		}

	}, "testGenerateCycleCourseWithValidDataOnlyStartAndEndNodes"
}

var restaurant1startingPoint = domain.Restaurant{Id: "86", Name: "foo1", Type: "bar1", Coordinates: [2]float32{-71.310988, 46.101794}, CalorieLevel: 5656}
var restaurant2startingPoint = domain.Restaurant{Id: "87", Name: "foo2", Type: "bar2", Coordinates: [2]float32{-71.307428, 46.104014}, CalorieLevel: 123}
var restaurant3startingPoint = domain.Restaurant{Id: "88", Name: "foo3", Type: "bar2", Coordinates: [2]float32{-71.301570, 46.107813}, CalorieLevel: 4000}

var node1startingPoint = domain.CyclePathNode{Coordinates: [2]float64{-71.310892, 46.101657}}
var node2startingPoint = domain.CyclePathNode{Coordinates: [2]float64{-71.307332, 46.103935}}
var node3startingPoint = domain.CyclePathNode{Coordinates: [2]float64{-71.301449, 46.107694}}
var node4startingPoint = domain.CyclePathNode{Coordinates: [2]float64{-71.312230, 46.100595}}
var node5startingPoint = domain.CyclePathNode{Coordinates: [2]float64{-71.298515, 46.109522}}
var pathForStartingPoint = domain.CyclePath{Id: "654321", Name: "GRAND_LINE",
	Nodes: []domain.CyclePathNode{node1startingPoint, node2startingPoint, node3startingPoint, node4startingPoint, node5startingPoint}}

func findStartingPointTestWithSingleMatchCandidate(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRepo(repo, t)
		defer cleanUpRepo(repo, t)

		err := repo.AddCyclePath(&fullCyclePathForCourse)
		if err != nil {
			t.Fatal(err)
		}

		allRestaurantsToAdd := []domain.Restaurant{restaurant1ForCourse,
			restaurant2ForCourse, restaurant3ForCourse}
		err = repo.AddAndConnectManyRestaurant(allRestaurantsToAdd)
		if err != nil {
			t.Fatal(err)
		}

		maxLength := 1100
		typeCandidates := []string{"bar1", "bar2"}
		startingPoint, err := repo.GetStartingPointBestCandidate(maxLength, typeCandidates)
		if err != nil {
			t.Fatal(err)
		}

		provenancePath, err := repo.GetCyclePathForNode(startingPoint)
		if err != nil {
			t.Fatal(err)
		}

		if provenancePath.Id != fullCyclePathForCourse.Id {
			t.Errorf("starting point should be on path with id %s, but received %s", fullCyclePathForCourse.Id, provenancePath.Id)
		}

	}, "testGetStartingPointWithSingleMatchCandidate"

}

func findStartingPointTestRandomness(repositoryProvider func() (infrastructures.RoutesGraph, error), t *testing.T) (func(t *testing.T), string) {
	repo, err := repositoryProvider()
	if err != nil {
		t.Fatal(err)
	}
	return func(t *testing.T) {
		cleanUpRepo(repo, t)
		defer cleanUpRepo(repo, t)

		err := repo.AddCyclePath(&fullCyclePathForCourse)
		if err != nil {
			t.Fatal(err)
		}
		err = repo.AddCyclePath(&pathForStartingPoint)
		if err != nil {
			t.Fatal(err)
		}

		allRestaurantsToAdd := []domain.Restaurant{restaurant1ForCourse,
			restaurant2ForCourse, restaurant3ForCourse, restaurant1startingPoint,
			restaurant2startingPoint, restaurant3startingPoint}
		err = repo.AddAndConnectManyRestaurant(allRestaurantsToAdd)
		if err != nil {
			t.Fatal(err)
		}

		maxLength := 1100
		typeCandidates := []string{"bar1", "bar2"}
		lastNode, err := repo.GetStartingPointBestCandidate(maxLength, typeCandidates)
		if err != nil {
			t.Fatal(err)
		}

		randomTryCounter := 0
		maxTry := 100
		var otherNode domain.CyclePathNode
		for (otherNode.Id == 0 || otherNode.Id == lastNode.Id) && randomTryCounter <= maxTry {

			otherNode, err = repo.GetStartingPointBestCandidate(maxLength, typeCandidates)
			if err != nil {
				t.Fatal(err)
			}

			randomTryCounter++
		}

		if randomTryCounter > maxTry {
			t.Errorf("could not find different node in %d try. Function is not random", maxTry)
		}

	}, "TestFindStartingPointRandomness"

}

func isDistanceInAcceptableRange(expectedDistance float64, repo *infrastructures.RoutesGraph, course *domain.CyclePath, start *domain.CyclePathNode) bool {

	dist, err := (*repo).GetCourseDistance(course, start)
	if err != nil {
		fmt.Println(err.Error())
		return false
	}

	fmt.Printf("course distance : %f\n", dist)
	minAccLength := 0.9 * expectedDistance
	maxAccLength := 1.1 * expectedDistance

	ok := minAccLength <= dist && dist <= maxAccLength

	return ok
}

func cleanUpRepo(repo infrastructures.RoutesGraph, t *testing.T) {
	err := repo.ClearRestaurants()
	if err != nil {
		t.Fatalf("was not able to clear the repo for restaurants: %s", err.Error())
	}

	err = repo.ClearCyclePaths()
	if err != nil {
		t.Fatalf("was not able to clear the repo for cyclepaths: %s", err.Error())
	}
}
