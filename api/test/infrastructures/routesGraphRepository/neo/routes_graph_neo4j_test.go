package neo

import (
	"fmt"
	"os"
	"testing"

	"github.com/spf13/viper"
	configRoot "gitlab.com/constraintAutomaton/velo-epicurien/api/config"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/routeGraphRepository/neo"
	testCases "gitlab.com/constraintAutomaton/velo-epicurien/api/test/infrastructures/routesGraphRepository"
)

func TestMain(m *testing.M) {
	config := configRoot.NeoDatabaseConfigurations{
		DBAddr:     "neo4j://neo4j:7687",
		DBUser:     "neo4j",
		DBPassword: "glo7035",
	}
	viper.Set("Neodatabase", config)
	routeRepo, err := neo.ProvideRouteGraph()
	if err != nil {
		panic(fmt.Sprintf("was not able to connect to neoTest\n%s", err.Error()))
	}
	fmt.Println("was able to provide neo4j database once")

	code := m.Run()

	routeRepo.Close()

	os.Exit(code)
}

func TestCasesRouteGraphRepository(t *testing.T) {

	for _, testCase := range testCases.ProvideTestCaseRouteGraphRepository() {
		test, name := testCase(neo.ProvideRouteGraph, t)
		t.Run(name, test)
	}
}
