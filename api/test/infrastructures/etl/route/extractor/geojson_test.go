package extractor

import (
	"reflect"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/route/extractor"
)

func TestExtractFromAGeoJson(t *testing.T) {
	extractor := extractor.FileExtractor("./datas.json")
	expectedValue := map[string]interface{}{
		"type": "type",
		"name": "name",
		"crs": map[string]interface{}{
			"type": "name",
			"properties": map[string]interface{}{
				"name": "name",
			},
		},
	}
	value, err := extractor()

	if err != nil {
		t.Fatalf("an error returned\n`%s`", err.Error())
	}

	if !reflect.DeepEqual(value, expectedValue) {
		t.Fatalf("value should be equal\nreceived:%v\nexpected:%v", value, expectedValue)
	}

}
func TestExtractFromAListJson(t *testing.T) {
	extractor := extractor.FileExtractor("./aList.json")
	_, err := extractor()
	if err == nil {
		t.Fatalf("value should had return an error\n%v", err)
	}
}

func TestExtractorFromAnEmptyJson(t *testing.T) {
	extractor := extractor.FileExtractor("./empty.json")
	expectedValue := map[string]interface{}{}
	value, err := extractor()

	if err != nil {
		t.Fatalf("an error returned\n`%s`", err.Error())
	}

	if !reflect.DeepEqual(value, expectedValue) {
		t.Fatalf("value should be equal\nreceived:%v\nexpected:%v", value, expectedValue)
	}
}

func TestExtractorFromInvalidJson(t *testing.T) {
	extractor := extractor.FileExtractor("./invalid.json")
	_, err := extractor()
	if err == nil {
		t.Fatalf("value should had return an error\n%v", err)
	}
}
