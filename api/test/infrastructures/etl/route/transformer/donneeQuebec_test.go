package transformer

import (
	"fmt"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/route/extractor"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/route/transformer"
)

func TestTransformWithValidMap(t *testing.T) {
	inputValue, err := getInputValue()
	if err != nil {
		t.Fatal(err)
	}
	expectedValue := []domain.CyclePath{
		{
			Name:   "Réseau utilitaire",
			Id:     "1",
			Length: 325.2,
			Nodes: []domain.CyclePathNode{
				{
					Coordinates: [2]float64{-1.1, 4.4},
				},
				{
					Coordinates: [2]float64{-7.1, 4.4},
				},
			},
		},
		{
			Name:   "Axe de la Saint-François",
			Id:     "2",
			Length: 620.7,
			Nodes: []domain.CyclePathNode{
				{
					Coordinates: [2]float64{-1, 2},
				},
				{
					Coordinates: [2]float64{-3, 4},
				},
			},
		},
	}

	value, err := transformer.TransformerDonneeQuebec(inputValue)
	if err != nil {
		t.Fatal(err)
	}
	if !compareCyclingPath(expectedValue, value) {
		t.Fatalf("values are not equal\nexpected:%v\bgot:%v", expectedValue, value)
	}

}

func TestAnEmptyMap(t *testing.T) {
	inputValue := map[string]interface{}{}
	_, err := transformer.TransformerDonneeQuebec(inputValue)
	if err != nil {
		t.Fatal(err)
	}
}
func TestAnInvalidMap(t *testing.T) {
	inputValue := map[string]interface{}{
		"aaa": "aaa",
	}
	_, err := transformer.TransformerDonneeQuebec(inputValue)
	if err == nil {
		t.Fatal(err)
	}
}

func getInputValue() (map[string]interface{}, error) {
	extractor := extractor.FileExtractor("./data.json")
	val, err := extractor()
	if err != nil {
		return nil, fmt.Errorf("was not able to get the expected data %v", err)
	}
	return val, nil
}
func compareCyclingPath(expected, receive []domain.CyclePath) bool {
	sensibility := 0.001
	if len(expected) != len(receive) {
		return false
	}

	for i, v := range expected {
		r := receive[i]
		if v.Id != r.Id {
			return false
		}
		if v.Length-r.Length >= float32(sensibility) {
			return false
		}
		if len(v.Nodes) != len(r.Nodes) {
			return false
		}
		for i, v := range v.Nodes {
			r := r.Nodes[i]
			if v.Coordinates[0]-r.Coordinates[0] >= sensibility {
				return false
			}
			if v.Coordinates[1]-r.Coordinates[1] >= sensibility {
				return false
			}
		}

	}
	return true
}
