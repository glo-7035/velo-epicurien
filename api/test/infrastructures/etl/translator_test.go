package etl

import (
	"testing"

	etl "gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/graph"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/mock"
)

func TestTranslateRestaurants(t *testing.T) {
	restaurantRepo := mock.RestaurantRepository{HasCalled: map[string]uint{}}
	routeGraphRepo := mock.GraphRepository{HasCalled: map[string]uint{}}
	routeRepo := mock.RouteRepository{HasCalled: map[string]uint{}}

	translator, err := etl.ProvideTranslatorWithParams(&routeGraphRepo, &restaurantRepo, &routeRepo)
	if err != nil {
		t.Fatal(err)
	}

	err = translator.TranslateNewRestaurantEntries()
	if err != nil {
		t.Fatal(err)
	}

	if !(restaurantRepo.HasCalled["All"] == 1) {
		t.Fatalf("should had called `mongo.restaurant.All` once")
	}

	if !(routeGraphRepo.HasCalled["AllRestaurants"] == 1) {
		t.Fatalf("should had called `neo.AllRestaurants` once")
	}

	if !(routeGraphRepo.HasCalled["AddAndConnectManyRestaurant"] == 1) {
		t.Fatalf("should had called `neo.AddAndConnectRestaurant` 2 times")
	}

}

func TestTranslateCyclePaths(t *testing.T) {
	restaurantRepo := mock.RestaurantRepository{HasCalled: map[string]uint{}}
	routeGraphRepo := mock.GraphRepository{HasCalled: map[string]uint{}}
	routeRepo := mock.RouteRepository{HasCalled: map[string]uint{}}

	translator, err := etl.ProvideTranslatorWithParams(&routeGraphRepo, &restaurantRepo, &routeRepo)
	if err != nil {
		t.Fatal(err)
	}

	err = translator.TranslateNewCyclePathEntries()
	if err != nil {
		t.Fatal(err)
	}

	if !(routeRepo.HasCalled["All"] == 1) {
		t.Fatalf("should had called `mongo.route.All` once")
	}

	if !(routeGraphRepo.HasCalled["AllCyclePaths"] == 1) {
		t.Fatalf("should had called `neo.AllCyclePaths` once")
	}

	if !(routeGraphRepo.HasCalled["AddCyclePath"] == 1) {
		t.Fatalf("should had called `neo.AddCyclePath` 2 times")
	}
}

