package loader

import (
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/restaurant/loader"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/mock"
)

func TestLoadingRestaurant(t *testing.T) {
	repo := mock.RestaurantRepository{HasCalled: map[string]uint{}}
	loader := loader.Loader(&repo)
	_ = loader([]domain.Restaurant{{Name: ""}})
	if !(repo.HasCalled["AddMany"] == 1) {
		t.Fatalf("should had called `AddMany`")
	}

}
