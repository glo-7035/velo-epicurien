package transformer

import (
	"os"
	"reflect"
	"testing"

	"github.com/spf13/viper"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/restaurant/transformer"
)

func TestMain(m *testing.M) {
	viper.Set("TypeRestaurantDonneeQuebecPath", "./typeRestaurant.json")
	code := m.Run()
	os.Exit(code)
}
func TestTransformerWithValidValue(t *testing.T) {
	json := []map[string]interface{}{
		{
			"Nom":        "foo",
			"ID":         float64(1),
			"Categories": "1",
			"Longitude":  "1.2",
			"Latitude":   "0.1",
		},
		{
			"Nom":        "bar",
			"ID":         float64(2),
			"Categories": "2",
			"Longitude":  "1.1",
			"Latitude":   "0.3",
		},
		{
			"Nom":        "boo",
			"ID":         float64(3),
			"Categories": "2,1,2,1",
			"Longitude":  "1.3",
			"Latitude":   "4.3",
		},
	}
	expectedValue := []domain.Restaurant{
		{
			Name:        json[0]["Nom"].(string),
			Type:        "foo",
			Coordinates: [2]float32{1.2, 0.1},
			Id:          "1",
			CalorieLevel: 700,
		},
		{
			Name:        json[1]["Nom"].(string),
			Type:        "bar",
			Coordinates: [2]float32{1.1, 0.3},
			Id:          "2",
			CalorieLevel: 700,
		},
		{
			Name:        json[2]["Nom"].(string),
			Type:        "bar",
			Coordinates: [2]float32{1.3, 4.3},
			Id:          "3",
			CalorieLevel: 700,
		},
	}

	value, err := transformer.TransformerDonneeQuebec(json)
	if err != nil {
		t.Fatalf("returned an error\n`%s`", err.Error())
	}
	if !reflect.DeepEqual(value, expectedValue) {
		t.Fatalf("value should be equal\nreceived:%v\nexpected:%v", value, expectedValue)
	}
}

func TestTransformWithInvalidValue(t *testing.T) {
	jsonValid := []map[string]interface{}{
		{
			"Nom":        "foo",
			"ID":         float64(1),
			"Categories": "1",
			"Longitude":  "1.2",
			"Latitude":   "0.1",
		},
	}
	invalidValues := map[string]interface{}{
		"Nom":        1,
		"ID":         "",
		"Categories": 1,
		"Longitude":  1.2,
		"Latitude":   0.1,
	}
	for k, v := range invalidValues {
		jsonEl1 := jsonValid[0]
		jsonEl1[k] = v
		json := []map[string]interface{}{
			jsonEl1,
		}
		_, err := transformer.TransformerDonneeQuebec(json)
		if err == nil {
			t.Fatalf("should return an error with field `%s`", k)
		}

	}
}
