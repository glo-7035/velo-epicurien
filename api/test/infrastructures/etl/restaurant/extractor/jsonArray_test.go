package extractor

import (
	"reflect"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/restaurant/extractor"
)

func TestExtractFromAValidJson(t *testing.T) {
	extractor := extractor.FileExtractor("./datas.json")
	expectedValue := []map[string]interface{}{
		{
			"ID":       131.0,
			"Nom":      "foo",
			"Distance": float64(-1),
		},
		{
			"Name": float64(1),
			"type": "bar",
		},
		{
			"ID":       131.0,
			"Nom":      "foo",
			"Distance": float64(-1),
		},
	}

	value, err := extractor()

	if err != nil {
		t.Fatalf("an error returned\n`%s`", err.Error())
	}

	if !reflect.DeepEqual(value, expectedValue) {
		t.Fatalf("value should be equal\nreceived:%v\nexpected:%v", value, expectedValue)
	}

}

func TestExtractFromAnEmptyJson(t *testing.T) {
	extractor := extractor.FileExtractor("./empty.json")
	expectedValue := []map[string]interface{}{}

	value, err := extractor()

	if err != nil {
		t.Fatalf("an error returned\n`%s`", err.Error())
	}

	if !reflect.DeepEqual(value, expectedValue) {
		t.Fatalf("value should be equal\nreceived:%v\nexpected:%v", value, expectedValue)
	}
}

func TestExtractFromAnInvalidJson(t *testing.T) {
	extractor := extractor.FileExtractor("./invalid.json")

	value, err := extractor()

	if err == nil {
		t.Fatalf("should return an error but returned\n`%v`", value)
	}
}

func TestExtractFromANonArrayJson(t *testing.T) {
	extractor := extractor.FileExtractor("./notAList.json")

	value, err := extractor()

	if err == nil {
		t.Fatalf("should return an error but returned\n`%v`", value)
	}
}
