package extractor

import (
	"testing"

	"github.com/spf13/viper"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/restaurant/extractor"
)

func TestARequestOfRestaurantToDonneeQuebec(t *testing.T) {
	viper.Set("RemoteRestaurantPath", "https://www.donneesquebec.ca/recherche/fr/dataset/61b5b4e9-d038-4995-b85a-de039dc1b06b/resource/386e62b2-47ae-43bc-a85a-efbcaf3130ba/download/restaurants.json")

	value, err := extractor.DonneeQuebec()()
	if err != nil {
		t.Fatal(err)
	}
	if len(value) <= 0 {
		t.Fatalf("the remote server provide empty data or the extractor parse no data")
	}
}
