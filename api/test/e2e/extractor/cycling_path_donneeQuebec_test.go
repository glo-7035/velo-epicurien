package extractor

import (
	"testing"

	"github.com/spf13/viper"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/route/extractor"
)

func TestARequestOfCyclingPathToDonneeQuebec(t *testing.T) {
	viper.Set("RemoteCyclingRoutePath", "http://donneesouvertes-sherbrooke.opendata.arcgis.com/datasets/e7b14d41a20c4aafa91ed4a9b99384c3_3.geojson?outSR={\"latestWkid\":32187,\"wkid\":32187}")

	value, err := extractor.DonneeQuebec()()
	if err != nil {
		t.Fatal(err)
	}
	if len(value) <= 0 {
		t.Fatalf("the remote server provide empty data or the extractor parse no data")
	}
}
