package e2e

import (
	"encoding/json"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
	"math"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/route"
)

const TRANSFORMED_DATA_ROUTE = "/transformed_data"

func TestARequestToTheTransformedDataRoute(t *testing.T) {

	err := PopulateDatabasesAnalytics()
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("GET", TRANSFORMED_DATA_ROUTE, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.TransformedData)
	handler.ServeHTTP(rr, req)

	var receivedTransformedData payload.TransformedDataBody

	err = json.NewDecoder(rr.Body).Decode(&receivedTransformedData)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if math.Floor(float64(receivedTransformedData.LongueurCyclable)) != math.Floor(FirstPathLength + SecondPathLength) {
		t.Errorf("Received wrong cyclable length number. Expected %f, received %f", FirstPathLength + SecondPathLength,
			receivedTransformedData.LongueurCyclable)
	}

	if len(receivedTransformedData.Restaurants) != len(AllRestaurantTypes) {
		t.Errorf("Received wrong restaurant type count. Expected %d, received %d", len(AllRestaurantTypes), len(receivedTransformedData.Restaurants))
	}

	for i := range receivedTransformedData.Restaurants {
		if receivedTransformedData.Restaurants[i] != AllRestaurantTypes[i] {
			t.Errorf("Received wrong count for restaurant type %s. Expected %d, received %d",
				i, AllRestaurantTypes[i], receivedTransformedData.Restaurants[i])
		}
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}

