package e2e

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/route"
)

const PARCOURS_ROUTE = "/parcours"
const MULTI_LINE = "MultiLineString"
const POINT = "Point"
const RESTAURANT_TYPE1 = "bar2"
const RESTAURANT_TYPE2 = "bar3"

func TestARequestToTheParcoursRoute(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	startPoint := payload.Geometry{
		Type:        "Point",
		Coordinates: [2]float32{46.103935, -71.307332},
	}

	body := payload.CourseRequest{
		StartingPoint: startPoint,
		MaximumLength: 1100,
		NumberOfStops: 2,
		Type:          []string{RESTAURANT_TYPE1, RESTAURANT_TYPE2},
	}
	jsonValue, _ := json.Marshal(body)

	req, err := http.NewRequest("GET", PARCOURS_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Course)
	handler.ServeHTTP(rr, req)

	var receivedCourse payload.GeoJson

	err = json.NewDecoder(rr.Body).Decode(&receivedCourse)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if receivedCourse.Type != "FeatureCollection" {
		t.Errorf("received geometry should be FeatureCollection GeoJSON")
	}

	totalDistance := extractTotalDistance(receivedCourse)

	if totalDistance < 0.9*float32(body.MaximumLength) ||
		totalDistance > 1.1*float32(body.MaximumLength) {
		t.Errorf("Received course does not respect distance criteria. Expected %v, Received %v",
			body.MaximumLength, totalDistance)
	}

	restaurants := extractRestaurants(receivedCourse)
	if len(restaurants) == 0 {
		t.Errorf("Path should have restaurants, received 0")
	}

	for _, restau := range restaurants {
		if restau.Type != RESTAURANT_TYPE2 && restau.Type != RESTAURANT_TYPE1 {
			t.Errorf("Received restaurant of invalid type. Received tpe %s, but expected [%s, %s]",
				restau.Type, RESTAURANT_TYPE1, RESTAURANT_TYPE2)
		}
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}

func TestARequestToTheParcoursRouteNoStops(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	startPoint := payload.Geometry{
		Type:        "Point",
		Coordinates: [2]float32{46.103935, -71.307332},
	}

	body := payload.CourseRequest{
		StartingPoint: startPoint,
		MaximumLength: 1100,
		NumberOfStops: 0,
		Type:          []string{RESTAURANT_TYPE1, RESTAURANT_TYPE2},
	}
	jsonValue, _ := json.Marshal(body)

	req, err := http.NewRequest("GET", PARCOURS_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Course)
	handler.ServeHTTP(rr, req)

	var receivedCourse payload.GeoJson

	err = json.NewDecoder(rr.Body).Decode(&receivedCourse)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if receivedCourse.Type != "FeatureCollection" {
		t.Errorf("received geometry should be FeatureCollection GeoJSON")
	}

	totalDistance := extractTotalDistance(receivedCourse)

	if totalDistance < 0.9*float32(body.MaximumLength) ||
		totalDistance > 1.1*float32(body.MaximumLength) {
		t.Errorf("Received course does not respect distance criteria. Expected %v, Received %v",
			body.MaximumLength, totalDistance)
	}

	restaurants := extractRestaurants(receivedCourse)
	if len(restaurants) != 0 {
		t.Errorf("Path should have 0 restaurants, received %d", len(restaurants))
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}

func TestARequestToTheParcoursRouteWithoutLength(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	startPoint := payload.Geometry{
		Type:        "Point",
		Coordinates: [2]float32{46.103935, -71.307332},
	}

	body := payload.CourseRequest{
		StartingPoint: startPoint,
		MaximumLength: 0,
		NumberOfStops: 2,
		Type:          []string{RESTAURANT_TYPE1, RESTAURANT_TYPE2},
	}
	jsonValue, _ := json.Marshal(body)

	req, err := http.NewRequest("GET", PARCOURS_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Course)
	handler.ServeHTTP(rr, req)

	var receivedError payload.BadRequestParameterBody

	err = json.NewDecoder(rr.Body).Decode(&receivedError)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status == http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}

func TestARequestToTheParcoursRouteWithInvalidTypes(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	startPoint := payload.Geometry{
		Type:        "Point",
		Coordinates: [2]float32{46.103935, -71.307332},
	}

	body := payload.CourseRequest{
		StartingPoint: startPoint,
		MaximumLength: 11000,
		NumberOfStops: 2,
		Type:          []string{RESTAURANT_TYPE1, RESTAURANT_TYPE2, "757sdasdawer"},
	}
	jsonValue, _ := json.Marshal(body)

	req, err := http.NewRequest("GET", PARCOURS_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Course)
	handler.ServeHTTP(rr, req)

	var receivedError payload.BadRequestParameterBody

	err = json.NewDecoder(rr.Body).Decode(&receivedError)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status == http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}

func TestARequestToTheParcoursRouteWithEmptyBody(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("GET", PARCOURS_ROUTE, http.NoBody)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Course)
	handler.ServeHTTP(rr, req)

	var receivedError payload.BadRequestParameterBody

	err = json.NewDecoder(rr.Body).Decode(&receivedError)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status == http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}

func extractTotalDistance(geoJson payload.GeoJson) float32 {
	var totalDistance float32 = 0.0
	for _, feature := range geoJson.Features {
		if feature.Geometry.Type == MULTI_LINE {
			if prop, ok := feature.Properties["length"]; ok {
				totalDistance += float32(prop.(float64))
			}
		}
	}

	return totalDistance
}

func extractRestaurants(geoJson payload.GeoJson) []domain.Restaurant {
	var restaurants []domain.Restaurant
	for _, feature := range geoJson.Features {
		if feature.Geometry.Type == POINT {
			if _, ok := feature.Properties["name"]; ok {
				var name string
				if val, ok := feature.Properties["name"]; ok {
					name = val.(string)
				}

				var typeRestau string
				if val, ok := feature.Properties["type"]; ok {
					typeRestau = val.(string)
				}

				restaurants = append(restaurants, domain.Restaurant{
					Name: name,
					Type: typeRestau,
				})
			}
		}
	}

	return restaurants
}
