package e2e

import (
	"fmt"
	"os"
	"reflect"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/config"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/routeGraphRepository/neo"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository/mongo"
)

func TestETL(t *testing.T) {
	if err := CleanDatabases(); err != nil {
		fmt.Printf("was not able to clean databases\n%s", err.Error())
		closeRepositories()
		os.Exit(2)
	}
	t.Log(config.ProvideConfig())
	if err := etl.ETL(); err != nil {
		t.Fatalf("ETL failed\n`%s`", err.Error())
	}
	isReflectedInRestaurantRepository(expectedRestaurants(), t)
	isReflectedInGraphRepository(expectedRestaurants(), t)
	isReflectedInRouteRepository(expectedCyclingPath(), t)

	if err := CleanDatabases(); err != nil {
		fmt.Printf("was not able to clean databases\n%s", err.Error())
		closeRepositories()
		os.Exit(2)
	}

}

func expectedRestaurants() []domain.Restaurant {
	return []domain.Restaurant{
		{
			Name:        "An Phú - Restaurant vietnamien",
			Id:          "131",
			Type:        "Saveurs du monde",
			Coordinates: [2]float32{-71.86472400000002, 45.4080776},
			CalorieLevel: 856,
		},
		{
			Name:        "Antidote FoodLab",
			Id:          "126",
			Type:        "Bonnes tables",
			Coordinates: [2]float32{-71.89704999999998, 45.399098},
			CalorieLevel: 600,
		},
	}
}

func expectedCyclingPath() []domain.CyclePath {
	return []domain.CyclePath{
		{
			Name:   "Réseau utilitaire",
			Id:     "1",
			Length: 325.2,
			Nodes: []domain.CyclePathNode{
				{
					Coordinates: [2]float64{-1.1, 4.4},
				},
				{
					Coordinates: [2]float64{-7.1, 4.4},
				},
			},
		},
		{
			Name:   "Axe de la Saint-François",
			Id:     "2",
			Length: 620.7,
			Nodes: []domain.CyclePathNode{
				{
					Coordinates: [2]float64{-1, 2},
				},
				{
					Coordinates: [2]float64{-3, 4},
				},
			},
		},
	}
}
func isReflectedInRestaurantRepository(expectedRestaurants []domain.Restaurant, t *testing.T) {
	repoRestaurant, err := mongo.ProvideRestaurantRepository()
	if err != nil {
		t.Fatalf("was not able to instanciate the restaurant repository\n`%s`", err.Error())
	}
	restaurantsFromRestaurantRepo, err := repoRestaurant.All()
	if err != nil {
		t.Fatalf("was not able to do `All` method of restaurant repository\n`%s`", err.Error())
	}

	if !sliceEqualDishorder(restaurantsFromRestaurantRepo, expectedRestaurants) {
		t.Errorf("the values from the  json don't reflect the restaurant repository \n expected: %v \n returned: %v", expectedRestaurants, restaurantsFromRestaurantRepo)

	}
}

// cycling path to be tested
func isReflectedInGraphRepository(expectedRestaurants []domain.Restaurant, t *testing.T) {
	repoGraph, err := neo.ProvideRouteGraph()
	if err != nil {
		t.Fatalf("was not able to instanciate the grah repository\n`%s`", err.Error())
	}

	restaurantFromRepo, err := repoGraph.AllRestaurants()
	if err != nil {
		t.Fatalf("was not able to do `AllRestaurants` method of graph repository\n`%s`", err.Error())
	}
	if !sliceEqualDishorder(restaurantFromRepo, expectedRestaurants) {
		t.Errorf("the values from the  json don't reflect the graph repository \n expected: %v \n returned: %v", expectedRestaurants, restaurantFromRepo)
	}
}

func isReflectedInRouteRepository(expectedCyclePaths []domain.CyclePath, t *testing.T) {
	repo, err := mongo.ProvideRouteRepository()
	if err != nil {
		t.Fatalf("was not able to instanciate the route repository\n`%s`", err.Error())
	}
	paths, err := repo.All()
	if err != nil {
		t.Fatalf("was not able to do `All` method of restaurant repository\n`%s`", err.Error())
	}
	if !reflect.DeepEqual(paths, expectedCyclePaths) {
		t.Errorf("the values from the  json don't reflect the graph repository \n expected: %v \n returned: %v", expectedCyclePaths, paths)
	}
}
func sliceEqualDishorder(subject []domain.Restaurant, expected []domain.Restaurant) bool {
	subjectMap := map[domain.Restaurant]domain.Restaurant{}
	expectedMap := map[domain.Restaurant]domain.Restaurant{}
	for _, restaurant := range subject {
		subjectMap[restaurant] = restaurant
	}
	for _, restaurant := range expected {
		expectedMap[restaurant] = restaurant
	}
	return restaurantsAreEqual(subjectMap, expectedMap)
}

func restaurantsAreEqual(subject, expected map[domain.Restaurant]domain.Restaurant) bool {
	sensibility := float32(0.001)
	for k, restaurant := range expected {
		testCase, exist := subject[k]
		if !exist {
			return false
		}
		if restaurant.Id != testCase.Id {
			return false
		}
		if restaurant.Name != testCase.Name {
			return false
		}
		if restaurant.Type != testCase.Type {
			return false
		}
		if restaurant.Latitude()-testCase.Latitude() >= sensibility {
			return false
		}
		if restaurant.Longitude()-testCase.Longitude() >= sensibility {
			return false
		}
	}
	return true
}
