package e2e

import (
	"fmt"
	"os"
	"testing"

	"github.com/spf13/viper"
	configRoot "gitlab.com/constraintAutomaton/velo-epicurien/api/config"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/graph"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/routeGraphRepository/neo"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository/mongo"
)

const FirstPathLength = 51.57
const SecondPathLength = 100.17
const FirstRestaurantCalorieLevel = 123
const SecondRestaurantCalorieLevel = 666

var AFirstRestaurant = domain.Restaurant{Id: "11111", Name: "foo1", Type: "bar1", Coordinates: [2]float32{1, 2}}
var ASecondRestaurant = domain.Restaurant{Id: "44444", Name: "foo2", Type: "bar1", Coordinates: [2]float32{3, 4}}
var AThirdRestaurant = domain.Restaurant{Id: "33333", Name: "foo3", Type: "bar2", Coordinates: [2]float32{4, 5}}
var AllRestaurantTypes = map[string]uint{AFirstRestaurant.Type: 2, AThirdRestaurant.Type: 1}
var RestaurantType = []string{"bar1", "bar2"}

var AFirstCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.784262, -71.286716}}
var ASecondCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.784365, -71.285707}}
var AThirdCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.783902, -71.286045}}
var ACyclePath = domain.CyclePath{Id: "7777", Name: "Sentier", Length: FirstPathLength, Nodes: []domain.CyclePathNode{AFirstCyclePathNode, ASecondCyclePathNode, AThirdCyclePathNode}}

var AFourthCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.784262, -71.286716}}
var AFifthCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.784365, -71.285707}}
var ASecondCyclePath = domain.CyclePath{Id: "8888", Name: "Adstock", Length: SecondPathLength, Nodes: []domain.CyclePathNode{AFourthCyclePathNode, AFifthCyclePathNode}}

var restaurant1ForCourse = domain.Restaurant{Id: "83", Name: "foo4", Type: "bar1", Coordinates: [2]float32{46.101794, -71.310988}, CalorieLevel: 5656}
var restaurant2ForCourse = domain.Restaurant{Id: "84", Name: "foo5", Type: "bar2", Coordinates: [2]float32{46.104014, -71.307428}, CalorieLevel: FirstRestaurantCalorieLevel}
var restaurant3ForCourse = domain.Restaurant{Id: "85", Name: "foo6", Type: "bar3", Coordinates: [2]float32{46.107813, -71.301570}, CalorieLevel: SecondRestaurantCalorieLevel}

var node1ForCourse = domain.CyclePathNode{Coordinates: [2]float64{46.101657, -71.310892}}
var node2ForCourse = domain.CyclePathNode{Coordinates: [2]float64{46.103935, -71.307332}}
var node3ForCourse = domain.CyclePathNode{Coordinates: [2]float64{46.107694, -71.301449}}
var node4ForCourse = domain.CyclePathNode{Coordinates: [2]float64{46.100595, -71.312230}}
var node5ForCourse = domain.CyclePathNode{Coordinates: [2]float64{46.109522, -71.298515}}
var fullCyclePathForCourse = domain.CyclePath{Id: "123456", Name: "Sentier",
	Nodes: []domain.CyclePathNode{node4ForCourse, node1ForCourse, node2ForCourse, node3ForCourse, node5ForCourse},
	Length: 1100,
}

var restaurant1startingPoint = domain.Restaurant{Id: "86", Name: "foo1", Type: "bar1", Coordinates: [2]float32{-71.310988, 46.101794}, CalorieLevel: 5656}
var restaurant2startingPoint = domain.Restaurant{Id: "87", Name: "foo2", Type: "bar2", Coordinates: [2]float32{-71.307428, 46.104014}, CalorieLevel: 123}
var restaurant3startingPoint = domain.Restaurant{Id: "88", Name: "foo3", Type: "bar2", Coordinates: [2]float32{-71.301570, 46.107813}, CalorieLevel: 4000}

var node1startingPoint = domain.CyclePathNode{Coordinates: [2]float64{-71.310892, 46.101657}}
var node2startingPoint = domain.CyclePathNode{Coordinates: [2]float64{-71.307332, 46.103935}}
var node3startingPoint = domain.CyclePathNode{Coordinates: [2]float64{-71.301449, 46.107694}}
var node4startingPoint = domain.CyclePathNode{Coordinates: [2]float64{-71.312230, 46.100595}}
var node5startingPoint = domain.CyclePathNode{Coordinates: [2]float64{-71.298515, 46.109522}}
var pathForStartingPoint = domain.CyclePath{Id: "654321", Name: "GRAND_LINE",
	Nodes: []domain.CyclePathNode{node1startingPoint, node2startingPoint, node3startingPoint, node4startingPoint, node5startingPoint},
	Length: 1100,
}

func TestMain(m *testing.M) {
	setEnv()
	checkModules()

	if err := CleanDatabases(); err != nil {
		fmt.Printf("was not able to clean databases\n%s", err.Error())
		closeRepositories()
		os.Exit(2)
	}

	fmt.Printf("\n\n\nPREPARED DBs FOR END TO END TESTING\n\n\n")
	code := m.Run()

	if err := CleanDatabases(); err != nil {
		fmt.Printf("was not able to clean databases\n%s", err.Error())
		closeRepositories()
		os.Exit(2)
	}
	closeRepositories()
	os.Exit(code)
}

func PopulateDatabasesAnalytics() error {

	restaurantRepo, err := mongo.ProvideRestaurantRepository()
	if err != nil {
		panic(fmt.Sprintf("was not able to connect to mongoTest\n%s", err.Error()))
	}
	routeRepo, err := mongo.ProvideRouteRepository()
	if err != nil {
		panic(fmt.Sprintf("was not able to connect to mongoTest\n%s", err.Error()))
	}

	translator, err := graph.ProvideTranslator()
	if err != nil {
		panic(fmt.Sprintf("was not able to connect to mongoTest\n%s", err.Error()))
	}

	err = restaurantRepo.Add(&AFirstRestaurant)
	if err != nil {
		fmt.Printf("\nCould not add restau with id: %s\n", AFirstRestaurant.Id)
		fmt.Println(err.Error())
		return err
	}
	err = restaurantRepo.Add(&ASecondRestaurant)
	if err != nil {
		fmt.Printf("\nCould not add restau with id: %s\n", ASecondRestaurant.Id)
		fmt.Println(err.Error())
		return err
	}
	err = restaurantRepo.Add(&AThirdRestaurant)
	if err != nil {
		fmt.Printf("\nCould not add restau with id: %s\n", AThirdRestaurant.Id)
		fmt.Println(err.Error())
		return err
	}

	err = routeRepo.Add(&ACyclePath)
	if err != nil {
		fmt.Printf("\nCould not add restau with id: %s\n", ACyclePath.Id)
		fmt.Println(err.Error())
		return err
	}

	err = routeRepo.Add(&ASecondCyclePath)
	if err != nil {
		fmt.Printf("\nCould not add restau with id: %s\n", ASecondCyclePath.Id)
		fmt.Println(err.Error())
		return err
	}

	err = translator.TranslateNewCyclePathEntries()
	if err != nil {
		fmt.Printf("\nCOULD NOT PERFORM TRANSLATE OF CYCLE PATHS\n")
		fmt.Println(err.Error())
		return err
	}

	err = translator.TranslateNewRestaurantEntries()
	if err != nil {
		fmt.Printf("\nCOULD NOT PERFORM TRANSLATE OF RESTAURANTS\n")
		fmt.Println(err.Error())
		return err
	}

	return nil
}

func PopulateDatabasesForCourse() error {

	restaurantRepo, err := mongo.ProvideRestaurantRepository()
	if err != nil {
		panic(fmt.Sprintf("was not able to connect to mongoTest\n%s", err.Error()))
	}
	routeRepo, err := mongo.ProvideRouteRepository()
	if err != nil {
		panic(fmt.Sprintf("was not able to connect to mongoTest\n%s", err.Error()))
	}

	translator, err := graph.ProvideTranslator()
	if err != nil {
		panic(fmt.Sprintf("was not able to connect to mongoTest\n%s", err.Error()))
	}

	err = restaurantRepo.AddMany([]domain.Restaurant{AFirstRestaurant,
		ASecondRestaurant, AThirdRestaurant, restaurant1ForCourse, restaurant2ForCourse, restaurant3ForCourse,
		restaurant1startingPoint, restaurant2startingPoint, restaurant3startingPoint})
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	err = routeRepo.AddMany([]domain.CyclePath{ACyclePath, ASecondCyclePath, fullCyclePathForCourse, pathForStartingPoint})
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	err = translator.TranslateNewCyclePathEntries()
	if err != nil {
		fmt.Printf("\nCOULD NOT PERFORM TRANSLATE OF CYCLE PATHS\n")
		fmt.Println(err.Error())
		return err
	}

	err = translator.TranslateNewRestaurantEntries()
	if err != nil {
		fmt.Printf("\nCOULD NOT PERFORM TRANSLATE OF RESTAURANTS\n")
		fmt.Println(err.Error())
		return err
	}

	return nil
}

func CleanDatabases() error {
	fmt.Printf("\n\n\nCLEAR END TO END TESTING DB\n\n\n")
	routeGraphRepo, err := neo.ProvideRouteGraph()
	if err != nil {
		panic(fmt.Sprintf("was not able to provide route graph repo\n%s", err.Error()))
	}
	restaurantRepo, err := mongo.ProvideRestaurantRepository()
	if err != nil {
		panic(fmt.Sprintf("was not able to connect to provide restaurant repo\n%s", err.Error()))
	}
	routeRepo, err := mongo.ProvideRouteRepository()
	if err != nil {
		panic(fmt.Sprintf("was not able to connect to provide route.mongo repo\n%s", err.Error()))
	}
	cacheRepo, err := mongo.ProvideCacheRepository()
	if err != nil {
		panic(fmt.Sprintf("was not able to connect to provide route.mongo repo\n%s", err.Error()))
	}

	err = routeGraphRepo.ClearCyclePaths()
	if err != nil {
		return err
	}
	err = routeGraphRepo.ClearRestaurants()
	if err != nil {
		return err
	}
	err = restaurantRepo.Clear()
	if err != nil {
		return err
	}
	err = routeRepo.Clear()
	if err != nil {
		return err
	}
	if err := cacheRepo.Clear(); err != nil {
		return err
	}

	return nil
}

func setEnv() {
	configNeo := configRoot.NeoDatabaseConfigurations{
		DBAddr:     "neo4j://neo4j:7687",
		DBUser:     "neo4j",
		DBPassword: "glo7035",
	}
	viper.Set("Neodatabase", configNeo)
	configMongo := configRoot.MongoDatabaseConfigurations{
		DBAddr: "mongodb://mongo:27017",
	}
	viper.Set("Mongodatabase", configMongo)
	viper.Set("LocalRestaurantPath", "./restaurants.json")
	viper.Set("TypeRestaurantDonneeQuebecPath", "./typesRestaurants.json")
	viper.Set("LocalCyclingRoutePath", "./cyclingPath.json")
	viper.Set("ReadmeFilePath", "./readme.md")
	viper.Set("RemoteRestaurantPath", "https://www.donneesquebec.ca/recherche/fr/dataset/61b5b4e9-d038-4995-b85a-de039dc1b06b/resource/386e62b2-47ae-43bc-a85a-efbcaf3130ba/download/restaurants.json")
	viper.Set("RemoteCyclingRoutePath", "http://donneesouvertes-sherbrooke.opendata.arcgis.com/datasets/e7b14d41a20c4aafa91ed4a9b99384c3_3.geojson?outSR={\"latestWkid\":32187,\"wkid\":32187}")

}

func checkModules() {
	_, err := neo.ProvideRouteGraph()
	if err != nil {
		panic(fmt.Sprintf("was not able to connect to neoTest\n%s", err.Error()))
	}
	_, err = mongo.ProvideRestaurantRepository()
	if err != nil {
		panic(fmt.Sprintf("was not able to connect to mongoTest\n%s", err.Error()))
	}
	_, err = graph.ProvideTranslator()
	if err != nil {
		panic(fmt.Sprintf("was not able to connect to mongoTest\n%s", err.Error()))
	}
	_, err = mongo.ProvideRouteRepository()
	if err != nil {
		panic(fmt.Sprintf("was not able to connect to mongoTest\n%s", err.Error()))
	}

}
func closeRepositories() {
	etl.CloseAllRepository()
}
