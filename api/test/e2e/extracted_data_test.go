package e2e

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/route"
)

const EXTRACTED_DATA_ROUTE = "/extracted_data"

func TestARequestToTheExtractedDataRoute(t *testing.T) {

	err := PopulateDatabasesAnalytics()
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("GET", EXTRACTED_DATA_ROUTE, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.ExtractedData)
	handler.ServeHTTP(rr, req)

	var receivedExtractedData payload.ExtractedDataBody

	err = json.NewDecoder(rr.Body).Decode(&receivedExtractedData)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if receivedExtractedData.NbRestaurants != 3 {
		t.Errorf("Received wrong number of restaurants. Expected 3, received %d", receivedExtractedData.NbRestaurants)
	}

	if receivedExtractedData.NbSegments != 2 {
		t.Errorf("Received wrong number of restaurants. Expected 0, received %d", receivedExtractedData.NbSegments)
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}
