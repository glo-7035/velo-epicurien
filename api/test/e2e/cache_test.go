package e2e

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/route"
)

const CACHE_ROUTE = "/admin/cache"

func TestARequestToCacheRepositoryWithEmptyRepository(t *testing.T) {
	err := CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("GET", CACHE_ROUTE, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Cache)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var receivedCacheResponse payload.CacheResponseBody

	err = json.NewDecoder(rr.Body).Decode(&receivedCacheResponse)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if len(receivedCacheResponse.CalorieResponses) != 0 || len(receivedCacheResponse.ParcoursResponses) != 0 {
		t.Fatalf("response not equal\nreceive:`%v`\nexpected:`%v`", receivedCacheResponse, payload.CacheResponseBody{})
	}
}

func TestARequestToCacheRepositoryWithPreviousRequestMade(t *testing.T) {
	err := CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}

	respCalorie, respParcours := makeRequestToRouteThatCacheData(t)

	req, err := http.NewRequest("GET", CACHE_ROUTE, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Cache)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var receivedCacheResponse payload.CacheResponseBody

	err = json.NewDecoder(rr.Body).Decode(&receivedCacheResponse)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	expectedCache := payload.CacheResponseBody{
		ParcoursResponses: []payload.GeoJson{respParcours},
		CalorieResponses:  []payload.CalorieResponseBody{respCalorie},
	}

	if len(receivedCacheResponse.CalorieResponses) != len(expectedCache.CalorieResponses) || len(receivedCacheResponse.ParcoursResponses) != len(expectedCache.ParcoursResponses) {
		t.Fatalf("response not equal\nreceive:`%v`\nexpected:`%v`", receivedCacheResponse, expectedCache)
	}
	_ = CleanDatabases()
}

func makeRequestToRouteThatCacheData(t *testing.T) (payload.CalorieResponseBody, payload.GeoJson) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	startPoint := payload.Geometry{
		Type:        "Point",
		Coordinates: [2]float32{46.103935, -71.307332},
	}

	reqCalorie := payload.CourseRequest{
		StartingPoint: startPoint,
		MaximumLength: 1100,
		NumberOfStops: 2,
		Type:          []string{RESTAURANT_TYPE1, RESTAURANT_TYPE2},
	}
	jsonValue, _ := json.Marshal(reqCalorie)

	req, err := http.NewRequest("GET", CALORIE_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Calories)
	handler.ServeHTTP(rr, req)

	var receivedCourseWithCalories payload.CalorieResponseBody

	err = json.NewDecoder(rr.Body).Decode(&receivedCourseWithCalories)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	reqParcours := payload.CourseRequest{
		StartingPoint: startPoint,
		MaximumLength: 1100,
		NumberOfStops: 2,
		Type:          []string{RESTAURANT_TYPE1, RESTAURANT_TYPE2},
	}
	jsonValue, _ = json.Marshal(reqParcours)

	req, err = http.NewRequest("GET", PARCOURS_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		_ = CleanDatabases()
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(route.Course)
	handler.ServeHTTP(rr, req)

	var receivedCourse payload.GeoJson

	err = json.NewDecoder(rr.Body).Decode(&receivedCourse)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}
	return receivedCourseWithCalories, receivedCourse
}
