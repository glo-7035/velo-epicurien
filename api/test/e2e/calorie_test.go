package e2e

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/route"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/service"
)

const CALORIE_ROUTE = "/calories"

func TestARequestToTheCaloriesRoute(t *testing.T) {
	err := CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}

	err = PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	startPoint := payload.Geometry{
		Type:        "Point",
		Coordinates: [2]float32{46.103935, -71.307332},
	}

	body := payload.CourseRequest{
		StartingPoint: startPoint,
		MaximumLength: 1100,
		NumberOfStops: 2,
		Type:          []string{RESTAURANT_TYPE1, RESTAURANT_TYPE2},
	}
	jsonValue, _ := json.Marshal(body)

	req, err := http.NewRequest("GET", CALORIE_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Calories)
	handler.ServeHTTP(rr, req)

	var receivedCourseWithCalories payload.CalorieResponseBody

	err = json.NewDecoder(rr.Body).Decode(&receivedCourseWithCalories)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if receivedCourseWithCalories.Course.Type != "FeatureCollection" {
		t.Errorf("received geometry should be FeatureCollection GeoJSON")
	}

	totalDistance := extractTotalDistance(receivedCourseWithCalories.Course)

	if totalDistance < 0.9*float32(body.MaximumLength) ||
		totalDistance > 1.1*float32(body.MaximumLength) {
		t.Errorf("Received course does not respect distance criteria. Expected %v, Received %v \nfrom `%v` ",
			body.MaximumLength, totalDistance, body)
	}

	restaurants := extractRestaurants(receivedCourseWithCalories.Course)
	if len(restaurants) == 0 {
		t.Errorf("Path should have restaurants, received 0")
	}

	for _, restau := range restaurants {
		if restau.Type != RESTAURANT_TYPE2 && restau.Type != RESTAURANT_TYPE1 {
			t.Errorf("Received restaurant of invalid type. Received tpe %s, but expected [%s, %s]",
				restau.Type, RESTAURANT_TYPE1, RESTAURANT_TYPE2)
		}
	}

	var expectedIntake float32 = FirstRestaurantCalorieLevel + SecondRestaurantCalorieLevel
	if receivedCourseWithCalories.CalorieIntake != expectedIntake {
		t.Errorf("received wrong calorie intake. Expected %v, but received %v", expectedIntake, receivedCourseWithCalories.CalorieIntake)
	}

	expectedOutput := service.CALORIES_PER_METER * totalDistance
	if receivedCourseWithCalories.CalorieOutput != expectedOutput {
		t.Errorf("received wrong calorie output. Expected %v, but received %v", expectedOutput, receivedCourseWithCalories.CalorieOutput)
	}

	expectedNet := expectedIntake - expectedOutput
	if receivedCourseWithCalories.CalorieNet != expectedNet {
		t.Errorf("received wrong calorie net. Expected %v, but received %v", expectedNet, receivedCourseWithCalories.CalorieNet)
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}

func TestARequestToTheCaloriesRouteNoStops(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	startPoint := payload.Geometry{
		Type:        "Point",
		Coordinates: [2]float32{46.103935, -71.307332},
	}

	body := payload.CourseRequest{
		StartingPoint: startPoint,
		MaximumLength: 1100,
		NumberOfStops: 0,
		Type:          []string{RESTAURANT_TYPE1, RESTAURANT_TYPE2},
	}
	jsonValue, _ := json.Marshal(body)

	req, err := http.NewRequest("GET", CALORIE_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Calories)
	handler.ServeHTTP(rr, req)

	var receivedCourseWithCalories payload.CalorieResponseBody

	err = json.NewDecoder(rr.Body).Decode(&receivedCourseWithCalories)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	if receivedCourseWithCalories.Course.Type != "FeatureCollection" {
		t.Errorf("received geometry should be FeatureCollection GeoJSON")
	}

	totalDistance := extractTotalDistance(receivedCourseWithCalories.Course)

	if totalDistance < 0.9*float32(body.MaximumLength) ||
		totalDistance > 1.1*float32(body.MaximumLength) {
		t.Errorf("Received course does not respect distance criteria. Expected %v, Received %v",
			body.MaximumLength, totalDistance)
	}

	restaurants := extractRestaurants(receivedCourseWithCalories.Course)
	if len(restaurants) != 0 {
		t.Errorf("Path should have 0 restaurants, received %d", len(restaurants))
	}

	var expectedIntake float32 = 0
	if receivedCourseWithCalories.CalorieIntake != expectedIntake {
		t.Errorf("received wrong calorie intake. Expected %v, but received %v", expectedIntake, receivedCourseWithCalories.CalorieIntake)
	}

	expectedOutput := service.CALORIES_PER_METER * totalDistance
	if receivedCourseWithCalories.CalorieOutput != expectedOutput {
		t.Errorf("received wrong calorie output. Expected %v, but received %v", expectedOutput, receivedCourseWithCalories.CalorieOutput)
	}

	expectedNet := expectedIntake - expectedOutput
	if receivedCourseWithCalories.CalorieNet != expectedNet {
		t.Errorf("received wrong calorie net. Expected %v, but received %v", expectedNet, receivedCourseWithCalories.CalorieNet)
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}

func TestARequestToTheCaloriesRouteWithoutLength(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	startPoint := payload.Geometry{
		Type:        "Point",
		Coordinates: [2]float32{46.103935, -71.307332},
	}

	body := payload.CourseRequest{
		StartingPoint: startPoint,
		MaximumLength: 0,
		NumberOfStops: 2,
		Type:          []string{RESTAURANT_TYPE1, RESTAURANT_TYPE2},
	}
	jsonValue, _ := json.Marshal(body)

	req, err := http.NewRequest("GET", CALORIE_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Course)
	handler.ServeHTTP(rr, req)

	var receivedError payload.BadRequestParameterBody

	err = json.NewDecoder(rr.Body).Decode(&receivedError)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status == http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}

func TestARequestToTheCaloriesRouteWithInvalidTypes(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	startPoint := payload.Geometry{
		Type:        "Point",
		Coordinates: [2]float32{46.103935, -71.307332},
	}

	body := payload.CourseRequest{
		StartingPoint: startPoint,
		MaximumLength: 11000,
		NumberOfStops: 2,
		Type:          []string{RESTAURANT_TYPE1, RESTAURANT_TYPE2, "757sdasdawer"},
	}
	jsonValue, _ := json.Marshal(body)

	req, err := http.NewRequest("GET", CALORIE_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Course)
	handler.ServeHTTP(rr, req)

	var receivedError payload.BadRequestParameterBody

	err = json.NewDecoder(rr.Body).Decode(&receivedError)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status == http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}

func TestARequestToTheCaloriesRouteWithEmptyBody(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("GET", CALORIE_ROUTE, http.NoBody)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Calories)
	handler.ServeHTTP(rr, req)

	var receivedError payload.BadRequestParameterBody

	err = json.NewDecoder(rr.Body).Decode(&receivedError)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status == http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}
