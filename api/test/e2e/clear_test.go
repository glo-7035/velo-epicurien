package e2e

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/routeGraphRepository/neo"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository/mongo"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/route"
)

const CLEAR_ROUTE = "/admin/clear"

func TestARequestToTheClearRouteWhenRepoIsEmpty(t *testing.T) {
	req, err := http.NewRequest("DELETE", CLEAR_ROUTE, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Clear)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	if empty, err := repositoryAreEmpty(); !empty || err != nil {
		t.Fatalf("the repo is not empty or/and\n`%v`", err)
	}
}
func TestARequestToClearRouteWhenRepoHaveData(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}
	req, err := http.NewRequest("DELETE", CLEAR_ROUTE, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Clear)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	if empty, err := repositoryAreEmpty(); !empty || err != nil {
		t.Fatalf("the repo is not empty or/and\n`%v`", err)
	}
	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}

}
func repositoryAreEmpty() (bool, error) {
	restaurantRepo, err := mongo.ProvideRestaurantRepository()
	if err != nil {
		return false, err
	}
	routeRepo, err := mongo.ProvideRouteRepository()
	if err != nil {
		return false, err
	}
	repoGraph, err := neo.ProvideRouteGraph()
	if err != nil {
		return false, err
	}
	if size, err := restaurantRepo.Size(); size != 0 || err != nil {
		return false, err
	}
	if size, err := routeRepo.Size(); size != 0 || err != nil {
		return false, err
	}
	paths, err := repoGraph.AllCyclePaths()
	if len(paths) != 0 || err != nil {
		return false, err
	}
	restaurants, err := repoGraph.AllRestaurants()
	if len(restaurants) != 0 || err != nil {
		return false, err
	}
	return true, nil
}
