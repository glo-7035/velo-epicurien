retourne le readme
`@GET /readme`

Cet appel permet à un utilisateur ou a une application d’obtenir tous les types de parcours disponibles. Il s’agit tout simplement de la liste des types de restaurants disponibles dans votre base de données

```
@GET /type

returns:
[
    str,
    str,
    str,
    ...
]
```