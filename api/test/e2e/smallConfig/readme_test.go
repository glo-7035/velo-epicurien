package smallConfig

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/spf13/viper"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/config"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/route"
)

var README_ROUTE = "/readme"

func TestARequestToTheReadmeRoute(t *testing.T) {
	viper.Set("ReadmeFilePath", "./readme.md")
	req, err := http.NewRequest("GET", README_ROUTE, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Readme)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if contentType := rr.Result().Header["Content-Type"]; contentType[0] != "text/markdown" {
		t.Errorf("handler returned wrong Content-Type: got %v want %v",
			contentType, "text/markdown")
	}

	if contentDisposition := rr.Result().Header["Content-Disposition"]; contentDisposition[0] != "attachment; filename=readme.md" {
		t.Errorf("handler returned wrong Content-Disposition : got %v want %v",
			contentDisposition, "text/markdown")
	}
	compareField(t, rr.Result())
}

func compareField(t *testing.T, resp *http.Response) {
	defer resp.Body.Close()
	defer os.Remove("test.md")

	expectedData, err := ioutil.ReadFile(config.ProvideConfig().ReadmeFilePath)
	if err != nil {
		t.Fatal(err)
	}
	file, err := os.Create("test.md")
	if err != nil {
		t.Fatal(err)
	}

	defer file.Close()

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		t.Fatal(err)
	}
	content, err := ioutil.ReadFile("test.md")
	data := string(content)
	if data != string(expectedData) {
		t.Fatalf("doesn't return the expected file\n expected: %v\n receive: %v", string(expectedData), string(data))
	}

}
