package smallConfig

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/route"
)

const HEARTBEAT_ROUTE = "/heartbeat"

func TestARequestToTheHeartbeatRoute(t *testing.T) {
	req, err := http.NewRequest("GET", HEARTBEAT_ROUTE, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.HeartBeat)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

}
