package e2e

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/route"
)

const TYPE_ROUTE = "/type"

func TestARequestToType(t *testing.T) {
	err := PopulateDatabasesAnalytics()
	if err != nil {
		t.Fatal(err)
	}
	req, err := http.NewRequest("GET", TYPE_ROUTE, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.Type)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	isValid(t, rr.Result())
	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}

}

func isValid(t *testing.T, resp *http.Response) {
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}
	val := []string{}
	if err := json.Unmarshal(body, &val); err != nil {
		t.Fatal(err)
	}

	if !sliceStringEqualDishorder(val, RestaurantType) {
		t.Fatalf("should be equal but \nreceive:`%v`\nexpected:`%v`", val, RestaurantType)
	}

}

func sliceStringEqualDishorder(subject, expected []string) bool {
	subjectMap := map[string]string{}
	expectedMap := map[string]string{}
	for _, v := range subject {
		subjectMap[v] = v
	}
	for _, v := range expected {
		expectedMap[v] = v
	}
	return reflect.DeepEqual(subjectMap, expectedMap)
}
