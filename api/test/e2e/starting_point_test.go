package e2e

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/route"
)

const STARTING_POINT_ROUTE = "/starting_point"

func TestARequestToTheStartingPointRoute(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	body := payload.StartingPointRequest{
		MaximumLength: 1100,
		Type:          []string{"bar1", "bar2"},
	}
	jsonValue, _ := json.Marshal(body)

	req, err := http.NewRequest("GET", STARTING_POINT_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.StartingPoint)
	handler.ServeHTTP(rr, req)

	var receivedStartingPoint payload.StartingPointResponse

	err = json.NewDecoder(rr.Body).Decode(&receivedStartingPoint)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if receivedStartingPoint.StartingPoint.Type != "Point" {
		t.Errorf("received geometry should be Point GeoJSON")
	}

	coord := receivedStartingPoint.StartingPoint.Coordinates.([]interface{})
	if len(coord) != 2 {
		t.Errorf("received coordinates should have length of 2")
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}

func TestARequestToTheStartingPointRouteWithNoTypes(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	body := payload.StartingPointRequest{
		MaximumLength: 1100,
		Type:          []string{},
	}
	jsonValue, _ := json.Marshal(body)

	req, err := http.NewRequest("GET", STARTING_POINT_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.StartingPoint)
	handler.ServeHTTP(rr, req)

	var receivedStartingPoint payload.StartingPointResponse

	err = json.NewDecoder(rr.Body).Decode(&receivedStartingPoint)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}

func TestARequestToTheStartingPointRouteWithNullTypes(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	body := payload.StartingPointRequest{
		MaximumLength: 1100,
	}
	jsonValue, _ := json.Marshal(body)

	req, err := http.NewRequest("GET", STARTING_POINT_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.StartingPoint)
	handler.ServeHTTP(rr, req)

	var receivedStartingPoint payload.StartingPointResponse

	err = json.NewDecoder(rr.Body).Decode(&receivedStartingPoint)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if receivedStartingPoint.StartingPoint.Type != "Point" {
		t.Errorf("received geometry should be Point GeoJSON")
	}

	coord := receivedStartingPoint.StartingPoint.Coordinates.([]interface{})
	if len(coord) != 2 {
		t.Errorf("received coordinates should have length of 2")
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}

func TestARequestToTheStartingPointRouteWith0Length(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	body := payload.StartingPointRequest{
		MaximumLength: 0,
		Type:          []string{"bar1", "bar2"},
	}
	jsonValue, _ := json.Marshal(body)

	req, err := http.NewRequest("GET", STARTING_POINT_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.StartingPoint)
	handler.ServeHTTP(rr, req)

	var receivedError payload.BadRequestParameterBody

	err = json.NewDecoder(rr.Body).Decode(&receivedError)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status == http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}
}

func TestARequestToTheStartingPointRouteWithWrongType(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	body := payload.StartingPointRequest{
		MaximumLength: 1100,
		Type:          []string{"bar1", "bar2", "484987asdadasd"},
	}
	jsonValue, _ := json.Marshal(body)

	req, err := http.NewRequest("GET", STARTING_POINT_ROUTE, bytes.NewBuffer(jsonValue))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.StartingPoint)
	handler.ServeHTTP(rr, req)

	var receivedError payload.BadRequestParameterBody

	err = json.NewDecoder(rr.Body).Decode(&receivedError)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status == http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}

}

func TestARequestToTheStartingPointRouteWithEmptyBody(t *testing.T) {
	err := PopulateDatabasesForCourse()
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("GET", STARTING_POINT_ROUTE, http.NoBody)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(route.StartingPoint)
	handler.ServeHTTP(rr, req)

	var receivedError payload.BadRequestParameterBody

	err = json.NewDecoder(rr.Body).Decode(&receivedError)
	if err != nil {
		t.Errorf("cannot deserialize received body")
	}

	if status := rr.Code; status == http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	err = CleanDatabases()
	if err != nil {
		t.Fatal(err)
	}

}
