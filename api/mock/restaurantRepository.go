package mock

import "gitlab.com/constraintAutomaton/velo-epicurien/api/domain"

type RestaurantRepository struct {
	HasCalled map[string]uint
}

func ProvideRestaurantRepository() RestaurantRepository {
	return RestaurantRepository{HasCalled: map[string]uint{}}
}

func (r *RestaurantRepository) Add(*domain.Restaurant) error {
	return nil
}

func (r *RestaurantRepository) AddMany([]domain.Restaurant) error {
	r.HasCalled["AddMany"] += 1
	return nil
}

func (r *RestaurantRepository) Ping() error {
	return nil
}
func (r *RestaurantRepository) Size() (uint, error) {
	return 0, nil
}
func (r *RestaurantRepository) RestaurantsTypes() (map[string]uint, error) {
	return nil, nil
}
func (r *RestaurantRepository) All() ([]domain.Restaurant, error) {
	r.HasCalled["All"] += 1
	return []domain.Restaurant{AFirstRestaurant, ASecondRestaurant, AThirdRestaurant}, nil
}
func (r *RestaurantRepository) Clear() error {
	return nil
}

func (r *RestaurantRepository) Close() {}
