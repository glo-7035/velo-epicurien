package mock

import "gitlab.com/constraintAutomaton/velo-epicurien/api/domain"

type RouteRepository struct {
	HasCalled map[string]uint
}

func (r *RouteRepository) Add(*domain.CyclePath) error {
	return nil
}

func (r *RouteRepository) AddMany([]domain.CyclePath) error {
	r.HasCalled["AddMany"] += 1
	return nil
}
func (r *RouteRepository) All() ([]domain.CyclePath, error) {
	r.HasCalled["All"] += 1
	return []domain.CyclePath{ACyclePath, ASecondCyclePath}, nil
}
func (r *RouteRepository) Clear() error {
	return nil
}
func (r *RouteRepository) Ping() error {
	return nil
}
func (r *RouteRepository) Size() (uint, error) {
	return 0, nil
}

func (r *RouteRepository) Close() {}
func (r *RouteRepository) TotalLength() (float64, error) {
	return 0, nil
}
