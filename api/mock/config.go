package mock

import "gitlab.com/constraintAutomaton/velo-epicurien/api/domain"

var AFirstRestaurant = domain.Restaurant{Id: "4567", Name: "foo1", Type: "bar1", Coordinates: [2]float32{1, 2}}
var ASecondRestaurant = domain.Restaurant{Name: "foo2", Type: "bar1", Coordinates: [2]float32{3, 4}}
var AThirdRestaurant = domain.Restaurant{Name: "foo3", Type: "bar2", Coordinates: [2]float32{4, 5}}

var AFirstCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.784262, -71.286716}}
var ASecondCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.784365, -71.285707}}
var AThirdCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.783902, -71.286045}}
var ACyclePath = domain.CyclePath{Id: "7777", Name: "Sentier", Nodes: []domain.CyclePathNode{AFirstCyclePathNode, ASecondCyclePathNode, AThirdCyclePathNode}}

var AFourthCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.784262, -71.286716}}
var AFifthCyclePathNode = domain.CyclePathNode{Coordinates: [2]float64{46.784365, -71.285707}}
var ASecondCyclePath = domain.CyclePath{Id: "8888", Name: "Adstock", Nodes: []domain.CyclePathNode{AFourthCyclePathNode, AFifthCyclePathNode}}
