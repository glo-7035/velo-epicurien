package mock

import (
	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
)

type GraphRepository struct {
	HasCalled map[string]uint
}

func ProvideRouteReposity() *GraphRepository {
	return &GraphRepository{HasCalled: map[string]uint{}}
}

func (r *GraphRepository) AddCyclePath(cyclePath *domain.CyclePath) error {
	r.HasCalled["AddCyclePath"] += 1
	return nil
}

func (r *GraphRepository) AddAndConnectManyRestaurant(restaurants []domain.Restaurant) error {
	r.HasCalled["AddAndConnectManyRestaurant"] += 1
	return nil
}

func (r *GraphRepository) AllRestaurants() ([]domain.Restaurant, error) {
	r.HasCalled["AllRestaurants"] += 1
	return []domain.Restaurant{AFirstRestaurant}, nil
}

func (r *GraphRepository) AllCyclePaths() ([]domain.CyclePath, error) {
	r.HasCalled["AllCyclePaths"] += 1
	return []domain.CyclePath{ACyclePath}, nil
}

func (r *GraphRepository) AddAndConnectRestaurant(*domain.Restaurant) error {
	r.HasCalled["AddAndConnectRestaurant"] += 1
	return nil
}


func (r *GraphRepository) GenerateCyclingCourse(startingPoint *domain.CyclePathNode, maxLength int, numberOfStops int, types []string) (domain.CyclePath, []domain.Restaurant, error) {
	r.HasCalled["GenerateCyclingCourse"] += 1
	return domain.CyclePath{}, nil, nil
}

func (r *GraphRepository) ClearRestaurants() error {
	return nil
}

func (r *GraphRepository) ClearCyclePaths() error {
	return nil
}

func (r *GraphRepository) Clear() error {
	return nil
}

func (r *GraphRepository) Close() {
}

func (r *GraphRepository) IsRestaurantConnectedToCyclePath(restaurant *domain.Restaurant, path *domain.CyclePath) (bool, error) {
	return false, nil
}

func (r *GraphRepository)  GetCourseDistance(course *domain.CyclePath, start *domain.CyclePathNode) (float64, error) {
	return 0, nil
}

func (r *GraphRepository) GetStartingPointBestCandidate(maxLength int, types []string) (domain.CyclePathNode, error) {
	return domain.CyclePathNode{}, nil
}

func (r *GraphRepository) GetCyclePathForNode(node domain.CyclePathNode) (domain.CyclePath, error) {
	return domain.CyclePath{}, nil
}
