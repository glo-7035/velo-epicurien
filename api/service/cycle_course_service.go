package service

import (
	"errors"
	"fmt"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/routeGraphRepository/neo"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository/mongo"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
)

const CALORIES_PER_METER = 0.0298

func FindBestMatchForStartingPoint(request payload.StartingPointRequest) (domain.CyclePathNode, error) {

	var err error
	acceptedTypes := request.Type
	if len(acceptedTypes) == 0 {
		acceptedTypes, err = GetRestaurantsTypes()
		if err != nil {
			return domain.CyclePathNode{}, err
		}
	}

	graph, err := neo.ProvideRouteGraph()
	if err != nil {
		return domain.CyclePathNode{}, err
	}


	fmt.Printf("FETCHING STARTING POINT\n")
	startingPoint, err := graph.GetStartingPointBestCandidate(request.MaximumLength, acceptedTypes)
	if err != nil {
		return domain.CyclePathNode{}, err
	}

	return startingPoint, nil
}

func ParcoursResponse(request payload.CourseRequest) (payload.GeoJson, error) {
	cache, errRepoCreated := mongo.ProvideCacheRepository()
	if errRepoCreated == nil {
		resp, err := cache.GetParcoursResponse(request)
		if err == nil {
			return resp, nil
		}
	}
	path, restaurants, err := GenerateCycleCourse(request)
	if err != nil {
		return payload.GeoJson{}, err
	}
	response := payload.ToGeoJson(path, restaurants)
	if errRepoCreated == nil {
		err = cache.AddParcoursResponseIfNotExist(request, response)
		if err == nil {
			fmt.Printf("parcours resp cache \nresp:%v\nreq:%v\n", response, request)
		}
	}
	return response, nil
}

func CalorieResponse(request payload.CourseRequest) (payload.CalorieResponseBody, error) {
	cache, errRepoCreated := mongo.ProvideCacheRepository()
	if errRepoCreated == nil {
		resp, err := cache.GetCalorieResponse(request)
		if err == nil {
			return resp, nil
		}
	}

	path, restaurants, err := GenerateCycleCourse(request)
	if err != nil {
		return payload.CalorieResponseBody{}, err
	}

	responseFeature := payload.ToGeoJson(path, restaurants)
	calorieIntake, calorieOutput := CalculateCalories(path, restaurants)

	calorieResponse := payload.CalorieResponseBody{
		CalorieIntake: calorieIntake,
		CalorieOutput: calorieOutput,
		CalorieNet:    calorieIntake - calorieOutput,
		Course:        responseFeature,
	}

	if errRepoCreated == nil {
		err = cache.AddCalorieResponseIfNotExist(request, calorieResponse)
		if err == nil {
			fmt.Printf("calorie resp cache \nresp:%v\nreq:%v\n", calorieResponse, request)
		}

	}
	return calorieResponse, nil

}
func GenerateCycleCourse(request payload.CourseRequest) (domain.CyclePath, []domain.Restaurant, error) {

	acceptedTypes, err := getAcceptedTypes(request.Type)
	if err != nil {
		return domain.CyclePath{}, nil, err
	}

	graph, err := neo.ProvideRouteGraph()
	if err != nil {
		return domain.CyclePath{}, nil, err
	}

	nodeStartingPoint, err := convertToCycleNode(request.StartingPoint)
	if err != nil {
		return domain.CyclePath{}, nil, err
	}

	path, restaurants, err := graph.GenerateCyclingCourse(&nodeStartingPoint, request.MaximumLength, request.NumberOfStops, acceptedTypes)
	if err != nil {
		return domain.CyclePath{}, nil, err
	}

	return path, restaurants, nil
}

func CalculateCalories(path domain.CyclePath, restaurants []domain.Restaurant) (float32, float32) {

	calorieOutput := path.Length * CALORIES_PER_METER

	var calorieInput float32 = 0.0
	for _, restaurant := range restaurants {
		calorieInput += float32(restaurant.CalorieLevel)
	}

	return calorieInput, calorieOutput
}

func convertToCycleNode(point payload.Geometry) (domain.CyclePathNode, error) {

	castedCoordinates := point.Coordinates.([]interface{})
	if len(castedCoordinates) != 2 {
		return domain.CyclePathNode{}, errors.New("number of coordiantes for point is invalid")
	}
	var coordinates [2]float64
	for i := range castedCoordinates {
		coordinates[i] = castedCoordinates[i].(float64)
	}

	return domain.CyclePathNode{
		Coordinates: coordinates,
	}, nil
}

func getAcceptedTypes(requestAcceptedTypes []string) ([]string, error) {
	var err error
	acceptedTypes := requestAcceptedTypes
	if len(requestAcceptedTypes) == 0 {
		acceptedTypes, err = GetRestaurantsTypes()
		if err != nil {
			return nil, err
		}
	}

	return acceptedTypes, nil
}

func IsValidRestaurantTypes(types []string) (bool, error) {
	acceptedTypes, err := GetRestaurantsTypes()
	if err != nil {
		return false, err
	}

	for _, typeRestaurant := range types {
		if !contains(acceptedTypes, typeRestaurant) {
			return false, nil
		}
	}

	return true, nil
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
