package service

import (
	"encoding/json"
	"fmt"
	"math"
	"net/http"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
)

var MAX_LENGTH_ACC = float32(3897)

type startingPointRequestHelper struct {
	MaximumLength interface{} `json:"maximumLength"`
	Type          interface{} `json:"type"`
}

type courseRequestHelper struct {
	StartingPoint interface{} `json:"startingPoint,omitempty"`
	MaximumLength interface{} `json:"maximumLength,omitempty"`
	NumberOfStops interface{} `json:"numberOfStops,omitempty"`
	Type          interface{} `json:"type,omitempty"`
}

func StartingPointError(body []byte) (payload.BadRequestParameterBody, error) {
	msg := startingPointRequestHelper{}
	err := json.Unmarshal(body, &msg)
	if err != nil {
		return payload.BadRequestParameterBody{}, err
	}

	resp := payload.BadRequestParameterBody{ErrorMessage: "the error(s) are/is"}
	if max, ok := msg.MaximumLength.(float64); ok {
		if max <= 0 {
			resp.ErrorMessage += ";" + payload.INVALID_LENGTH_MESSAGE
		}
		if math.Mod(max, 1) != 0 {
			resp.ErrorMessage += ";" + "maximumLength should be an integer"
		}

	} else {
		resp.ErrorMessage += ";" + payload.INVALID_LENGTH_MESSAGE

	}

	if sliceType, ok := msg.Type.([]interface{}); ok {
		if len(sliceType) > 0 {

			for _, typeResto := range sliceType {
				sliceType := []string{}

				if v, ok := typeResto.(string); !ok {
					resp.ErrorMessage += ";" + payload.INVALID_TYPE_MESSAGE
					break
				} else {
					sliceType = append(sliceType, v)
				}
				if ok, _ := IsValidRestaurantTypes(sliceType); !ok {
					resp.ErrorMessage += ";" + payload.INVALID_TYPE_MESSAGE

				}
			}
		}

	} else {
		resp.ErrorMessage += ";" + payload.INVALID_TYPE_MESSAGE
	}

	return resp, nil
}

func IsValidCycleLength(length int) bool {
	MAX_LENGTH_ACC, _ = GetRouteGraphTotalLength()
	return float32(length) < MAX_LENGTH_ACC
}

func ParcoursError(body []byte) (payload.BadRequestParameterBody, int, error) {
	MAX_LENGTH_ACC, _ = GetRouteGraphTotalLength()

	msg := courseRequestHelper{}
	err := json.Unmarshal(body, &msg)
	httpError := http.StatusBadRequest
	if err != nil {
		return payload.BadRequestParameterBody{}, 0, err
	}
	resp := payload.BadRequestParameterBody{ErrorMessage: "the error(s) are/is"}

	if max, ok := msg.MaximumLength.(float64); ok {
		if max <= 0 {
			resp.ErrorMessage += ";" + payload.INVALID_LENGTH_MESSAGE
		}
		if math.Mod(max, 1) != 0 {
			resp.ErrorMessage += ";" + "maximumLength should be an integer"
		}
		if float32(max) > MAX_LENGTH_ACC {
			resp.ErrorMessage += ";" + fmt.Sprintf("maximumLength should less than %v", MAX_LENGTH_ACC)
			return resp, http.StatusNotFound, nil
		}
	} else {
		resp.ErrorMessage += ";" + payload.INVALID_LENGTH_MESSAGE

	}

	if numStop, ok := msg.NumberOfStops.(float64); ok {
		if numStop <= 0 {
			resp.ErrorMessage += ";" + payload.INVALID_NUMBER_STOP
		}
		if math.Mod(numStop, 1) != 0 {
			resp.ErrorMessage += ";" + "numberOfStops should be an integer"
		}
	} else {
		resp.ErrorMessage += ";" + payload.INVALID_NUMBER_STOP

	}

	if sliceType, ok := msg.Type.([]interface{}); ok {
		if len(sliceType) > 0 {

			for _, typeResto := range sliceType {
				sliceType := []string{}

				if v, ok := typeResto.(string); !ok {
					resp.ErrorMessage += ";" + payload.INVALID_TYPE_MESSAGE
					break
				} else {
					sliceType = append(sliceType, v)
				}
				if ok, _ := IsValidRestaurantTypes(sliceType); !ok {
					httpError = http.StatusNotFound
					resp.ErrorMessage += ";" + payload.INVALID_TYPE_MESSAGE

				}
			}
		}

	} else {
		resp.ErrorMessage += ";" + payload.INVALID_TYPE_MESSAGE
	}
	isValidStartingPoint := true
	if v, ok := msg.StartingPoint.(map[string]interface{}); ok {
		if typeGeo, ok := v["type"]; ok {
			if typeString, ok := typeGeo.(string); ok {
				if typeString != "Point" {
					isValidStartingPoint = false
				}
			} else {
				isValidStartingPoint = false

			}
			if _, ok := v["coordinates"].([]float64); !ok {
				isValidStartingPoint = false
			}
		} else {
			isValidStartingPoint = false

		}
	} else {
		isValidStartingPoint = false
	}
	if !isValidStartingPoint {
		resp.ErrorMessage += ";" + payload.STARTING_POINT_INVALID

	}
	return resp, httpError, nil
}
