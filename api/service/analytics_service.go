package service

import (
	"fmt"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository/mongo"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
)

func GetTotalPathCount() (int, error) {
	routeRepo, err := mongo.ProvideRouteRepository()
	if err != nil {
		return 0, err
	}

	count, err := routeRepo.Size()
	if err != nil {
		return 0, err
	}

	return int(count), nil
}

func GetRouteGraphTotalLength() (float32, error) {
	routeRepo, err := mongo.ProvideRouteRepository()
	if err != nil {
		return 0, err
	}

	totalLength, err := routeRepo.TotalLength()
	if err != nil {
		return 0, err
	}

	return float32(totalLength), nil
}

func GetTotalRestaurantCount() (int, error) {
	restaurant, err := mongo.ProvideRestaurantRepository()
	if err != nil {
		return 0, err
	}

	size, err := restaurant.Size()
	if err != nil {
		return 0, err
	}
	return int(size), nil
}

func GetNumberOfRestaurantByTypes() (map[string]uint, error) {
	restaurant, err := mongo.ProvideRestaurantRepository()
	if err != nil {
		return nil, err
	}

	allTypes, err := restaurant.RestaurantsTypes()
	if err != nil {
		return nil, err
	}

	return allTypes, nil
}

func GetRestaurantsTypes() ([]string, error) {
	numberRestaurantByType, err := GetNumberOfRestaurantByTypes()
	if err != nil {
		return nil, err
	}
	restaurantsType := make([]string, 0, len(numberRestaurantByType))
	for k := range numberRestaurantByType {
		restaurantsType = append(restaurantsType, k)
	}
	return restaurantsType, nil
}

func GetCache() (payload.CacheResponseBody, error) {
	cache, err := mongo.ProvideCacheRepository()
	if err != nil {
		return payload.CacheResponseBody{}, fmt.Errorf("the cache repository is not available")
	}

	parcours, calorie, err := cache.GetAllResponse()
	if err != nil {
		return payload.CacheResponseBody{}, fmt.Errorf("the cache repository is not available")
	}
	fmt.Printf("here!\n%v\n%v\n", parcours, calorie)
	return payload.CacheResponseBody{
		ParcoursResponses: parcours,
		CalorieResponses:  calorie,
	}, nil
}
