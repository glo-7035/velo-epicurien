package service

import (
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository/mongo"
)

func IncrementalETL() error {
	return etl.RemoteETL()
}

func Clear() error {
	return etl.ClearAllRepository()
}

func ClearCache() error {
	cache, err := mongo.ProvideCacheRepository()
	if err != nil {
		return err
	}
	return cache.Clear()
}
