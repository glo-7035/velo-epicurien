# Parcours de vélo épicurien

## Readme

retourne le readme (peut être telecharger)
`@GET /readme`
## Heartbeat

Cet appel permet à un utilisateur ou a une application cliente de vérifier que l'application est up.

```
@GET /heartbeat

returns:
{
    "villeChoisie": str
}
```

## Extracted data

Cet appel permet à un utilisateur ou a une application cliente d'obtenir le nombre de restaurants et le nombre de pistes cyclables que l'application utilise

```
@GET /extracted_data

returns:
{
    "nbRestaurants":int,
    "nbSegments":int
}
```

## Transformed data

Cet appel permet à un utilisateur ou a une application cliente d'obtenir:

- l’objet restaurants qui contient le nombre de restaurant par type dans votre BD de points de restaurants transformés
- la valeur numérique longueurCyclable qui contient la longueur totale des chemins pouvant être utilisés dans votre application

```
@GET /transformed_data

returns:
{
    "restaurants":{
        $type1: int,
        $type2: int,
        ...
    },
    "longueurCyclable":float
}
```

## Type

Cet appel permet à un utilisateur ou a une application d’obtenir tous les types de parcours disponibles. Il s’agit tout simplement de la liste des types de restaurants disponibles dans votre base de données

```
@GET /type

returns:
[
    str,
    str,
    str,
    ...
]
```

## Point de départ

Cet appel permet à un utilisateur ou une application cliente d’obtenir un point de départ aléatoire:

d’un trajet d’une longueur maximum_length ± 10%
comprenant des restaurants inclus dans les types définis dans le tableau type et
si le tableau type est vide, on assume que tous les types sont possibles
Le point de départ est un objet géographique de type GeoPoint

distance proposée pour les tests
`maximumLenghts = [1000, 12500, 1500, 2000, 3000, 3500]`


```
@GET /starting_point (avec le payload):
{
    "maximumLength": int (en mètre),
    "type": [str, str, ... ]
}

returns:
{
    "startingPoint" : {"type":"Point", "coordinates":[float, float]}
}
```

## Générer un parcours

Cet appel permet à un utilisateur ou une application cliente d’obtenir:

    - un trajet partant d’un point dans un rayon de 500m du point startingPoint
    - le trajet obtenu est d’une longueur de maximumLength ± 10%
    - le trajet à au plus (et de préférence) numberOfStops arrets
    - qui sont des restaurants inclus dans les types définis dans le tableau type et
    - si le tableau type est vide, on assume que tous les types sont possibles

```
@GET /parcours (avec le payload):
{
    "startingPoint" : {"type":"Point", "coordinates":[float, float]},
    "maximumLength": int (en mètre),
    "numberOfStops": int,
    "type": [str, str, ... ]
}

returns:
{
    "type": "FeatureCollection",
    "features": [
        {
            "type":"Feature",
            "geometry":{
                "type": "Point",
                "coordinates":  [float, float]
            },
            "properties":{
                "name":str,
                "type":str
            }
        }, ..., {
            "type":"Feature",
            "geometry":{
                "type": "MultiLineString",
                "coordinates": [[
                     [float, float],  [float, float],  [float, float], ...
                    ]]
            },
            "properties":{
                "length":float
            }
        }
    ]
}
```

## Fonctionnalité bonus

Un cache des réponses a été fait de sorte qu'une requête fait dans les routes `/parcours` et `/calorie` faite une seconde fois n'est pas recalculée et la réponse est directement prise d'une base de données. Afin de valider cela, une route est présente.

```
@GET /admin/cache

returns
{
    "parcoursResponse":[parcoursResponse],
    "calorieResponses":[calorieResponses]

}
```

De plus le cache peut être supprimé

```
@DELETE /admin/cache
returns
{
    "response": "cache deleted"
}
```

Un ETL qui prend des données provenant d'une ressource en ligne a été fait. Objectif est d'avoir un ETL incrémental par contre cela rend les services indisponibles pendant que les nouvelles données sont ajoutées.

https://www.donneesquebec.ca/recherche/fr/dataset/61b5b4e9-d038-4995-b85a-de039dc1b06b/resource/386e62b2-47ae-43bc-a85a-efbcaf3130ba/download/restaurants.json

et

http://donneesouvertes-sherbrooke.opendata.arcgis.com/datasets/e7b14d41a20c4aafa91ed4a9b99384c3_3.geojson?outSR={\"latestWkid\":32187,\"wkid\":32187}

il est appelé de cette façon

```
@ GET /admin/etl
{
    "Before": {
    "restaurants":{
        $type1: int,
        $type2: int,
        ...
    },
    "longueurCyclable":float
},
	"Now":{
    "restaurants":{
        $type1: int,
        $type2: int,
        ...
    },
    "longueurCyclable":float
}
}
```
la base de données peut être complètement supprimée comme ceci

```
@ DELETE /admin/clear
returns
{
    "response": "cache deleted"
}
```

## Fonctionnalité avancée

Calcul du nombre de calories brûlées versus nombre de calories
consommées à un restaurant. Afin de simplifier la données des calories, un repas typique
a été ciblé pour chaque type de restaurant et le nombre de calories correspondant a été
attribué au type de restaurant comme `CalorieLevel` moyen. Voici le nombre de calorie moyen
pour chaque type de restaurant:

**Bonne tables** : repas spaghetti + sauce = 600 calories
**Brasseries** : 4 bières à 200 calories/bière = 800 calories
**Cafés & Bistros** : Équivalent de 10 Timbits + 1 café = 700 calories
**Chefs créateurs** : 2 tartar de boeuf + 1 crème brûlée + 2 coupes de vin = 630 calories
**Cuisine familiale** : 1 portion de pâté chinois avec à côtés = 377 calories
**Délices d'ici** : (cabane à sucre) ragût de boulettes + grand-père à l'érable + portion de tourtière + 2 oreille de christ = 1245 calories
**Pubs et microbrasseries** : 1 nachos + 1 bière = 1350 calories
**Restaurantion rapide** : 1 trio big-mac = 1330 calories
**Saveurs du monde** : (restaurant chinois) Poulet et brocoli + porc sucré & salé + soupe Wonton + à côtés = 856 calories

Ensuite, pour le nombre de calories dépensées pour une distance parcourue est approximée en assumant que l'utilisateur est une personne normal qui roule
environ à une vitesse de 14 mph (22 km/h). À une telle vitesse, un cycliste normal dépense environ 0.0298 calories/mètre [source](https://caloriesburnedhq.com/calories-burned-biking/#:~:text=a%20150%20person%20cycling%20a,to%2075%20calories%20per%20mile.). À noter ici que l'unité 'calorie'
correspond en fait à une 'kcal' dans la réalité. En effet, en nutrition, une calorie fait toujours référence à une kcal,

Cette approximation d'énergie dépensée pourra être amélioré si le MVP est approuvé.

Générer un parcours avec un calcul de calories consommées et dépensées

Cet appel permet à un utilisateur ou une application cliente d’obtenir:

    - un trajet partant d’un point dans un rayon de 500m du point startingPoint
    - le trajet obtenu est d’une longueur de maximumLength ± 10%
    - le trajet à au plus (et de préférence) numberOfStops arrets
    - qui sont des restaurants inclus dans les types définis dans le tableau type et
    - si le tableau type est vide, on assume que tous les types sont possibles
    - la somme des calories dépensées avec le parcours de vélo
    - la somme des calories ingérées avec les restaurants
    - le bilan de calories net pour voir s'il est positif ou négatif

```
@GET /calories (avec le payload):
{
    "startingPoint" : {"type":"Point", "coordinates":[float, float]},
    "maximumLength": int (en mètre),
    "numberOfStops": int,
    "type": [str, str, ... ]
}

returns:
{
    calorieIntake: float,
    calorieOutput: float,
    calorieNet float,
    course : {
        "type": "FeatureCollection",
        "features": [
            {
                "type":"Feature",
                "geometry":{
                    "type": "Point",
                    "coordinates":  [float, float]
                },
                "properties":{
                    "name":str,
                    "type":str
                }
            }, ..., {
                "type":"Feature",
                "geometry":{
                    "type": "MultiLineString",
                    "coordinates": [[
                         [float, float],  [float, float],  [float, float], ...
                        ]]
                },
                "properties":{
                    "length":float
                }
            }
        ]
    }
}
```
