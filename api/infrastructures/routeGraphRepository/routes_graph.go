package routeGraphRepository

import (
	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
)

type RoutesGraph interface {
	AddAndConnectRestaurant(*domain.Restaurant) error
	AddAndConnectManyRestaurant(restaurants []domain.Restaurant) error
	AddCyclePath(cyclePath *domain.CyclePath) error
	AllRestaurants() ([]domain.Restaurant, error)
	AllCyclePaths() ([]domain.CyclePath, error)
	Clear() error
	ClearRestaurants() error
	ClearCyclePaths() error
	Close()
	GetCourseDistance(course *domain.CyclePath, start *domain.CyclePathNode) (float64, error)
	GenerateCyclingCourse(startingPoint *domain.CyclePathNode, maxLength int, numberOfStops int, types []string) (domain.CyclePath, []domain.Restaurant, error)
	GetStartingPointBestCandidate(maxLength int, types []string) (domain.CyclePathNode, error)
	GetCyclePathForNode(node domain.CyclePathNode) (domain.CyclePath, error)
	IsRestaurantConnectedToCyclePath(restaurant *domain.Restaurant, path *domain.CyclePath) (bool, error)
}
