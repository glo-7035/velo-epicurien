package neo

import (
	"errors"
	"fmt"
	"github.com/neo4j/neo4j-go-driver/neo4j"
	configRoot "gitlab.com/constraintAutomaton/velo-epicurien/api/config"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	infrastructures "gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/routeGraphRepository"
	"math/rand"
	"sort"
	"time"
)

type routesGraphNeo4j struct {
	driver                neo4j.Driver
	distanceMaxConnection float32
}

func ProvideRouteGraph() (infrastructures.RoutesGraph, error) {

	config := configRoot.ProvideConfig().NeoDatabase

	driver, err := neo4j.NewDriver(config.DBAddr, neo4j.BasicAuth(config.DBUser, config.DBPassword, ""), func(c *neo4j.Config) {
		c.Encrypted = false
	})
	if err != nil {
		return nil, err
	}

	routesGraph := routesGraphNeo4j{driver, 500}
	err = routesGraph.initializeRouteGraph()
	if err != nil {
		return nil, err
	}

	return &routesGraph, nil
}

func (routesGraph *routesGraphNeo4j) initializeRouteGraph() error {
	session, err := routesGraph.driver.Session(neo4j.AccessModeWrite)
	if err != nil {
		return err
	}
	defer session.Close()

	fmt.Printf("######### INDEX CREATION NEO4J #########\n")

	_, err = session.Run("CREATE INDEX [restaurantId] [IF NOT EXISTS] FOR (r:Restaurant) ON (n.id)", nil)
	if err != nil {
		return err
	}
	_, err = session.Run("CREATE INDEX [geoPointLocalisation] [IF NOT EXISTS] FOR (n:GeoPoint) ON (n.longitude, n.latitude)", nil)
	if err != nil {
		return err
	}
	_, err = session.Run("CREATE INDEX [cycleNodeLocalisation] [IF NOT EXISTS] FOR (n:CycleNode) ON (n.longitude, n.latitude)", nil)
	if err != nil {
		return err
	}
	_, err = session.Run("CREATE INDEX [cyclePathId] [IF NOT EXISTS] FOR (n:CycleNode) ON (n.path_id)", nil)
	if err != nil {
		return err
	}

	fmt.Printf("######### SUCCESSFUL INDEX CREATION NEO4J #########\n")
	return nil
}

func (routesGraph *routesGraphNeo4j) AllCyclePaths() ([]domain.CyclePath, error) {
	session, err := routesGraph.driver.Session(neo4j.AccessModeRead)
	if err != nil {
		return nil, err
	}
	defer session.Close()

	result, err := session.Run("MATCH (c:CycleNode) RETURN c.path_id, c.path_name, c.longitude, c.latitude, ID(c), c.total_path_length ORDER BY c.path_id", nil)
	if err != nil {
		return nil, err
	}

	var allCyclePath []domain.CyclePath
	var currentCyclePath domain.CyclePath
	var processedIds []string
	lastIterationSet := false

	for result.Next() {
		record := result.Record()
		pathId := record.GetByIndex(0).(string)
		pathName := record.GetByIndex(1).(string)
		pathLength := record.GetByIndex(5).(float64)

		if !contains(processedIds, pathId) {
			if len(processedIds) > 0 {
				allCyclePath = append(allCyclePath, currentCyclePath) //completed the previous cycle path
			}

			currentCyclePath = domain.CyclePath{
				Id:    pathId,
				Name:  pathName,
				Nodes: []domain.CyclePathNode{},
				Length: float32(pathLength),
			}

			processedIds = append(processedIds, pathId)
		}

		node := domain.CyclePathNode{
			Coordinates: [2]float64{record.GetByIndex(2).(float64), record.GetByIndex(3).(float64)},
			Id:          record.GetByIndex(4).(int64),
		}

		currentCyclePath.Nodes = append(currentCyclePath.Nodes, node)
		lastIterationSet = true
	}

	if lastIterationSet {
		allCyclePath = append(allCyclePath, currentCyclePath) //was not added from last iteration
	}

	return allCyclePath, nil

}

func (routesGraph *routesGraphNeo4j) allCyclePathIds() ([]string, error) {
	session, err := routesGraph.driver.Session(neo4j.AccessModeRead)
	if err != nil {
		return nil, err
	}
	defer session.Close()

	result, err := session.Run("MATCH (c:CycleNode) RETURN DISTINCT c.path_id", nil)
	if err != nil {
		return nil, err
	}

	var allIds []string
	for result.Next() {
		record := result.Record()
		allIds = append(allIds, record.GetByIndex(0).(string))
	}

	return allIds, nil
}

func contains(slice []string, target string) bool {
	for _, a := range slice {
		if a == target {
			return true
		}
	}
	return false
}

func (routesGraph *routesGraphNeo4j) AllRestaurants() ([]domain.Restaurant, error) {

	session, err := routesGraph.driver.Session(neo4j.AccessModeRead)
	if err != nil {
		return nil, err
	}
	defer session.Close()

	result, err := session.Run("MATCH (r:Restaurant) RETURN r.id, r.name, r.type, r.longitude, r.latitude, r.calorieLevel", nil)
	if err != nil {
		return nil, err
	}

	var allRestaurants []domain.Restaurant
	for result.Next() {
		record := result.Record()
		restaurant := domain.Restaurant{
			Id:           record.GetByIndex(0).(string),
			Name:         record.GetByIndex(1).(string),
			Type:         record.GetByIndex(2).(string),
			Coordinates:  [2]float32{float32(record.GetByIndex(3).(float64)), float32(record.GetByIndex(4).(float64))},
			CalorieLevel: int(record.GetByIndex(5).(int64)),
		}

		allRestaurants = append(allRestaurants, restaurant)
	}

	return allRestaurants, nil
}

func (routesGraph *routesGraphNeo4j) AddAndConnectRestaurant(restaurant *domain.Restaurant) error {
	session, err := routesGraph.driver.Session(neo4j.AccessModeWrite)

	if err != nil {
		return err
	}
	defer session.Close()

	allCyclePathIds, err := routesGraph.allCyclePathIds()
	if err != nil {
		return err
	}

	return routesGraph.addAndConnectRestaurant(restaurant, allCyclePathIds, &session)
}

func (routesGraph *routesGraphNeo4j) AddAndConnectManyRestaurant(restaurants []domain.Restaurant) error {

	if restaurants == nil || len(restaurants) == 0 {
		return nil
	}

	session, err := routesGraph.driver.Session(neo4j.AccessModeWrite)

	if err != nil {
		return err
	}
	defer session.Close()

	allCyclePathIds, err := routesGraph.allCyclePathIds()
	if err != nil {
		return err
	}

	var idsInError []string
	for _, restaurant := range restaurants {
		if err := routesGraph.addAndConnectRestaurant(&restaurant, allCyclePathIds, &session); err != nil {
			idsInError = append(idsInError, restaurant.Id)
		}
	}

	if len(idsInError) > 0 {
		var formatedIdsInError string
		for _, err := range idsInError {
			formatedIdsInError += " - " + err
		}
		return errors.New("failed to connect restaurant for following ids: " + formatedIdsInError)
	}

	return nil
}

func (routesGraph *routesGraphNeo4j) addAndConnectRestaurant(restaurant *domain.Restaurant, allCyclePathsIds []string, session *neo4j.Session) error {
	var restaurantDatabaseGeneratedId int64
	resultId, err := (*session).Run("MERGE (restaurant:Restaurant {id:$id, name:$name, type:$type, latitude:$latitude, longitude:$longitude, calorieLevel:$calorieLevel}) RETURN ID(restaurant)",
		map[string]interface{}{
			"id":           restaurant.Id,
			"name":         restaurant.Name,
			"type":         restaurant.Type,
			"latitude":     restaurant.Latitude(),
			"longitude":    restaurant.Longitude(),
			"calorieLevel": restaurant.CalorieLevel,
		})
	if err != nil {
		return err
	}

	for resultId.Next() {
		restaurantDatabaseGeneratedId = resultId.Record().GetByIndex(0).(int64)
	}

	fmt.Printf("######### created restaurant : %s   ########\n", restaurant.Id)

	for _, cyclePathId := range allCyclePathsIds {

		fmt.Printf("######### try Connecting restaurant : %s to cyclePath with Id %s  ########\n", restaurant.Id, cyclePathId)
		_, err := (*session).Run(`
				CALL {
					MATCH (node:CycleNode), (restaurant:Restaurant)
					WHERE ID(restaurant)=$id AND node.path_id=$path_id
					WITH point({ longitude: node.longitude, latitude: node.latitude}) AS firstPoint, point({ longitude: restaurant.longitude, latitude: restaurant.latitude}) AS restaurantPoint, ID(node) as nodeId, ID(restaurant) as restaurantId
					WITH distance(firstPoint, restaurantPoint) AS dist, nodeId, restaurantId
					WHERE dist<=$distMaxAcceptable
					RETURN restaurantId, nodeId, dist
					ORDER BY dist limit 1
				}
				MATCH (node2:CycleNode), (restaurant2:Restaurant)
				WHERE ID(restaurant2)=restaurantId AND ID(node2) = nodeId
				MERGE (node2)-[path:ARE_CONNECTED {distance: dist}]->(restaurant2)`,
			map[string]interface{}{
				"id":                restaurantDatabaseGeneratedId,
				"path_id":           cyclePathId,
				"distMaxAcceptable": routesGraph.distanceMaxConnection,
			})
		if err != nil {
			return err
		}

	}
	return nil
}

func (routesGraph *routesGraphNeo4j) ClearRestaurants() error {
	session, err := routesGraph.driver.Session(neo4j.AccessModeWrite)

	if err != nil {
		return err
	}
	defer session.Close()

	_, err = session.Run("MATCH (restaurant:Restaurant) DETACH DELETE restaurant", nil)

	return err
}

func (routesGraph *routesGraphNeo4j) ClearCyclePaths() error {
	session, err := routesGraph.driver.Session(neo4j.AccessModeWrite)

	if err != nil {
		return err
	}
	defer session.Close()

	_, err = session.Run("MATCH (node:CycleNode) DETACH DELETE node", nil)

	return err
}

func (routesGraph *routesGraphNeo4j) AddCyclePath(cyclePath *domain.CyclePath) error {

	session, err := routesGraph.driver.Session(neo4j.AccessModeWrite)

	if err != nil {
		return err
	}
	defer session.Close()

	err = routesGraph.createOneCyclePath(cyclePath, session)
	if err != nil {
		return err
	}

	return nil
}

func (routesGraph *routesGraphNeo4j) createOneCyclePath(cyclePath *domain.CyclePath, session neo4j.Session) error {

	nodes := cyclePath.Nodes
	if len(nodes) == 0 {
		return nil
	}

	previousNode := nodes[0]
	var previousNodeId int64

	fmt.Printf("############## CREATING CYCLE PATH %s ##############\n", cyclePath.Id)

	resultPreviousNodeId, err := session.Run("MERGE (node:CycleNode:GeoPoint {path_id: $id, path_name: $name, latitude:$latitude, longitude:$longitude, total_path_length: $total_path_length}) RETURN ID(node)", map[string]interface{}{
		"id":        cyclePath.Id,
		"name":      cyclePath.Name,
		"latitude":  previousNode.Latitude(),
		"longitude": previousNode.Longitude(),
		"total_path_length": cyclePath.Length,
	})
	if err != nil {
		return err
	}

	for resultPreviousNodeId.Next() {
		previousNodeId = resultPreviousNodeId.Record().GetByIndex(0).(int64)
	}

	for _, currentNode := range nodes[1:] {

		resultCurrentNodeId, err := session.Run(`
			MERGE (node:CycleNode:GeoPoint {path_id: $id, path_name: $name, latitude:$latitude, longitude:$longitude, total_path_length: $total_path_length})
			RETURN ID(node)`, map[string]interface{}{
			"id":        cyclePath.Id,
			"name":      cyclePath.Name,
			"latitude":  currentNode.Latitude(),
			"longitude": currentNode.Longitude(),
			"total_path_length": cyclePath.Length,
		})
		if err != nil {
			return err
		}

		var currentNodeId int64
		for resultCurrentNodeId.Next() {
			currentNodeId = resultCurrentNodeId.Record().GetByIndex(0).(int64)
		}

		_, err = session.Run(`MATCH (node1:CycleNode), (node2:CycleNode)
		WHERE ID(node1) = $previousNodeId AND ID(node2) = $currentNodeId
		WITH node1, node2, point({ longitude: node1.longitude, latitude: node1.latitude }) AS firstPoint, 
		point({ longitude: node2.longitude, latitude: node2.latitude }) AS secondPoint
		WITH node1, node2, distance(firstPoint, secondPoint) as dist
		MERGE (node1)-[path:ARE_CONNECTED {distance: dist}]->(node2)`, map[string]interface{}{
			"previousNodeId": previousNodeId,
			"currentNodeId":  currentNodeId,
		})
		if err != nil {
			return err
		}

		previousNodeId = currentNodeId
	}

	return nil
}

func (routesGraph *routesGraphNeo4j) Clear() error {

	err := routesGraph.ClearCyclePaths()
	if err != nil {
		return err
	}
	return routesGraph.ClearRestaurants()
}

func (routesGraph *routesGraphNeo4j) Close() {
	routesGraph.driver.Close()
}

func (routesGraph *routesGraphNeo4j) GetStartingPointBestCandidate(maxLength int, types []string) (domain.CyclePathNode, error) {
	session, err := routesGraph.driver.Session(neo4j.AccessModeRead)
	if err != nil {
		return domain.CyclePathNode{}, err
	}
	defer session.Close()

	minAccLength := 0.9 * float64(maxLength)
	maxAccLength := 1.1 * float64(maxLength)
	allCyclePaths, err := routesGraph.AllCyclePaths()
	if err != nil {
		return domain.CyclePathNode{}, err
	}

	firstNodeInEachPath := make(map[domain.CyclePathNode]domain.CyclePath)
	var dataSetMaxDistance float32 = 0

	rand.Seed(time.Now().Unix())
	rand.Shuffle(len(allCyclePaths), func(i, j int) {
		allCyclePaths[i], allCyclePaths[j] = allCyclePaths[j], allCyclePaths[i]
	})
	for _, path := range allCyclePaths {
		if path.Length > dataSetMaxDistance {
			dataSetMaxDistance = path.Length
		}
		sort.Slice(path.Nodes, func(i, j int) bool {
			return path.Nodes[i].Id < path.Nodes[j].Id
		})
		if float64(path.Length) >= minAccLength {
			firstNodeInEachPath[path.Nodes[0]] = path
		}
	}

	if len(firstNodeInEachPath) == 0 {
		return domain.CyclePathNode{}, errors.New(fmt.Sprintf("no starting point found. Longest cycle path in dataset is %f meters long", dataSetMaxDistance))
	}

	bestStartCandidate := domain.CyclePathNode{}

	for node, correspondingCyclePath := range firstNodeInEachPath {

		for _, otherNode := range correspondingCyclePath.Nodes {
			if otherNode.Id == node.Id {
				continue
			}


			distanceResult, err := session.Run(`
				CALL {
					MATCH p=(c:CycleNode)-[r1:ARE_CONNECTED*1..]-(d:CycleNode)
					WHERE ID(c) = $firstNodeId AND ID(d) = $anyNodeId
					RETURN p limit 1
				 }
				WIth relationships(p) as r2
				WITH REDUCE (total = 0, rel in r2 | total + rel.distance) as totalDistance
				RETURN totalDistance
				`, map[string]interface{}{
					"firstNodeId":         node.Id,
					"anyNodeId": otherNode.Id,
			})
			if err != nil {
				return domain.CyclePathNode{}, err
			}

			var distance float64 = 0
			for distanceResult.Next() {
				record := distanceResult.Record()
				distance = record.GetByIndex(0).(float64)
			}

			if distance >= minAccLength && distance <= maxAccLength {

				fmt.Printf("GOT ONE CANDIDATE\n")
				restaurantsResult, err := session.Run(`
					CALL {
						MATCH p=(c:CycleNode)-[r1:ARE_CONNECTED*1..]-(d:CycleNode)
						WHERE ID(c) = $firstNodeId AND ID(d) = $anyNodeId
						RETURN p limit 1
					 }
					MATCH (restaurant:Restaurant)-[:ARE_CONNECTED]-(n:CycleNode)
					WHERE n in nodes(p) AND restaurant.type in $acceptedTypes
					RETURN restaurant
					`, map[string]interface{}{
						"firstNodeId":         node.Id,
						"anyNodeId": otherNode.Id,
						"acceptedTypes" : types,
				})
				if err != nil {
					return domain.CyclePathNode{}, err
				}


				reachableRestaurantsInAcceptedLength := extractRestaurantEntriesFromDatabaseResult(&restaurantsResult)
				bestStartCandidate = node
				if reachableRestaurantsInAcceptedLength != nil &&
					len(reachableRestaurantsInAcceptedLength) > 0 {
					return bestStartCandidate, nil
				}
			}
		}

	}

	return bestStartCandidate, errors.New(fmt.Sprintf("cannot find starting point with restaurants on path of length %v ± 10", maxLength) + "%")
}

func (routesGraph *routesGraphNeo4j) GenerateCyclingCourse(startingPoint *domain.CyclePathNode,
	maxLength int, numberOfStops int, types []string) (domain.CyclePath, []domain.Restaurant, error) {

	minAccLength := 0.9 * float64(maxLength)
	maxAccLength := 1.1 * float64(maxLength)
	nearNodes, err := routesGraph.getNearestNodeFrom(startingPoint)
	if err != nil {
		return domain.CyclePath{}, nil, err
	}

	var startPointWithReference domain.CyclePathNode
	var correspondingCyclePath domain.CyclePath
	for _, someNode := range nearNodes {
		correspondingCyclePath, err = routesGraph.getCyclePathForNodeInternal(someNode)
		if err != nil {
			return domain.CyclePath{}, nil, err
		}
		startPointWithReference = someNode
		if correspondingCyclePath.Length >= float32(minAccLength) {
			break
		}
	}
	session, err := routesGraph.driver.Session(neo4j.AccessModeRead)
	if err != nil {
		return domain.CyclePath{}, nil, err
	}
	defer session.Close()


	if correspondingCyclePath.Length < float32(minAccLength) {
		return domain.CyclePath{}, nil, errors.New(fmt.Sprintf("invalid max length, target cycle path total length is %f", correspondingCyclePath.Length))
	}

	lastCandidateRestaurantCount := 0
	for _, otherNode := range correspondingCyclePath.Nodes {
		if otherNode.Id == startPointWithReference.Id {
			continue
		}
		distanceResult, err := session.Run(`
				CALL {
					MATCH p=(c:CycleNode)-[r1:ARE_CONNECTED*1..]-(d:CycleNode)
					WHERE ID(c) = $firstNodeId AND ID(d) = $anyNodeId
					RETURN p limit 1
				 }
				WIth relationships(p) as r2
				WITH REDUCE (total = 0, rel in r2 | total + rel.distance) as totalDistance
				RETURN totalDistance
				`, map[string]interface{}{
			"firstNodeId":         startPointWithReference.Id,
			"anyNodeId": otherNode.Id,
		})
		if err != nil {
			return domain.CyclePath{}, nil, err
		}

		var distance float64 = 0
		for distanceResult.Next() {
			record := distanceResult.Record()
			distance = record.GetByIndex(0).(float64)
		}
		if distance >= minAccLength && distance <= maxAccLength {
			restaurantsResult, err := session.Run(`
					CALL {
						MATCH p=(c:CycleNode)-[r1:ARE_CONNECTED*1..]-(d:CycleNode)
						WHERE ID(c) = $firstNodeId AND ID(d) = $anyNodeId
						RETURN p limit 1
					 }
					MATCH (restaurant:Restaurant)-[:ARE_CONNECTED]-(n:CycleNode)
					WHERE n in nodes(p) AND restaurant.type in $acceptedTypes
					RETURN restaurant
					`, map[string]interface{}{
				"firstNodeId":		startPointWithReference.Id,
				"anyNodeId": 		otherNode.Id,
				"acceptedTypes" : 	types,
			})
			if err != nil {
				return domain.CyclePath{}, nil, err
			}

			resultPath, err := session.Run(`
					MATCH p=(c:CycleNode)-[r1:ARE_CONNECTED*1..]-(d:CycleNode)
					WHERE ID(c) = $firstNodeId AND ID(d) = $anyNodeId
					RETURN p limit 1
				`, map[string]interface{}{
				"firstNodeId":	startPointWithReference.Id,
				"anyNodeId": 	otherNode.Id,
			})
			if err != nil {
				return domain.CyclePath{}, nil, err
			}



			var cycleCourse domain.CyclePath
			extractCyclePathFromDatabaseResult(resultPath, &cycleCourse)

			cycleCourse.Length = float32(distance)

			var reachableRestaurantsInAcceptedLength []domain.Restaurant
			if numberOfStops == 0 {
				reachableRestaurantsInAcceptedLength = nil
			} else {
				reachableRestaurantsInAcceptedLength = extractRestaurantEntriesFromDatabaseResult(&restaurantsResult)
			}

			if reachableRestaurantsInAcceptedLength != nil &&
				len(reachableRestaurantsInAcceptedLength) > lastCandidateRestaurantCount {

				var restaurantsToReturn []domain.Restaurant
				if len(reachableRestaurantsInAcceptedLength) > numberOfStops{
					restaurantsToReturn = reachableRestaurantsInAcceptedLength[:numberOfStops]
				} else {
					restaurantsToReturn = reachableRestaurantsInAcceptedLength
				}

				return cycleCourse, restaurantsToReturn, nil
			}


			return cycleCourse, reachableRestaurantsInAcceptedLength, nil
		}
	}

	return domain.CyclePath{}, nil, err
}

func extractRestaurantEntriesFromDatabaseResult(resultRestaurants *neo4j.Result) []domain.Restaurant {
	var restaurants []domain.Restaurant

	for (*resultRestaurants).Next() {
		record := (*resultRestaurants).Record()
		rawNode := record.GetByIndex(0).(neo4j.Node)
		properties := rawNode.Props()
		restaurant := domain.Restaurant{
			Id:           properties["id"].(string),
			Name:         properties["name"].(string),
			Type:         properties["type"].(string),
			Coordinates:  [2]float32{float32(properties["longitude"].(float64)), float32(properties["latitude"].(float64))},
			CalorieLevel: int(properties["calorieLevel"].(int64)),
		}

		restaurants = append(restaurants, restaurant)
	}
	return restaurants
}

func extractCyclePathFromDatabaseResult(resultPath neo4j.Result, cycleCourse *domain.CyclePath) {

	for resultPath.Next() {
		record := resultPath.Record()
		pathEntity := record.GetByIndex(0).(neo4j.Path)

		for _, rawNode := range pathEntity.Nodes() {
			properties := rawNode.Props()
			var node domain.CyclePathNode
			node.Id = rawNode.Id()
			node.Coordinates[0] = properties["longitude"].(float64)
			node.Coordinates[1] = properties["latitude"].(float64)
			cycleCourse.Nodes = append(cycleCourse.Nodes, node)

			if len(cycleCourse.Name) == 0 {
				cycleCourse.Name = properties["path_name"].(string)
			}

			if len(cycleCourse.Id) == 0 {
				cycleCourse.Name = properties["path_id"].(string)
			}
		}
	}
}

func (routesGraph *routesGraphNeo4j) IsRestaurantConnectedToCyclePath(restaurant *domain.Restaurant, path *domain.CyclePath) (bool, error) {
	session, err := routesGraph.driver.Session(neo4j.AccessModeRead)
	if err != nil {
		return false, err
	}
	defer session.Close()

	result, err := session.Run(`
		MATCH (r:Restaurant { id: $restaurantId})-[a:ARE_CONNECTED]-(node:CycleNode {path_id: $path_id}) RETURN a
	`, map[string]interface{}{
		"restaurantId": restaurant.Id,
		"path_id":      path.Id,
	})
	if err != nil {
		return false, err
	}

	for result.Next() {
		return true, nil
	}
	return false, nil
}

func (routesGraph *routesGraphNeo4j) GetCyclePathForNode(node domain.CyclePathNode) (domain.CyclePath, error) {

	nearNodes, err := routesGraph.getNearestNodeFrom(&node)
	if err != nil {
		return domain.CyclePath{}, err
	}
	nodeRef := nearNodes[0]

	path, err2 := routesGraph.getCyclePathForNodeInternal(nodeRef)
	if err2 == nil {
		return path, err2
	}

	return domain.CyclePath{}, errors.New("could not find path for node")
}

func (routesGraph *routesGraphNeo4j) getCyclePathForNodeInternal(nodeRef domain.CyclePathNode) (domain.CyclePath, error) {
	allCyclePaths, err := routesGraph.AllCyclePaths()
	if err != nil {
		return domain.CyclePath{}, err
	}

	for _, path := range allCyclePaths {
		for _, n := range path.Nodes {
			if n.Id == nodeRef.Id {
				return path, nil
			}
		}
	}
	return domain.CyclePath{}, nil
}

func (routesGraph *routesGraphNeo4j) getNearestNodeFrom(point *domain.CyclePathNode) ([]domain.CyclePathNode, error) {

	if point.Id != 0 {
		return nil, nil
	}

	session, err := routesGraph.driver.Session(neo4j.AccessModeRead)
	if err != nil {
		return nil, err
	}
	defer session.Close()

	result, err := session.Run(`
	MATCH (node1:CycleNode)
    WITH node1, point({ longitude: node1.longitude, latitude: node1.latitude }) AS firstPoint, 
		 point({ longitude: $longitude, latitude: $latitude }) AS secondPoint
	WITH node1, distance(firstPoint, secondPoint) as dist
	ORDER BY dist
	RETURN node1 LIMIT 3
	`, map[string]interface{}{
		"longitude": point.Longitude(),
		"latitude":  point.Latitude(),
	})
	if err != nil {
		return nil, err
	}

	var nodes []domain.CyclePathNode
	for result.Next() {
		record := result.Record()
		rawNode := record.GetByIndex(0).(neo4j.Node)
		properties := rawNode.Props()

		cycleNode := domain.CyclePathNode{
			Id:          rawNode.Id(),
			Coordinates: [2]float64{properties["longitude"].(float64), properties["latitude"].(float64)},
		}

		nodes = append(nodes, cycleNode)
	}

	return nodes, nil
}

func (routesGraph *routesGraphNeo4j) GetCourseDistance(course *domain.CyclePath, start *domain.CyclePathNode) (float64, error) {

	nodesToProcess := routesGraph.extractNodesDbIds(*course)
	nearNodes, err := routesGraph.getNearestNodeFrom(start)
	if err != nil {
		return 0, err
	}
	startPoint := nearNodes[0]
	nodesToProcessFiltered := removeIfPresent(nodesToProcess, startPoint.Id)
	connectionCount := len(nodesToProcessFiltered)

	session, err := routesGraph.driver.Session(neo4j.AccessModeRead)
	if err != nil {
		return 0, err
	}
	defer session.Close()
	result, err := session.Run(fmt.Sprintf(`
		MATCH p=(node1:CycleNode)-[tt:ARE_CONNECTED*%d..]-(node2:CycleNode)
		WHERE ID(node1) = $startNodeId AND ID(node2) in $acceptedIds
		WITH REDUCE (total = 0, rel in tt | total + rel.distance) as totalDistance
		RETURN totalDistance
	`, connectionCount),
		map[string]interface{}{
			"startNodeId": startPoint.Id,
			"acceptedIds": nodesToProcessFiltered,
		})
	if err != nil {
		return 0, err
	}

	dist := 0.0
	for result.Next() {
		record := result.Record()
		dist = record.GetByIndex(0).(float64)
		return dist, nil
	}

	return dist, nil
}

func (routesGraph *routesGraphNeo4j) extractNodesDbIds(course domain.CyclePath) []int64 {
	var nodesToProcess []int64
	for _, node := range course.Nodes {
		if node.Id == 0 {
			nearNodes, err := routesGraph.getNearestNodeFrom(&node)
			if err != nil {
				continue
			}
			nodeWithId := nearNodes[0]

			nodesToProcess = append(nodesToProcess, nodeWithId.Id)
		} else {
			nodesToProcess = append(nodesToProcess, node.Id)
		}
	}

	return nodesToProcess
}

func removeIfPresent(collection []int64, toRemove int64) []int64 {
	var result []int64
	for _, a := range collection {
		if a != toRemove {
			result = append(result, a)
		}
	}
	return result
}
