package mongo

import (
	"context"
	"fmt"
	"reflect"
	"time"

	"github.com/cnf/structhash"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/config"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type cacheRepository struct {
	addr     string
	parcours *mongo.Collection
	calorie  *mongo.Collection
}

func ProvideCacheRepository() (storeRepository.CacheRepository, error) {
	driver := cacheRepository{addr: config.ProvideConfig().MongoDatabase.DBAddr}
	err, disconnect, _, client := driver.connectToDb()
	defer disconnect()
	if err != nil {
		return nil, err
	}
	database := client.Database(DATABASE)
	parcours := database.Collection(PARCOURS_RESPONSE_COLLECTION)
	calorie := database.Collection(CALORIE_RESONSE_COLLECTION)

	driver = cacheRepository{addr: config.ProvideConfig().MongoDatabase.DBAddr,
		parcours: parcours,
		calorie:  calorie,
	}
	return driver, nil
}
func (c cacheRepository) GetParcoursResponse(req payload.CourseRequest) (payload.GeoJson, error) {
	err, disconnect, ctx, _ := c.connectToDb()
	defer disconnect()
	if err != nil {
		return payload.GeoJson{}, err
	}
	hashReq, err := structhash.Hash(req, 1)
	if err != nil {
		return payload.GeoJson{}, err
	}
	filter := bson.D{{"req", hashReq}}
	resp := cacheGeojsonResponse{}
	err = c.parcours.FindOne(ctx, filter).Decode(&resp)
	if err != nil {
		return payload.GeoJson{}, err
	}

	return resp.Resp, nil
}

func (c cacheRepository) GetCalorieResponse(req payload.CourseRequest) (payload.CalorieResponseBody, error) {
	err, disconnect, ctx, _ := c.connectToDb()
	defer disconnect()
	if err != nil {
		return payload.CalorieResponseBody{}, err
	}
	hashReq, err := structhash.Hash(req, 1)
	if err != nil {
		return payload.CalorieResponseBody{}, err
	}
	filter := bson.D{{"req", hashReq}}
	resp := cacheCalorieResponseBody{}
	err = c.calorie.FindOne(ctx, filter).Decode(&resp)
	if err != nil {
		return payload.CalorieResponseBody{}, err
	}
	return resp.Resp, nil
}

func (c cacheRepository) Clear() error {
	err, disconnect, ctx, _ := c.connectToDb()
	defer disconnect()
	if err != nil {
		return err
	}
	_, err = c.parcours.DeleteMany(ctx, bson.D{})
	if err != nil {
		return err
	}
	_, err = c.calorie.DeleteMany(ctx, bson.D{})
	if err != nil {
		return err
	}
	return nil
}

func (c cacheRepository) Close() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, _ := mongo.Connect(ctx, options.Client().ApplyURI(c.addr))
	_ = client.Disconnect(ctx)

}

func (c cacheRepository) connectToDb() (error, func(), context.Context, *mongo.Client) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(c.addr))

	return err, func() {
		cancel()
	}, ctx, client
}

func (c cacheRepository) AddParcoursResponseIfNotExist(req payload.CourseRequest, resp payload.GeoJson) error {
	err, _, ctx, _ := c.connectToDb()
	if err != nil {
		return err
	}

	hashReq, err := structhash.Hash(req, 1)
	if err != nil {
		return err
	}
	filter := bson.D{{"req", hashReq}}
	doc := c.parcours.FindOne(ctx, filter)
	if doc.Err() != nil {
		if doc.Err().Error() != mongo.ErrNoDocuments.Error() {
			return err
		}
		err, disconnect, ctx, _ := c.connectToDb()
		defer disconnect()
		if err != nil {
			return err
		}
		cacheDoc := cacheGeojsonResponse{
			Resp: resp,
			Req:  hashReq,
		}

		_, err = c.parcours.InsertOne(ctx, cacheDoc)
		return err
	}

	return fmt.Errorf("the response is already inside the cache")
}

func (c cacheRepository) AddCalorieResponseIfNotExist(req payload.CourseRequest, resp payload.CalorieResponseBody) error {
	err, _, ctx, _ := c.connectToDb()
	if err != nil {
		return err
	}

	hashReq, err := structhash.Hash(req, 1)
	if err != nil {
		return err
	}
	filter := bson.D{{"req", hashReq}}
	doc := c.calorie.FindOne(ctx, filter)

	if doc.Err() != nil {
		if doc.Err().Error() != mongo.ErrNoDocuments.Error() {
			return err
		}
		err, disconnect, ctx, _ := c.connectToDb()
		defer disconnect()
		if err != nil {
			return err
		}
		cacheDoc := cacheCalorieResponseBody{
			Resp: resp,
			Req:  hashReq,
		}

		_, err = c.calorie.InsertOne(ctx, cacheDoc)
		return err
	}

	return fmt.Errorf("the response is already inside the cache")
}

func (c cacheRepository) GetAllResponse() ([]payload.GeoJson, []payload.CalorieResponseBody, error) {
	err, disconnect, ctx, _ := c.connectToDb()
	defer disconnect()
	if err != nil {
		return nil, nil, err
	}
	cursor, err := c.parcours.Find(ctx, bson.D{})
	if err != nil {
		return nil, nil, err
	}
	geojsonResp := []cacheGeojsonResponse{}
	err = cursor.All(ctx, &geojsonResp)
	if err != nil {
		return nil, nil, err
	}

	cursor, err = c.calorie.Find(ctx, bson.D{})
	if err != nil {
		return nil, nil, err
	}
	calorieResp := []cacheCalorieResponseBody{}
	err = cursor.All(ctx, &calorieResp)
	if err != nil {
		return nil, nil, err
	}
	if len(geojsonResp) > 0 {
		if reflect.DeepEqual(geojsonResp[0], cacheGeojsonResponse{}) {
			geojsonResp = []cacheGeojsonResponse{}
		}
	}
	if len(calorieResp) > 0 {
		if reflect.DeepEqual(calorieResp[0], cacheCalorieResponseBody{}) {
			calorieResp = []cacheCalorieResponseBody{}
		}
	}
	rGeoJson := []payload.GeoJson{}
	for _, v := range geojsonResp {
		rGeoJson = append(rGeoJson, v.Resp)
	}
	rCalorie := []payload.CalorieResponseBody{}
	for _, v := range calorieResp {
		rCalorie = append(rCalorie, v.Resp)
	}
	return rGeoJson, rCalorie, nil
}

type cacheCalorieResponseBody struct {
	Resp payload.CalorieResponseBody `bson:"resp"`
	Req  string                      `bson:"req"`
}

type cacheGeojsonResponse struct {
	Resp payload.GeoJson `bson:"resp"`
	Req  string          `bson:"req"`
}
