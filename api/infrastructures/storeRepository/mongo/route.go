package mongo

import (
	"context"
	"time"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/config"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type routeRepository struct {
	addr   string
	routes *mongo.Collection
}

func ProvideRouteRepository() (storeRepository.RouteRepository, error) {
	driver := routeRepository{addr: config.ProvideConfig().MongoDatabase.DBAddr}
	err, disconnect, _, client := driver.connectToDb()
	defer disconnect()
	if err != nil {
		return nil, err
	}
	database := client.Database(DATABASE)
	route := database.Collection(ROUTE_COLLECTION)
	driver = routeRepository{addr: config.ProvideConfig().MongoDatabase.DBAddr,
		routes: route,
	}
	return driver, nil
}

func (r routeRepository) Add(cyclePath *domain.CyclePath) error {
	err, disconnect, ctx, _ := r.connectToDb()
	defer disconnect()
	if err != nil {
		return err
	}
	opts := options.Update().SetUpsert(true)

	geoJson := fromCyclePath(cyclePath)
	filter := bson.D{{"id", cyclePath.Id}}
	_, err = r.routes.UpdateOne(ctx, filter, ToGeoJsonBsonUpdateOperator(&geoJson), opts)
	return err
}

func (r routeRepository) AddMany(cyclePaths []domain.CyclePath) error {

	for _, cyclePath := range cyclePaths {
		err := r.Add(&cyclePath)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r routeRepository) All() ([]domain.CyclePath, error) {
	err, disconnect, ctx, _ := r.connectToDb()
	defer disconnect()
	if err != nil {
		return nil, err
	}
	cursor, err := r.routes.Find(ctx, bson.D{})
	if err != nil {
		return nil, err
	}
	geojson := []geoJson{}
	err = cursor.All(ctx, &geojson)
	if err != nil {
		return nil, err
	}
	cyclePath := make([]domain.CyclePath, len(geojson))
	for i, el := range geojson {
		cyclePath[i] = el.TocyclePath()
	}
	return cyclePath, nil
}
func (r routeRepository) Clear() error {
	err, disconnect, ctx, _ := r.connectToDb()
	defer disconnect()
	if err != nil {
		return err
	}
	_, err = r.routes.DeleteMany(ctx, bson.D{})
	if err != nil {
		return err
	}
	return nil
}

func (r routeRepository) Ping() error {
	err, disconnect, ctx, client := r.connectToDb()
	defer disconnect()
	if err != nil {
		return err
	}
	err = client.Ping(ctx, readpref.Primary())
	return err
}

func (r routeRepository) Size() (uint, error) {
	err, disconnect, ctx, _ := r.connectToDb()
	defer disconnect()
	if err != nil {
		return 0, err
	}
	size, err := r.routes.CountDocuments(ctx, bson.D{})
	if err != nil {
		return 0, err
	}
	return uint(size), err
}

func (r routeRepository) connectToDb() (error, func(), context.Context, *mongo.Client) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(r.addr))

	return err, func() {
		cancel()
	}, ctx, client
}

func (r routeRepository) Close() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, _ := mongo.Connect(ctx, options.Client().ApplyURI(r.addr))
	_ = client.Disconnect(ctx)

}

func (r routeRepository) TotalLength() (float64, error) {
	err, disconnect, ctx, _ := r.connectToDb()
	defer disconnect()
	if err != nil {
		return 0, err
	}
	groupStage := bson.D{
		{"$group", bson.D{
			{"_id", bson.TypeNull},
			{"total", bson.D{
				{"$sum", "$features.length"},
			}},
		}},
	}
	cursor, err := r.routes.Aggregate(ctx, mongo.Pipeline{groupStage})
	if err != nil {
		return 0, err
	}
	result := []map[string]float64{}
	err = cursor.All(ctx, &result)
	if err != nil {
		return 0, err
	}
	if len(result) == 0 {
		return 0, nil
	}
	return result[0]["total"], nil
}
