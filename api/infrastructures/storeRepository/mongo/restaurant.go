package mongo

import (
	"context"
	"time"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/config"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type mongoRestaurantRepository struct {
	addr        string
	restaurants *mongo.Collection
}

func ProvideRestaurantRepository() (storeRepository.RestaurantRepository, error) {
	driver := mongoRestaurantRepository{addr: config.ProvideConfig().MongoDatabase.DBAddr}
	err, disconnect, _, client := driver.connectToDb()
	defer disconnect()
	if err != nil {
		return nil, err
	}

	database := client.Database(DATABASE)
	Restaurant := database.Collection(RESTAURANT_COLLECTION)
	driver = mongoRestaurantRepository{addr: config.ProvideConfig().MongoDatabase.DBAddr, restaurants: Restaurant}

	return driver, nil
}

func (m mongoRestaurantRepository) Add(restaurant *domain.Restaurant) error {
	err, disconnect, ctx, _ := m.connectToDb()
	defer disconnect()
	opts := options.Update().SetUpsert(true)
	if err != nil {
		return err
	}
	filter := bson.D{{"name", restaurant.Name}}
	_, err = m.restaurants.UpdateOne(ctx, filter, ToRestaurantBsonUpdateOperator(restaurant), opts)
	return err
}

func (m mongoRestaurantRepository) Ping() error {
	err, disconnect, ctx, client := m.connectToDb()
	defer disconnect()
	if err != nil {
		return err
	}
	err = client.Ping(ctx, readpref.Primary())
	return err
}

func (m mongoRestaurantRepository) AddMany(Restaurants []domain.Restaurant) error {

	for _, v := range Restaurants {
		err := m.Add(&v)
		if err != nil {
			return err
		}
	}

	return nil
}

func (m mongoRestaurantRepository) Size() (uint, error) {
	err, disconnect, ctx, _ := m.connectToDb()
	defer disconnect()
	if err != nil {
		return 0, err
	}
	size, err := m.restaurants.CountDocuments(ctx, bson.D{})
	if err != nil {
		return 0, err
	}
	return uint(size), err
}

func (m mongoRestaurantRepository) RestaurantsTypes() (map[string]uint, error) {
	err, disconnect, ctx, _ := m.connectToDb()
	defer disconnect()
	if err != nil {
		return nil, err
	}
	groupStage := bson.D{
		{"$group", bson.D{
			{"_id", "$type"},
			{"total", bson.D{
				{"$sum", 1},
			}},
		}},
	}
	projectStage := bson.D{
		{
			"$project", bson.D{
				{"type", "$_id"},
				{"total", true},
			},
		},
	}
	cursor, err := m.restaurants.Aggregate(ctx, mongo.Pipeline{groupStage, projectStage})
	if err != nil {
		return nil, err
	}
	restaurantType := map[string]uint{}
	for cursor.Next(ctx) {
		result := aggregateTypeRestaurantResult{}
		err := cursor.Decode(&result)
		if err != nil {
			return nil, err
		}
		restaurantType[result.Type] = result.Total
	}
	return restaurantType, nil
}

func (m mongoRestaurantRepository) All() ([]domain.Restaurant, error) {
	err, disconnect, ctx, _ := m.connectToDb()
	defer disconnect()
	if err != nil {
		return nil, err
	}
	cursor, err := m.restaurants.Find(ctx, bson.D{})
	if err != nil {
		return nil, err
	}
	restaurants := []domain.Restaurant{}
	err = cursor.All(ctx, &restaurants)
	if err != nil {
		return nil, err
	}
	return restaurants, nil
}
func (m mongoRestaurantRepository) Clear() error {
	err, disconnect, ctx, _ := m.connectToDb()
	defer disconnect()
	if err != nil {
		return err
	}
	_, err = m.restaurants.DeleteMany(ctx, bson.D{})
	if err != nil {
		return err
	}
	return nil
}
func (m mongoRestaurantRepository) connectToDb() (error, func(), context.Context, *mongo.Client) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(m.addr))

	return err, func() {
		cancel()
	}, ctx, client
}

func (m mongoRestaurantRepository) Close() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, _ := mongo.Connect(ctx, options.Client().ApplyURI(m.addr))
	_ = client.Disconnect(ctx)
}

type aggregateTypeRestaurantResult struct {
	Type  string `bson:"type"`
	Total uint   `bson:"total"`
}

func ToRestaurantBsonUpdateOperator(r *domain.Restaurant) bson.D {
	return bson.D{{"$set", bson.D{
		{"name", r.Name},
		{"type", r.Type},
		{"id", r.Id},
		{"coordinates", r.Coordinates},
		{"calorieLevel", r.CalorieLevel}}},
	}
}
