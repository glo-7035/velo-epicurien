package mongo

import (
	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"go.mongodb.org/mongo-driver/bson"
)

type geoJson struct {
	Name     string  `bson:"name,omitempty"`
	Features feature `bson:"features,omitempty"`
	Id       string  `bson:"id,omitempty"`
}
type feature struct {
	Length   float32  `bson:"length,omitempty"`
	Geometry geometry `bson:"geometry,omitempty"`
}
type geometry struct {
	Type        string       `bson:"type,omitempty"`
	Coordinates [][2]float64 `bson:"coordinates,omitempty"`
}

func fromCyclePath(cyclePath *domain.CyclePath) geoJson {
	coodinates := make([][2]float64, len(cyclePath.Nodes))
	for i, node := range cyclePath.Nodes {
		coodinates[i] = node.Coordinates
	}
	geometry := geometry{
		Type:        "LineString",
		Coordinates: coodinates,
	}
	feature := feature{
		Length:   cyclePath.Length,
		Geometry: geometry,
	}
	return geoJson{
		Name:     cyclePath.Name,
		Id:       cyclePath.Id,
		Features: feature,
	}
}
func (g geoJson) TocyclePath() domain.CyclePath {
	nodes := make([]domain.CyclePathNode, len(g.Features.Geometry.Coordinates))
	for i, coordinate := range g.Features.Geometry.Coordinates {
		nodes[i] = domain.CyclePathNode{Coordinates: coordinate}
	}
	return domain.CyclePath{
		Id:     g.Id,
		Length: g.Features.Length,
		Name:   g.Name,
		Nodes:  nodes,
	}
}

func ToGeoJsonBsonUpdateOperator(g *geoJson) bson.D {
	return bson.D{{"$set", bson.D{
		{"name", g.Name},
		{"id", g.Id},
		{"features", bson.D{
			{"length", g.Features.Length},
			{"geometry", bson.D{
				{"type", g.Features.Geometry.Type},
				{"coordinates", g.Features.Geometry.Coordinates},
			}},
		}},
	}}}
}
