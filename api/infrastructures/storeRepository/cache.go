package storeRepository

import "gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"

type CacheRepository interface {
	GetParcoursResponse(payload.CourseRequest) (payload.GeoJson, error)
	GetCalorieResponse(payload.CourseRequest) (payload.CalorieResponseBody, error)
	Clear() error
	AddParcoursResponseIfNotExist(payload.CourseRequest, payload.GeoJson) error
	AddCalorieResponseIfNotExist(payload.CourseRequest, payload.CalorieResponseBody) error
	Close()
	GetAllResponse() ([]payload.GeoJson, []payload.CalorieResponseBody, error)
}
