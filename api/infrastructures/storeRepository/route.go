package storeRepository

import "gitlab.com/constraintAutomaton/velo-epicurien/api/domain"

type RouteRepository interface {
	Add(*domain.CyclePath) error
	AddMany([]domain.CyclePath) error
	All() ([]domain.CyclePath, error)
	Clear() error
	Ping() error
	Size() (uint, error)
	Close()
	TotalLength() (float64, error)
}
