package storeRepository

import (
	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
)

type RestaurantRepository interface {
	Add(*domain.Restaurant) error
	AddMany([]domain.Restaurant) error
	Ping() error
	Size() (uint, error)
	RestaurantsTypes() (map[string]uint, error)
	All() ([]domain.Restaurant, error)
	Clear() error
	Close()
}
