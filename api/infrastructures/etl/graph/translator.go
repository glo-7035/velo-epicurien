package graph

import (
	"fmt"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/etlError"
	infrastructures "gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/routeGraphRepository"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/routeGraphRepository/neo"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository/mongo"
	"sort"
	"strconv"
)

type Translator struct {
	routeGraphRepo infrastructures.RoutesGraph
	restaurantRepo storeRepository.RestaurantRepository
	routeRepo      storeRepository.RouteRepository
}

func ProvideTranslator() (Translator, error) {

	routeGraph, err := neo.ProvideRouteGraph()
	if err != nil {
		return Translator{}, err
	}
	restaurant, err := mongo.ProvideRestaurantRepository()
	if err != nil {
		return Translator{}, err
	}
	route, err := mongo.ProvideRouteRepository()
	if err != nil {
		return Translator{}, err
	}

	return Translator{routeGraphRepo: routeGraph, restaurantRepo: restaurant, routeRepo: route}, nil
}

func ProvideTranslatorWithParams(routeGraphRepo infrastructures.RoutesGraph,
	restaurantRepo storeRepository.RestaurantRepository,
	routeRepo storeRepository.RouteRepository) (Translator, error) {
	return Translator{routeGraphRepo: routeGraphRepo, restaurantRepo: restaurantRepo, routeRepo: routeRepo}, nil
}

func (translator *Translator) TranslateNewCyclePathEntries() error {

	cyclePaths, err := translator.routeRepo.All()
	if err != nil {
		return err
	}

	alreadyAddedCyclePaths, err := translator.routeGraphRepo.AllCyclePaths()
	if err != nil {
		return err
	}

	var idsInError []string
	fmt.Println(len(cyclePaths))
	for _, cyclePath := range cyclePaths {
		if !containsCyclePath(alreadyAddedCyclePaths, &cyclePath) {
			err = translator.routeGraphRepo.AddCyclePath(&cyclePath)
			if err != nil {
				fmt.Println(err)
				idsInError = append(idsInError,  "[ERROR CYCLE PATH] " + cyclePath.Name + " : " + err.Error())
			}
		}
	}
	fmt.Printf("there was %v error\n", len(idsInError))
	if len(idsInError) != 0 {
		var formattedIdsInError string
		for _, err := range idsInError {
			formattedIdsInError += "\n" + err
		}
		fmt.Printf(formattedIdsInError)
		return etlError.MinorError("failed to add cyclePaths for following entries: \n" + formattedIdsInError)
	}

	return nil
}

func containsCyclePath(s []domain.CyclePath, e *domain.CyclePath) bool {
	for _, a := range s {
		if (&a).Id == e.Id {
			return true
		}
	}
	return false
}

func (translator *Translator) translateNewCyclePathEntry(cyclePath *domain.CyclePath) error {

	err := translator.routeGraphRepo.AddCyclePath(cyclePath)
	if err != nil {
		return err
	}
	return nil
}

func (translator *Translator) TranslateNewRestaurantEntries() error {

	restaurants, err := translator.restaurantRepo.All()
	if err != nil {
		return err
	}

	alreadyAddedRestaurants, err := translator.routeGraphRepo.AllRestaurants()
	if err != nil {
		return err
	}

	newRestaurantOnly := except(restaurants, alreadyAddedRestaurants)


	sort.Slice(newRestaurantOnly, func(i, j int) bool {
		iVal, err := strconv.Atoi(newRestaurantOnly[i].Id)
		if err != nil {
			return false
		}
		jVal, err := strconv.Atoi(newRestaurantOnly[j].Id)
		if err != nil {
			return false
		}

		return iVal < jVal
	})

	err = translator.routeGraphRepo.AddAndConnectManyRestaurant(newRestaurantOnly)
	if err != nil {
		return etlError.MinorError(err.Error())
	}

	return nil
}

func except(collection []domain.Restaurant, except []domain.Restaurant) []domain.Restaurant {
	var result []domain.Restaurant
	for _, a := range collection {
		if !containsRestaurant(except, &a) {
			result = append(result, a)
		}
	}
	return result
}

func containsRestaurant(s []domain.Restaurant, e *domain.Restaurant) bool {
	for _, a := range s {
		if a.Id == e.Id {
			return true
		}
	}
	return false
}

func (translator *Translator) translateNewRestaurantEntry(restaurant domain.Restaurant) error {
	err := translator.routeGraphRepo.AddAndConnectRestaurant(&restaurant)
	if err != nil {
		return err
	}
	return nil
}
