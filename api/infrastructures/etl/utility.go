package etl

import (
	"fmt"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/routeGraphRepository/neo"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository/mongo"
)

func ClearAllRepository() error {
	restaurantRepo, err := mongo.ProvideRestaurantRepository()
	if err != nil {
		return fmt.Errorf("was not able to instanciate restaurant repository\n`%v`", err)
	}

	if err := restaurantRepo.Clear(); err != nil {
		return fmt.Errorf("was not able to clear restaurant repository\n`%v`", err)
	}

	routeRepo, err := mongo.ProvideRouteRepository()
	if err != nil {
		return fmt.Errorf("was not able to instanciate route repository\n`%v`", err)
	}
	if err := routeRepo.Clear(); err != nil {
		return fmt.Errorf("was not able to clear route repository\n`%v`", err)
	}

	graphRepository, err := neo.ProvideRouteGraph()
	if err != nil {
		return fmt.Errorf("was not able to instanciate graph repository\n`%v`", err)
	}
	if err := graphRepository.Clear(); err != nil {
		return fmt.Errorf("was not able to clear the graph repository\n`%v`", err)

	}
	cacheRepository, err := mongo.ProvideCacheRepository()
	if err != nil {
		return fmt.Errorf("was not able to instanciate cache repository\n`%v`", err)
	}
	if err := cacheRepository.Clear(); err != nil {
		return fmt.Errorf("was not able to clear the cache repository\n`%v`", err)

	}
	return nil
}

func CloseAllRepository() {
	repoGraph, err := neo.ProvideRouteGraph()
	if err != nil {
		fmt.Println("was not able to close the graph repository")
	}
	restaurantRepo, err := mongo.ProvideRestaurantRepository()
	if err != nil {
		fmt.Println("was not able to instanciate restaurant repository")
	}
	routeRepo, err := mongo.ProvideRouteRepository()
	if err != nil {
		fmt.Println("was not able to instanciate route repository")
	}
	cacheRepository, err := mongo.ProvideCacheRepository()
	if err != nil {
		fmt.Println("was not able to instanciate cache repository")
	}
	restaurantRepo.Close()
	repoGraph.Close()
	routeRepo.Close()
	cacheRepository.Close()
	fmt.Println("bye")
}
