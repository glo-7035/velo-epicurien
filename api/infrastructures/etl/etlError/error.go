package etlError

import (
	"errors"
	"strings"
)

const minorErrorIdentifier = "[MINOR]"

func MinorError(message string) error {
	return errors.New(minorErrorIdentifier + "\n" + message)
}

func IsMinor(err error) bool {
	return strings.HasPrefix(err.Error(), minorErrorIdentifier)
}