package restaurant

import (
	"fmt"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
)

func EtlProcedureRestaurant(
	extract func() ([]map[string]interface{}, error),
	transform func([]map[string]interface{}) ([]domain.Restaurant, error),
	load func([]domain.Restaurant) error,
) error {
	jsonRestaurant, err := extract()
	if err != nil {
		return fmt.Errorf("was not able to load the raw datas of restaurant\n`%s`", err.Error())
	}

	Restaurant, err := transform(jsonRestaurant)

	if err != nil {
		return fmt.Errorf("was not able to transform the json into Restaurant objects\n`%s`", err.Error())
	}

	err = load(Restaurant)
	if err != nil {
		return fmt.Errorf("was not able to load into database\n`%s`", err.Error())
	}
	return nil
}
