package extractor

import (
	"encoding/json"
	"io/ioutil"
)

func FileExtractor(path string) func() ([]map[string]interface{}, error) {
	return func() ([]map[string]interface{}, error) {
		data, err := ioutil.ReadFile(path)
		if err != nil {
			return nil, err
		}

		restaurants := []map[string]interface{}{}
		err = json.Unmarshal(data, &restaurants)
		if err != nil {
			return nil, err
		}
		return restaurants, nil
	}
}
