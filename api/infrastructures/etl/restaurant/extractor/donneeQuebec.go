package extractor

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/config"
)

func DonneeQuebec() func() ([]map[string]interface{}, error) {
	return func() ([]map[string]interface{}, error) {
		data := []map[string]interface{}{}
		path := config.ProvideConfig().RemoteRestaurantPath
		resp, err := http.Get(path)
		if err != nil {
			return nil, err
		}
		if resp != nil {
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return nil, err
			}
			if err = json.Unmarshal(body, &data); err != nil {
				return nil, err
			}
			resp.Body.Close()
		} else {
			return nil, fmt.Errorf("the http response is a nil pointer")
		}
		return data, nil
	}

}
