package loader

import (
	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository"
)

func Loader(repo storeRepository.RestaurantRepository) func(restaurents []domain.Restaurant) error {
	return func(restaurents []domain.Restaurant) error {
		return repo.AddMany(restaurents)
	}
}
