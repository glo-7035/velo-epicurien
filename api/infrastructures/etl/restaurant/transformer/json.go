package transformer

type donneeQuebec struct {
	ID         float64
	Nom        string
	Latitude   string
	Longitude  string
	Categories string
}
