package transformer

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/config"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
)

var typesRestaurants = map[int]string{}
var typesToCalorieMapping = map[string]int{}

func TransformerDonneeQuebec(data []map[string]interface{}) ([]domain.Restaurant, error) {
	if len(typesRestaurants) == 0 {
		loadType()
	}
	jsonbody, err := json.Marshal(data)
	if err != nil {
		return nil, fmt.Errorf("`Marshal error `%v`", err)
	}
	obj := []donneeQuebec{}
	jsonDecoder := json.NewDecoder(bytes.NewBuffer(jsonbody))

	if err := jsonDecoder.Decode(&obj); err != nil {

		return nil, fmt.Errorf("was not able to parse the map\n%v", err)
	}

	restaurants := make([]domain.Restaurant, 0, len(data))
	for _, el := range obj {

		restaurant := domain.Restaurant{
			Id:          fmt.Sprintf("%v", int(el.ID)),
			Name:        el.Nom,
			Coordinates: getPosition(el),
			Type:        getType(el),
		}
		if isValid(restaurant) {
			setCalorieLevel(&restaurant)
			restaurants = append(restaurants, restaurant)
		}

	}
	return restaurants, nil
}

func isValid(restaurant domain.Restaurant) bool {
	return len(restaurant.Coordinates) != 0
}

func setCalorieLevel(restaurant *domain.Restaurant) {
	if len(typesToCalorieMapping) == 0 {
		calculateCalorieLevelPerType()
	}

	if calorieLevel, ok := typesToCalorieMapping[(*restaurant).Type]; ok {
		(*restaurant).CalorieLevel = calorieLevel
	} else {
		(*restaurant).CalorieLevel = typesToCalorieMapping["DEFAULT"]
	}
}

func calculateCalorieLevelPerType() {
	typesToCalorieMapping["DEFAULT"] = 700
	typesToCalorieMapping["Bonnes tables"] = 600
	typesToCalorieMapping["Brasseries"] = 800
	typesToCalorieMapping["Cafés & Bistros"] = 700
	typesToCalorieMapping["Chefs créateurs"] = 630
	typesToCalorieMapping["Cuisine familiale"] = 377
	typesToCalorieMapping["Délices d'ici"] = 1245
	typesToCalorieMapping["Pubs et microbrasseries"] = 1350
	typesToCalorieMapping["Restauration rapide"] = 1330
	typesToCalorieMapping["Saveurs du monde"] = 856
}

func getPosition(el donneeQuebec) [2]float32 {
	lat, err := strconv.ParseFloat(el.Latitude, 32)
	if err != nil {
		return [2]float32{}
	}

	long, err := strconv.ParseFloat(el.Longitude, 32)
	if err != nil {
		return [2]float32{}

	}
	return [2]float32{float32(long), float32(lat)}

}

func getType(el donneeQuebec) string {
	typeString := strings.Split(el.Categories, ",")[0]
	typeInt, err := strconv.Atoi(typeString)
	if err == nil {
		v, ok := typesRestaurants[typeInt]
		if ok {
			return v
		}
		return "CAS_EXCLUSIF_EN_PRECOMMANDE"
	} else {
		return "CAS_GERER_EN_DLC"
	}
}
func loadType() {
	data, err := ioutil.ReadFile(config.ProvideConfig().TypeRestaurantDonneeQuebecPath)
	if err != nil {
		panic(err.Error())
	}

	types := []map[string]interface{}{}
	err = json.Unmarshal(data, &types)
	if err != nil {
		panic(err)
	}
	for _, el := range types {
		id, ok := el["ID"].(float64)
		if !ok {
			panic(" was not able to convert `ID` to int")
		}
		key := int(id)
		if err != nil {
			panic(err)
		}
		name, ok := el["Nom"].(string)
		if !ok {
			panic("was not able to convert `Nom` to string")
		}
		typesRestaurants[key] = name
	}
}
