package route

import (
	"fmt"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
)

func EtlProcedureRoute(
	extract func() (map[string]interface{}, error),
	transform func(map[string]interface{}) ([]domain.CyclePath, error),
	load func([]domain.CyclePath) error,
) error {
	jsonRoute, err := extract()
	if err != nil {
		return fmt.Errorf("was not able to load the raw datas of cycling path\n`%v`", err)
	}

	cyclingPath, err := transform(jsonRoute)

	if err != nil {
		return fmt.Errorf("was not able to transform the json into cyclingPath objects\n`%v`", err)
	}

	err = load(cyclingPath)
	if err != nil {
		return fmt.Errorf("was not able to load into database\n`%v`", err)
	}
	return nil
}
