package loader

import (
	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository"
)

func Loader(repo storeRepository.RouteRepository) func(paths []domain.CyclePath) error {
	return func(paths []domain.CyclePath) error {
		return repo.AddMany(paths)
	}
}
