package extractor

import (
	"encoding/json"
	"io/ioutil"
)

func FileExtractor(path string) func() (map[string]interface{}, error) {
	return func() (map[string]interface{}, error) {
		data, err := ioutil.ReadFile(path)
		if err != nil {
			return nil, err
		}
		route := map[string]interface{}{}

		err = json.Unmarshal(data, &route)
		if err != nil {
			return nil, err
		}
		return route, nil
	}
}
