package transformer

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
)

func TransformerDonneeQuebec(geojson map[string]interface{}) ([]domain.CyclePath, error) {

	jsonbody, err := json.Marshal(geojson)
	if err != nil {
		return nil, fmt.Errorf("`Marshal error `%v`", err)
	}

	obj := geoJson{}
	jsonDecoder := json.NewDecoder(bytes.NewBuffer(jsonbody))
	jsonDecoder.DisallowUnknownFields()

	if err := jsonDecoder.Decode(&obj); err != nil {

		return nil, fmt.Errorf("was not able to parse the map\n%v", err)
	}
	paths := make([]domain.CyclePath, 0, len(obj.Features))
	for _, feature := range obj.Features {
		nodes := make([]domain.CyclePathNode, 0, len(feature.Geometry.Coordinates))
		for _, coordinate := range feature.Geometry.Coordinates {
			c, ok := coordinate.([]interface{})
			if ok {
				c0, ok0 := c[0].(float64)
				c1, ok1 := c[1].(float64)
				if ok0 && ok1 {
					nodes = append(nodes, domain.CyclePathNode{Coordinates: [2]float64{c0, c1}})

				}
			}
		}
		path := domain.CyclePath{
			Id:     strconv.Itoa(feature.Properties.OBJECTID),
			Length: float32(feature.Properties.Shape_Length),
			Name:   feature.Properties.NOMDESTINATIONSHERBROOKE,
			Nodes:  nodes,
		}
		if isValid(path) {
			paths = append(paths, path)
		}
	}
	return paths, nil
}

func isValid(c domain.CyclePath) bool {
	return len(c.Nodes) != 0
}
