package transformer

type geoJson struct {
	Type     string     `json:"type"`
	Name     string     `json:"name"`
	Crs      crs        `json:"crs"`
	Features []features `json:"features"`
}
type crs struct {
	Type       string            `json:"type"`
	Properties map[string]string `json:"properties"`
}
type features struct {
	Type       string     `json:"type"`
	Properties properties `json:"properties"`
	Geometry   geometry   `json:"geometry"`
}

type properties struct {
	OBJECTID                 int
	NOMDESTINATIONSHERBROOKE string
	TYPEREVETEMENT           int
	NOMMTQ                   string
	Shape_Length             float64
}

type geometry struct {
	Type        string        `json:"type"`
	Coordinates []interface{} `json:"coordinates"`
}
