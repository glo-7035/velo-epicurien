package etl

import (
	"fmt"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/config"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/etlError"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/routeGraphRepository/neo"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/graph"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/restaurant"
	extractorRestaurant "gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/restaurant/extractor"
	loaderRestaurant "gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/restaurant/loader"
	transformerRestaurant "gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/restaurant/transformer"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/route"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/storeRepository/mongo"

	extractorRoute "gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/route/extractor"
	loaderRoute "gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/route/loader"
	transformerRoute "gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl/route/transformer"
)

func ETL() error {

	restaurantRepo, err := mongo.ProvideRestaurantRepository()
	if err != nil {
		return fmt.Errorf("was not able to get the restaurant repository\n%s", err.Error())
	}

	routeRepo, err := mongo.ProvideRouteRepository()
	if err != nil {
		return fmt.Errorf("was not able to get the route repository\n%s", err.Error())
	}

	err = restaurantRepo.Clear()
	if err != nil {
		return err
	}

	err = routeRepo.Clear()
	if err != nil {
		return err
	}

	err = restaurant.EtlProcedureRestaurant(
		extractorRestaurant.FileExtractor(config.ProvideConfig().LocalRestaurantPath),
		transformerRestaurant.TransformerDonneeQuebec,
		loaderRestaurant.Loader(restaurantRepo),
	)
	if err != nil {
		return err
	}
	err = route.EtlProcedureRoute(
		extractorRoute.FileExtractor(config.ProvideConfig().LocalCyclingRoutePath),
		transformerRoute.TransformerDonneeQuebec,
		loaderRoute.Loader(routeRepo),
	)
	if err != nil {
		return err
	}

	translator, err := graph.ProvideTranslator()
	if err != nil {
		return fmt.Errorf("was not able to instanciate")
	}

	err = translator.TranslateNewCyclePathEntries()
	if err != nil && !etlError.IsMinor(err) {
		return fmt.Errorf("was not able to integrate cycle path data to the graph\n `%s`", err)
	}
	err = translator.TranslateNewRestaurantEntries()
	if err != nil && !etlError.IsMinor(err) {
		return fmt.Errorf("was not able to integrate data to the graph\n `%s`", err)
	}

	return nil
}

func RemoteETL() error {
	graphRepository, err := neo.ProvideRouteGraph()
	if err != nil {
		return fmt.Errorf("was not able to instanciate graph repository\n`%v`", err)
	}
	if err := graphRepository.Clear(); err != nil {
		return fmt.Errorf("was not able to clear the graph repository\n`%v`", err)

	}

	restaurantRepo, err := mongo.ProvideRestaurantRepository()
	if err != nil {
		return fmt.Errorf("was not able to get the restaurant repository\n%s", err.Error())
	}

	routeRepo, err := mongo.ProvideRouteRepository()
	if err != nil {
		return fmt.Errorf("was not able to get the route repository\n%s", err.Error())
	}

	err = restaurant.EtlProcedureRestaurant(
		extractorRestaurant.DonneeQuebec(),
		transformerRestaurant.TransformerDonneeQuebec,
		loaderRestaurant.Loader(restaurantRepo),
	)
	if err != nil {
		return err
	}
	err = route.EtlProcedureRoute(
		extractorRoute.DonneeQuebec(),
		transformerRoute.TransformerDonneeQuebec,
		loaderRoute.Loader(routeRepo),
	)
	if err != nil {
		return err
	}

	translator, err := graph.ProvideTranslator()
	if err != nil {
		return fmt.Errorf("was not able to instanciate")
	}

	err = translator.TranslateNewCyclePathEntries()
	if err != nil && !etlError.IsMinor(err) {
		return fmt.Errorf("was not able to integrate cycle path data to the graph\n `%s`", err)
	}
	err = translator.TranslateNewRestaurantEntries()
	if err != nil && !etlError.IsMinor(err) {
		return fmt.Errorf("was not able to integrate data to the graph\n `%s`", err)
	}

	return nil
}
