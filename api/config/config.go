package config

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/spf13/viper"
)

type Configurations struct {
	MongoDatabase                  MongoDatabaseConfigurations
	NeoDatabase                    NeoDatabaseConfigurations
	ServerAddr                     string
	LocalRestaurantPath            string
	TypeRestaurantDonneeQuebecPath string
	RestaurantRepository           string
	RouteGraphRepository           string
	LocalCyclingRoutePath          string
	ReadmeFilePath                 string
	RemoteRestaurantPath           string
	RemoteCyclingRoutePath         string
}

type MongoDatabaseConfigurations struct {
	DBAddr     string
	DBUser     string
	DBPassword string
}

type NeoDatabaseConfigurations struct {
	DBAddr     string
	DBUser     string
	DBPassword string
}

func ProvideConfig() Configurations {
	config := Configurations{}
	err := viper.Unmarshal(&config)
	if err != nil {

		panic(fmt.Sprintf("enable to convert config to struct\n%s", err.Error()))
	}
	return config
}

func SetEnvariables(path string) error {

	file, err := ioutil.ReadFile(path)
	if err != nil {
		dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
		if err != nil {
			fmt.Printf("was on directory: %s\n`%s`", dir, err)
		}
		return err
	}
	err = viper.ReadConfig(bytes.NewBuffer(file))
	if err != nil {
		return fmt.Errorf("was not able to parse the config file\n`%v`", err)
	}
	return nil
}
