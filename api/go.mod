module gitlab.com/constraintAutomaton/velo-epicurien/api

go 1.14

require (
	github.com/cnf/structhash v0.0.0-20201013183111-a92e111048cd
	github.com/gorilla/mux v1.8.0
	github.com/magiconair/properties v1.8.1
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/neo4j/neo4j-go-driver v1.8.3
	github.com/robpike/filter v0.0.0-20150108201509-2984852a2183
	github.com/spf13/viper v1.7.1
	go.mongodb.org/mongo-driver v1.4.2
)
