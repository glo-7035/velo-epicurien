package server

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/config"

	"github.com/gorilla/mux"
	route "gitlab.com/constraintAutomaton/velo-epicurien/api/rest/route"
)

func setRoute(r *mux.Router) {

	r.HandleFunc("/heartbeat", route.HeartBeat).Methods("GET")
	r.HandleFunc("/extracted_data", route.ExtractedData).Methods("GET")
	r.HandleFunc("/transformed_data", route.TransformedData).Methods("GET")
	r.HandleFunc("/readme", route.Readme).Methods("GET")
	r.HandleFunc("/type", route.Type).Methods("GET")
	r.HandleFunc("/parcours", route.Course).Methods("GET")
	r.HandleFunc("/starting_point", route.StartingPoint).Methods("GET")
	r.HandleFunc("/calories", route.Calories).Methods("GET")
	r.HandleFunc("/admin/etl", route.Etl).Methods("GET")
	r.HandleFunc("/admin/clear", route.Clear).Methods("DELETE")
	r.HandleFunc("/admin/cache", route.Cache).Methods("GET")
	r.HandleFunc("/admin/cache", route.ClearCache).Methods("DELETE")
}

func getPort() string {

	return ":" + config.ProvideConfig().ServerAddr
}

// BuildServer build a server, provide the route to it and link the close channel to the server
func BuildServer(srv *http.Server, close chan os.Signal) {

	r := initializeServer()
	setRoute(r)

	srv.Addr = getPort()
	srv.Handler = r

	signal.Notify(close, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	log.Println("Server started")
	<-close
	closeServer(srv)
}

// initialise the router
func initializeServer() *mux.Router {
	r := mux.NewRouter()
	r.Use(mux.CORSMethodMiddleware(r))
	return r
}

// send a close server signal
func SendCloseSignal(close chan os.Signal) {
	close <- os.Interrupt
}

func closeServer(srv *http.Server) {
	log.Print("Server Stopped")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}
	log.Print("Server Exited Properly")
}
