package payload

const INVALID_LENGTH_MESSAGE = "maximumLength should be an integer greater than 0 and defined"
const INVALID_TYPE_MESSAGE = "At least one provided type was invalid. Please see /type endpoint for valid types. The type param should be a in the form of a list of string"
const ETL_FAILED = "Internal error ETL Failed because\n`%v`"
const INVALID_BODY_ERROR = "Provided request body was empty. This request requires body. Please refer to the /readme route for details."
const INVALID_NUMBER_STOP = "numberOfStops should be an integer greater than 0 and defined"
const STARTING_POINT_INVALID = "startingPoint should be a geoJson point"

type StartingPointRequest struct {
	MaximumLength int      `json:"maximumLength"`
	Type          []string `json:"type,omitempty"`
}

type StartingPointResponse struct {
	StartingPoint Geometry `json:"startingPoint,omitempty"`
}

type TransformedDataBody struct {
	Restaurants      map[string]uint `json:"restaurants"`
	LongueurCyclable float32         `json:"longueurCyclable"`
}

type ExtractedDataBody struct {
	NbRestaurants int `json:"nbRestaurants"`
	NbSegments    int `json:"nbSegments"`
}

type CourseRequest struct {
	StartingPoint Geometry `json:"startingPoint,omitempty"`
	MaximumLength int      `json:"maximumLength,omitempty"`
	NumberOfStops int      `json:"numberOfStops,omitempty"`
	Type          []string `json:"type,omitempty"`
}

type BadRequestParameterBody struct {
	ErrorMessage string `json:"errorMessage,omitempty"`
}

type CalorieResponseBody struct {
	CalorieIntake float32 `json:"calorieIntake"`
	CalorieOutput float32 `json:"calorieOutput"`
	CalorieNet    float32 `json:"calorieNet"`
	Course        GeoJson `json:"course"`
}

type ETLResponse struct {
	Before        TransformedDataBody `json:"before"`
	Now           TransformedDataBody `json:"now"`
	CalorieIntake float32             `json:"calorieIntake" bson:"calorieIntake"`
	CalorieOutput float32             `json:"calorieOutput" bson:"calorieOutput"`
	CalorieNet    float32             `json:"calorieNet" bson:"calorieNet"`
	Course        GeoJson             `json:"course" bson:"course"`
}

type CacheResponseBody struct {
	ParcoursResponses []GeoJson             `json:"parcoursResponse"`
	CalorieResponses  []CalorieResponseBody `json:"calorieResponses"`
}
