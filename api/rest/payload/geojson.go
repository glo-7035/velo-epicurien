package payload

import (
	"reflect"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/domain"
)

type GeoJson struct {
	Type     string    `json:"type" bson:"type"`
	Features []Feature `json:"features" bson:"features"`
}
type Feature struct {
	Type       string                 `json:"type" bson:"type"`
	Geometry   Geometry               `json:"geometry" bson:"geometry"`
	Properties map[string]interface{} `json:"properties" bson:"properties"`
}
type Geometry struct {
	Type        string      `json:"type" bson:"type"`
	Coordinates interface{} `json:"coordinates" bson:"coordinates"`
}

type PropertiesRestaurant struct {
	Name string `json:"name" bson:"name"`
	Type string `json:"type" bson:"type"`
}

func (p PropertiesRestaurant) ToMap() map[string]interface{} {
	return map[string]interface{}{
		"name": p.Name,
		"type": p.Type,
	}
}

type PropertiesPath struct {
	Length float32 `json:"length" bson:"length"`
}

func (p PropertiesPath) ToMap() map[string]interface{} {
	return map[string]interface{}{
		"length": p.Length,
	}
}

func ToGeoJsonPoint(cyclePathNode domain.CyclePathNode) Geometry {

	return Geometry{
		Type:        "Point",
		Coordinates: cyclePathNode.Coordinates,
	}
}

func ToGeoJson(cyclePath domain.CyclePath, restaurants []domain.Restaurant) GeoJson {
	resp := GeoJson{
		Type: "FeatureCollection",
	}
	features := make([]Feature, 0, len(restaurants)+1)
	for _, r := range restaurants {
		feature := Feature{
			Type: "Feature",
			Geometry: Geometry{
				Type:        "Point",
				Coordinates: r.Coordinates,
			},
			Properties: PropertiesRestaurant{
				Name: r.Name,
				Type: r.Type,
			}.ToMap(),
		}
		features = append(features, feature)
	}
	if !reflect.DeepEqual(cyclePath, domain.CyclePath{}) {

		featuresCyclePath := Feature{
			Type: "Feature",
			Geometry: Geometry{
				Type: "MultiLineString",
			},
			Properties: PropertiesPath{
				Length: cyclePath.Length,
			}.ToMap(),
		}
		coordinates := make([][2]float64, 0, len(cyclePath.Nodes))
		for _, n := range cyclePath.Nodes {
			coordinates = append(coordinates, n.Coordinates)
		}
		featuresCyclePath.Geometry.Coordinates = [][][2]float64{coordinates}
		features = append(features, featuresCyclePath)
	}
	if len(features) > 0 {
		resp.Features = features
	} else {
		return GeoJson{}
	}
	return resp
}
