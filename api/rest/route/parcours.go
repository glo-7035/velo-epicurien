package route

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/utility"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/service"
)

func Course(w http.ResponseWriter, r *http.Request) {

	var body payload.CourseRequest

	if r.Body == http.NoBody {
		errorMsg := payload.BadRequestParameterBody{
			ErrorMessage: payload.INVALID_BODY_ERROR,
		}
		utility.SendJSONBadRequest(&errorMsg, &w)
		return
	}
	bodyBinary, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utility.SendInternalError(&w, err)
	}
	err = json.Unmarshal(bodyBinary, &body)
	if err != nil {
		errorMsg, httpError, err := service.ParcoursError(bodyBinary)
		if err != nil {
			utility.SendInternalError(&w, err)
			return
		}
		utility.SendJSONBadRequestCustomError(&errorMsg, &w, httpError)
		return
	}

	if body.MaximumLength == 0 {
		errorMsg, httpError, err := service.ParcoursError(bodyBinary)
		if err != nil {
			utility.SendInternalError(&w, err)
			return
		}
		utility.SendJSONBadRequestCustomError(&errorMsg, &w, httpError)
		return
	}

	validTypes, err := service.IsValidRestaurantTypes(body.Type)
	if err != nil {
		utility.SendInternalError(&w, err)
		return
	}
	if !validTypes {
		errorMsg, httpError, err := service.ParcoursError(bodyBinary)
		if err != nil {
			utility.SendInternalError(&w, err)
			return
		}
		utility.SendJSONBadRequestCustomError(&errorMsg, &w, httpError)
		return
	}

	validLength := service.IsValidCycleLength(body.MaximumLength)
	if !validLength {
		errorMsg, httpError, err := service.ParcoursError(bodyBinary)
		if err != nil {
			utility.SendInternalError(&w, err)
			return
		}
		utility.SendJSONBadRequestCustomError(&errorMsg, &w, httpError)
		return
	}

	response, err := service.ParcoursResponse(body)
	if err != nil {
		errorMsg := payload.BadRequestParameterBody{
			ErrorMessage: err.Error(),
		}
		utility.SendJSONBadRequest(&errorMsg, &w)
		return
	}

	utility.SendJSONResponse(&response, &w)
}
