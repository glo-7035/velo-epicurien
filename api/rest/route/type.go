package route

import (
	"net/http"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/utility"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/service"
)

func Type(w http.ResponseWriter, r *http.Request) {
	types, err := service.GetRestaurantsTypes()
	if err != nil {
		utility.SendInternalError(&w, err)
	}
	utility.SendJSONResponse(types, &w)
}
