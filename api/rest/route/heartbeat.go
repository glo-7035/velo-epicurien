package route

import (
	"net/http"

	utility "gitlab.com/constraintAutomaton/velo-epicurien/api/rest/utility"
)

const CITY = "Sherbrooke"

func HeartBeat(w http.ResponseWriter, r *http.Request) {
	pingResp := map[string]string{"villeChoisie": CITY}
	utility.SendJSONResponse(&pingResp, &w)
}
