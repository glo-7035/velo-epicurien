package route

import (
	"io"
	"net/http"
	"os"
	"strconv"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/config"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/utility"
)

func Readme(w http.ResponseWriter, r *http.Request) {
	file, err := os.Open(config.ProvideConfig().ReadmeFilePath)
	if err != nil {
		utility.SendInternalError(&w, err)
	}
	defer file.Close()
	fileStat, _ := file.Stat()
	fileSize := strconv.FormatInt(fileStat.Size(), 10)

	w.Header().Set("Content-Disposition", "attachment; filename=readme.md")
	w.Header().Set("Content-Type", "text/markdown")
	w.Header().Set("Content-Length", fileSize)

	_, err = io.Copy(w, file)
	if err != nil {
		utility.SendInternalError(&w, err)
	}
}
