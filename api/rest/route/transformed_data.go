package route

import (
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
	"net/http"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/utility"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/service"
)

func TransformedData(w http.ResponseWriter, r *http.Request) {
	allTypes, err := service.GetNumberOfRestaurantByTypes()
	if err != nil {
		utility.SendInternalError(&w, err)
	}

	routeLength, err := service.GetRouteGraphTotalLength()
	if err != nil {
		utility.SendInternalError(&w, err)
	}

	transData := payload.TransformedDataBody{
		Restaurants:      allTypes,
		LongueurCyclable: routeLength,
	}

	utility.SendJSONResponse(&transData, &w)
}
