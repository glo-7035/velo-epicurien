package route

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/utility"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/service"
)

func StartingPoint(w http.ResponseWriter, r *http.Request) {

	var body payload.StartingPointRequest

	if r.Body == http.NoBody {
		errorMsg := payload.BadRequestParameterBody{
			ErrorMessage: payload.INVALID_BODY_ERROR,
		}
		utility.SendJSONBadRequest(&errorMsg, &w)
		return
	}
	bodyBinary, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utility.SendInternalError(&w, err)
	}
	err = json.Unmarshal(bodyBinary, &body)
	if err != nil {
		errorMsg, err := service.StartingPointError(bodyBinary)
		if err != nil {
			utility.SendInternalError(&w, err)
			return
		}
		utility.SendJSONBadRequest(&errorMsg, &w)
		return
	}

	if body.MaximumLength == 0 {
		errorMsg, err := service.StartingPointError(bodyBinary)
		if err != nil {
			utility.SendInternalError(&w, err)
			return
		}
		utility.SendJSONBadRequest(&errorMsg, &w)
		return
	}

	validTypes, err := service.IsValidRestaurantTypes(body.Type)
	if err != nil {
		errorMsg, err := service.StartingPointError(bodyBinary)
		if err != nil {
			utility.SendInternalError(&w, err)
			return
		}
		utility.SendJSONBadRequest(&errorMsg, &w)
		return
	}
	if !validTypes {
		errorMsg := payload.BadRequestParameterBody{
			ErrorMessage: payload.INVALID_TYPE_MESSAGE,
		}
		utility.SendJSONBadRequest(&errorMsg, &w)
		return
	}

	startingCycleNode, err := service.FindBestMatchForStartingPoint(body)
	if err != nil {
		errorMsg := payload.BadRequestParameterBody{
			ErrorMessage: err.Error(),
		}
		utility.SendJSONBadRequest(&errorMsg, &w)
		return
	}

	startingPoint := payload.ToGeoJsonPoint(startingCycleNode)
	responseStartingPoint := payload.StartingPointResponse{
		StartingPoint: startingPoint,
	}

	utility.SendJSONResponse(&responseStartingPoint, &w)
}
