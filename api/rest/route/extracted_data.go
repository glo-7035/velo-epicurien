package route

import (
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/utility"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/service"
	"net/http"
)

func ExtractedData(w http.ResponseWriter, r *http.Request) {
	nbRestaurants, err := service.GetTotalRestaurantCount()
	if err != nil {
		utility.SendInternalError(&w, err)
	}

	nbSegments, err := service.GetTotalPathCount()
	if err != nil {
		utility.SendInternalError(&w, err)
	}

	pingResp := payload.ExtractedDataBody {
		NbRestaurants: nbRestaurants,
		NbSegments:    nbSegments,
	}
	utility.SendJSONResponse(&pingResp, &w)
}

