package route

import (
	"fmt"
	"net/http"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/payload"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/rest/utility"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/service"
)

func Etl(w http.ResponseWriter, r *http.Request) {

	stateRepoBefore, err := getStateRepository()
	if err != nil {
		utility.SendInternalError(&w, err)
	}
	err = service.IncrementalETL()
	if err != nil {
		utility.SendInternalError(&w, fmt.Errorf(payload.ETL_FAILED, err))
	}
	stateRepoNow, err := getStateRepository()
	if err != nil {
		utility.SendInternalError(&w, err)
	}

	resp := payload.ETLResponse{
		Before: stateRepoBefore,
		Now:    stateRepoNow,
	}
	utility.SendJSONResponse(resp, &w)
}
func Clear(w http.ResponseWriter, r *http.Request) {
	err := service.Clear()
	if err != nil {
		utility.SendInternalError(&w, err)
	}
	resp := map[string]string{"response": "All repo are clear"}
	utility.SendJSONResponse(resp, &w)
}

func getStateRepository() (payload.TransformedDataBody, error) {
	allTypes, err := service.GetNumberOfRestaurantByTypes()
	if err != nil {
		return payload.TransformedDataBody{}, err
	}

	routeLength, err := service.GetRouteGraphTotalLength()
	if err != nil {
		return payload.TransformedDataBody{}, err
	}

	return payload.TransformedDataBody{
		Restaurants:      allTypes,
		LongueurCyclable: routeLength,
	}, nil
}
func Cache(w http.ResponseWriter, r *http.Request) {
	resp, err := service.GetCache()
	if err != nil {
		errorJson := payload.BadRequestParameterBody{
			ErrorMessage: err.Error(),
		}
		utility.SendJSONResponse(&errorJson, &w)
	}
	utility.SendJSONResponse(&resp, &w)
}

func ClearCache(w http.ResponseWriter, r *http.Request) {
	if err := service.ClearCache(); err != nil {
		utility.SendInternalError(&w, err)
	}
	utility.SendJSONResponse(&map[string]string{"response": "cache deleted"}, &w)
}
