package utility

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

func SendJSONResponse(resp interface{}, w *http.ResponseWriter) {

	b, err := json.Marshal(resp)
	if err != nil {
		SendInternalError(w, err)
	} else {
		(*w).Header().Set("Content-Type", "application/json")
		_, err = io.WriteString(*w, string(b))
		if err != nil {
			SendInternalError(w, err)
		}
	}
}

func SendJSONBadRequest(resp interface{}, w *http.ResponseWriter) {

	b, err := json.Marshal(resp)
	if err != nil {
		SendInternalError(w, err)
	} else {
		(*w).Header().Set("Content-Type", "application/json")
		(*w).WriteHeader(http.StatusBadRequest)
		_, err = io.WriteString(*w, string(b))
		if err != nil {
			SendInternalError(w, err)
		}
	}
}

func SendJSONBadRequestCustomError(resp interface{}, w *http.ResponseWriter, httpError int) {

	b, err := json.Marshal(resp)
	if err != nil {
		SendInternalError(w, err)
	} else {
		(*w).Header().Set("Content-Type", "application/json")
		(*w).WriteHeader(httpError)
		_, err = io.WriteString(*w, string(b))
		if err != nil {
			SendInternalError(w, err)
		}
	}
}

func SendInternalError(w *http.ResponseWriter, err error) {
	log.Println(err)
	http.Error(*w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	fmt.Fprintf(*w, "")
}
