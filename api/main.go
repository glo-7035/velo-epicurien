package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/constraintAutomaton/velo-epicurien/api/config"

	"gitlab.com/constraintAutomaton/velo-epicurien/api/infrastructures/etl"
	server "gitlab.com/constraintAutomaton/velo-epicurien/api/server"
)

func main() {

	setEnvariables()
	etl := getCLIFlag()

	if etl {
		etlMode()
		fmt.Println("ETL mode")

	} else {
		serverMode()
		fmt.Println("server mode")
	}
	fmt.Println("bye")
	closeRepositories()
	os.Exit(0)
}
func getCLIFlag() bool {
	etl := flag.Bool("etl", false, "lanch the app in ETL mode")
	flag.Parse()
	return *etl
}

func serverMode() {
	srv := &http.Server{}
	close := make(chan os.Signal, 1)
	server.BuildServer(srv, close)
}

func setEnvariables() {
	configNeo := config.NeoDatabaseConfigurations{
		DBAddr:     "neo4j://neo4j:7687",
		DBUser:     "neo4j",
		DBPassword: "glo7035",
	}
	viper.Set("Neodatabase", configNeo)
	configMongo := config.MongoDatabaseConfigurations{
		DBAddr: "mongodb://mongo:27017",
	}
	viper.Set("Mongodatabase", configMongo)
	viper.Set("ServerAddr", "80")
	viper.Set("LocalRestaurantPath", "../app/asset/Data/restaurants.json")
	viper.Set("TypeRestaurantDonneeQuebecPath", "../app/asset/Data/restaurantscategories.json")
	viper.Set("LocalCyclingRoutePath", "../app/asset/Data/pistes_cyclables/Pistes_cyclables.geojson")
	viper.Set("ReadmeFilePath", "../app/readme.md")
	viper.Set("RemoteRestaurantPath", "https://www.donneesquebec.ca/recherche/fr/dataset/61b5b4e9-d038-4995-b85a-de039dc1b06b/resource/386e62b2-47ae-43bc-a85a-efbcaf3130ba/download/restaurants.json")
	viper.Set("RemoteCyclingRoutePath", "http://donneesouvertes-sherbrooke.opendata.arcgis.com/datasets/e7b14d41a20c4aafa91ed4a9b99384c3_3.geojson?outSR={\"latestWkid\":32187,\"wkid\":32187}")
	fmt.Println(config.ProvideConfig())
}

func etlMode() {

	if err := etl.ETL(); err != nil {
		fmt.Println(err)
		err := etl.ClearAllRepository()
		if err != nil {
			fmt.Println(err.Error())
		} else {
			fmt.Println("The repository has been cleared")
		}
		fmt.Println("ETL failled")
		closeRepositories()
		os.Exit(1)
	}
}

func closeRepositories() {
	etl.CloseAllRepository()

}
