package domain

import "fmt"

type CyclePathNode struct {
	Id int64 `json:"db_id,omitempty"`
	Coordinates [2]float64 `json:"coordinates,omitempty"`
}

func (c CyclePathNode) Longitude() float64 {
	return c.Coordinates[0]
}

func (c CyclePathNode) Latitude() float64 {
	return c.Coordinates[1]
}

func (n *CyclePathNode) Print() {
	fmt.Printf("######### NODE %d #########\n", n.Id)
	fmt.Printf("longitude : %v\n", n.Longitude())
	fmt.Printf("latitude : %v\n", n.Latitude())
	fmt.Println()
}

