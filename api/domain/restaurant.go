package domain

import (
	"fmt"
)

type Restaurant struct {
	Id           string     `bson:"id,omitempty" json:"id,omitempty"`
	Name         string     `bson:"name,omitempty" json:"name,omitempty"`
	Type         string     `bson:"type,omitempty" json:"type,omitempty"`
	Coordinates  [2]float32 `bson:"coordinates,omitempty" json:"coordinates,omitempty"`
	CalorieLevel int        `bson:"calorieLevel,omitempty" json:"calorieLevel,omitempty"`
}

func (r Restaurant) Longitude() float32 {
	return r.Coordinates[0]
}

func (r Restaurant) Latitude() float32 {
	return r.Coordinates[1]
}

func (r Restaurant) Print() {
	fmt.Printf("######## RESTAURANT %s ########\n", r.Id)
	fmt.Printf("Name: %s\n", r.Name)
	fmt.Printf("Type: %s\n", r.Type)
	fmt.Printf("Longitude: %f\n", r.Longitude())
	fmt.Printf("Latitude: %f\n", r.Latitude())
	fmt.Println()
}
