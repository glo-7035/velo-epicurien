package domain

type CyclePath struct {
	Nodes  []CyclePathNode `json:"nodes,omitempty"`
	Id     string          `json:"id,omitempty"`
	Length float32         `json:"length,omitempty"`
	Name   string          `json:"name,omitempty"`
}
