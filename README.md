Démarrage de l'application

1. Exécuter `make run` à la racine du projet

2. Si une des BD plante à l'exécution, essayer de faire `make clear-docker` ou/et vider les répertoires ./data/mongo ou ./data/neo4j

3. Joindre le lien http://localhost/heartbeat, http://localhost/extracted_data et http://localhost/transformed_data
